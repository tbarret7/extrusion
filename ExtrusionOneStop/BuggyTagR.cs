﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;
using System.Data.SqlClient;
using Microsoft.Reporting.WinForms;

namespace ExtrusionOneStop
{
    public partial class BuggyTagR : Form
    {      
        

        public BuggyTagR()        
        {
            InitializeComponent();            

        }
        DataTable set = new DataTable();

        private void Form6_Load(object sender, EventArgs e)
        {
            GEI_ExtrusionDataSet ds = new GEI_ExtrusionDataSet();
            DataTable arr = new DataTable();           
            

            string oradb = "Data Source=(DESCRIPTION="
                 + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=gei-glovia)(PORT=1521)))"
                 + "(CONNECT_DATA=(SERVER=prod)(SERVICE_NAME=G2prod.gei.local)));"
                 + "User Id=glovia_prod;Password=manager;";
            OracleConnection connection = new OracleConnection(oradb);

            OracleCommand command = new OracleCommand
            {
                Connection = connection,
                CommandText = 
                "SELECT " +
                    "(CASE WHEN WO_RTG.START_DATE is Null THEN WO.START_DATE ELSE WO_RTG.START_DATE END) AS SDate, " +
                    "(CASE WHEN WO_RTG.END_DATE is Null THEN WO.DUE_DATE ELSE WO_RTG.END_DATE END) AS EDate, " +
                    "WO.ITEM AS ITEM, WO.REVISION AS REV, WO.WO_NUM AS NUM, Trim(WO.WO_NUM) AS tWO, WO.WO_LINE, WO_RTG.OPERATION, WO_RTG.FUNCTION_, " +
                    "ITEM_CCN.USER2 As DIE, ITEM.HAZARD AS TEMPER, ITEM.LENGTH AS LENGTH, WO_RTG.WC As WC, WO_BHDR.BOM_DESCRIPTION As CUSTOMER, " +
                    "ITEM.USER1 As WT_FT, (WO.ORD_QTY - WO_RTG.COMP_QTY) as OPENQTY, WO_BOM.ITEM As ALLOY, " +
                    "WO.ORD_QTY AS ORDQTY, WO_RTG.COMP_QTY, OVER_QTY_PCT, tblPRT_MIN_OPER.PRTWO, tblPRT_MIN_OPER.PRTLN, " +
                    "tblPRT_MIN_OPER.PRTDEPT, tblPRT_MIN_OPER.PRTDESC, " +
                    "(CASE WHEN OVER_QTY_PCT is Null THEN WO.ORD_QTY ELSE (WO.ORD_QTY - (WO.ORD_QTY * OVER_QTY_PCT / 100)) END) AS Min, WO.STATUS AS STATUS " +
                "From " +
                    "WO Inner Join WO_RTG On WO_RTG.CCN = WO.CCN And WO_RTG.MAS_LOC = WO.MAS_LOC And WO_RTG.WO_NUM = WO.WO_NUM And " +
                    "WO_RTG.WO_LINE = WO.WO_LINE INNER JOIN ITEM_CCN ON ITEM_CCN.ITEM = WO.ITEM And ITEM_CCN.REVISION = WO.REVISION " +
                    "LEFT JOIN WO_BOM ON WO_RTG.CCN = WO_BOM.CCN And WO_RTG.MAS_LOC = WO_BOM.MAS_LOC And WO_RTG.WO_NUM = WO_BOM.WO_NUM And " +
                    "WO_RTG.WO_LINE = WO_BOM.WO_LINE " +
                    "LEFT JOIN WO_BHDR On WO_RTG.CCN = WO_BHDR.CCN And WO_RTG.MAS_LOC = WO_BHDR.MAS_LOC And WO_RTG.WO_NUM = WO_BHDR.WO_NUM And " +
                    "WO_RTG.WO_LINE = WO_BHDR.WO_LINE " +
                    "LEFT JOIN SHP_TOLR ON ITEM_CCN.SHP_TOLR = SHP_TOLR.SHP_TOLR AND ITEM_CCN.CCN = SHP_TOLR.CCN INNER JOIN " +
                    "ITEM On ITEM.ITEM = ITEM_CCN.ITEM And ITEM.REVISION = ITEM_CCN.REVISION " +
                    "LEFT OUTER JOIN " +
                        "(Select " +
                            "WOWOB.CCN, " +
                            "WOWOB.MAS_LOC, " +
                            "WOWOB.WO_NUM, " +
                            "WOWOB.WO_LINE, " +
                            "WOWOB.PARENT_WO_NUM AS PRTWO, " +
                            "WOWOB.PARENT_WO_LINE AS PRTLN, " +
                            "PRT_WO_RTG_Filter.DEPT AS PRTDEPT, " +
                            "PRT_WO_RTG_Filter.DSCRPT AS PRTDESC " +
                        "From " +
                            "WO Inner Join " +
                            "WOWOB On WOWOB.CCN = WO.CCN " +
                            "And WOWOB.MAS_LOC = WO.MAS_LOC " +
                            "And WOWOB.WO_NUM = WO.WO_NUM " +
                            "And WOWOB.WO_LINE = WO.WO_LINE Inner Join " +
                                "(Select " +
                                        "WO_RTG.CCN, " +
                                        "WO_RTG.MAS_LOC, " +
                                        "WO_RTG.WO_NUM, " +
                                        "WO_RTG.WO_LINE, " +
                                        "Min(WO_RTG.OPERATION) As MIN_PRT_OPER " +
                                    "From " +
                                        "WO Inner Join " +
                                        "WO_RTG On WO_RTG.CCN = WO.CCN " +
                                        "And WO_RTG.MAS_LOC = WO.MAS_LOC " +
                                        "And WO_RTG.WO_NUM = WO.WO_NUM " +
                                        "And WO_RTG.WO_LINE = WO.WO_LINE " +
                                    "Where " +
                                        "WO.CCN = '7' And " +
                                        "(WO.STATUS = 'I' Or " +
                                        "WO.STATUS = 'R' Or " +
                                        "WO.STATUS = 'U') And " +
                                        "WO.MFG_CLOSE_DATE Is Null " +
                                    "Group By " +
                                        "WO_RTG.CCN, " +
                                        "WO_RTG.MAS_LOC, " +
                                        "WO_RTG.WO_NUM, " +
                                        "WO_RTG.WO_LINE) WO_RTG_MIN_Oper On WOWOB.CCN = WO_RTG_MIN_Oper.CCN " +
                                        "And WOWOB.MAS_LOC = WO_RTG_MIN_Oper.MAS_LOC " +
                                        "And WOWOB.PARENT_WO_NUM = WO_RTG_MIN_Oper.WO_NUM " +
                                        "And WOWOB.PARENT_WO_LINE = WO_RTG_MIN_Oper.WO_LINE Inner Join " +
                                            "(Select " +
                                                    "WO_RTG.CCN, " +
                                                    "WO_RTG.MAS_LOC, " +
                                                    "WO_RTG.WO_NUM, " +
                                                    "WO_RTG.WO_LINE, " +
                                                    "WO_RTG.OPERATION, " +
                                                    "DEPT.DEPARTMENT As DEPT, " +
                                                    "DEPT.DESCRIPTION As DSCRPT " +
                                                "From " +
                                                    "WO Inner Join " +
                                                    "WO_RTG On WO_RTG.CCN = WO.CCN " +
                                                    "And WO_RTG.MAS_LOC = WO.MAS_LOC " +
                                                    "And WO_RTG.WO_NUM = WO.WO_NUM " +
                                                    "And WO_RTG.WO_LINE = WO.WO_LINE Inner Join " +
                                                    "WC On WO_RTG.CCN = WC.CCN " +
                                                    "And WO_RTG.WC = WC.WC Inner Join " +
                                                    "DEPT On WC.CCN = DEPT.CCN " +
                                                    "And WC.DEPARTMENT = DEPT.DEPARTMENT " +
                                                "Where " +
                                                    "WO.CCN = '7' And " +
                                                    "(WO.STATUS = 'I' Or " +
                                                    "WO.STATUS = 'R' Or " +
                                                    "WO.STATUS = 'U') And " +
                                                    "WO.MFG_CLOSE_DATE Is Null) PRT_WO_RTG_Filter On PRT_WO_RTG_Filter.CCN = WO_RTG_MIN_Oper.CCN " +
                                                    "And PRT_WO_RTG_Filter.MAS_LOC = WO_RTG_MIN_Oper.MAS_LOC " +
                                                    "And PRT_WO_RTG_Filter.WO_NUM = WO_RTG_MIN_Oper.WO_NUM " +
                                                    "And PRT_WO_RTG_Filter.WO_LINE = WO_RTG_MIN_Oper.WO_LINE " +
                                                    "And PRT_WO_RTG_Filter.OPERATION = WO_RTG_MIN_Oper.MIN_PRT_OPER) tblPRT_MIN_OPER " +
                                                    "ON WO.WO_NUM = tblPRT_MIN_OPER.WO_NUM AND WO.WO_LINE = tblPRT_MIN_OPER.WO_LINE " +
                "WHERE "+
                "WO.MFG_CLOSE_DATE IS NULL AND WO_RTG.FUNCTION_ = 'PRESS' AND WO.STATUS = 'R' And WO_BOM.ITEM Like 'A7%' And WO.ORD_QTY > 0 " +
                "And WO.Item Not Like '%PLAN%'"

            };                             

            this.reportViewer1.RefreshReport();
            
            try
            {              

                connection.Open();
                OracleDataAdapter adapter = new OracleDataAdapter(command);
                
                adapter.Fill(arr);
                adapter.Fill(ds, ds.Tables[0].TableName);

                connection.Close();

                
                dataGridView1.DataSource = arr;
                                
                
            }
            catch(Exception x)
            {
                MessageBox.Show(x.ToString());
            }
                                          
            ReportDataSource src = new ReportDataSource("BuggyTagR", ds.Tables[0]);        
            
            this.reportViewer1.LocalReport.DataSources.Clear();                   
            this.reportViewer1.LocalReport.DataSources.Add(src);            
            this.reportViewer1.Refresh();
            this.reportViewer1.RefreshReport();
          
        }      
    }
}
