﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace ExtrusionOneStop
{
    public partial class BuggyTag : Form
    {
       DataTable Data = new DataTable();
        
       
         public BuggyTag(DataTable data)
         {
            InitializeComponent();         
            Data = data;            
            
         }

        private void BuggyTag_Load(object sender, EventArgs e)
        {


            GEI_ExtrusionDataSet ds = new GEI_ExtrusionDataSet();
            
            dataGridView1.DataSource = ds.Tables[1];

            foreach (DataRow r in Data.Rows)
            {
                ds.BuggyTag.ImportRow(r);
            }

            ds.BuggyTag.AcceptChanges();

            this.reportViewer1.RefreshReport();
            ReportDataSource src2 = new ReportDataSource("BuggyTag", ds.Tables[1]);
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(src2);
            this.reportViewer1.Refresh();

        }
    }
}
