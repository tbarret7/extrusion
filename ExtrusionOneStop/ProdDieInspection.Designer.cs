﻿namespace ExtrusionOneStop
{
    partial class ProdDieInspection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label25 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.button9 = new System.Windows.Forms.Button();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.tblProdDieInspDataGridView = new System.Windows.Forms.DataGridView();
            this.tblProdDieInspBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gEI_ExtrusionDataSet2 = new ExtrusionOneStop.GEI_ExtrusionDataSet2();
            this.tblProdDieInspTableAdapter = new ExtrusionOneStop.GEI_ExtrusionDataSet2TableAdapters.tblProdDieInspTableAdapter();
            this.tableAdapterManager = new ExtrusionOneStop.GEI_ExtrusionDataSet2TableAdapters.TableAdapterManager();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Weld_scrap = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Weld_scrap_comments = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Coring = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Coring_comments = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TB_comments = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comments = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RejComments = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InspBy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CorrectedBy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vendor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DieTrial = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Production = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblProdDieInspDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblProdDieInspBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gEI_ExtrusionDataSet2)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(395, 22);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(41, 13);
            this.label25.TabIndex = 59;
            this.label25.Text = "Vendor";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "YTD",
            "TRI-R",
            "EXCO",
            "MVTM",
            "COMPES",
            "THUMB"});
            this.comboBox1.Location = new System.Drawing.Point(447, 19);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(75, 21);
            this.comboBox1.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(229, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 57;
            this.label2.Text = "Suffix";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 56;
            this.label1.Text = "Die Number";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(273, 19);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 5;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(94, 19);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox12);
            this.groupBox1.Controls.Add(this.checkBox2);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(473, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(654, 79);
            this.groupBox1.TabIndex = 60;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Header";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(447, 53);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(75, 20);
            this.textBox12.TabIndex = 23;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(273, 53);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(77, 17);
            this.checkBox2.TabIndex = 8;
            this.checkBox2.Text = "Production";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(94, 53);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(65, 17);
            this.checkBox1.TabIndex = 7;
            this.checkBox1.Text = "Die Trial";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.textBox8);
            this.groupBox2.Controls.Add(this.textBox7);
            this.groupBox2.Controls.Add(this.textBox6);
            this.groupBox2.Controls.Add(this.textBox5);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.textBox4);
            this.groupBox2.Controls.Add(this.textBox3);
            this.groupBox2.Location = new System.Drawing.Point(475, 78);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(652, 301);
            this.groupBox2.TabIndex = 61;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Footer";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(540, 244);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 13);
            this.label8.TabIndex = 77;
            this.label8.Text = "Correction Date";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(391, 245);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 13);
            this.label7.TabIndex = 76;
            this.label7.Text = "Corrected By";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(155, 244);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 75;
            this.label6.Text = "Date";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 244);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 74;
            this.label5.Text = "Inspected By";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(543, 260);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(100, 20);
            this.textBox8.TabIndex = 13;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(394, 260);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(100, 20);
            this.textBox7.TabIndex = 12;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(155, 260);
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(100, 20);
            this.textBox6.TabIndex = 65;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(6, 260);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(100, 20);
            this.textBox5.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 13);
            this.label3.TabIndex = 62;
            this.label3.Text = "Die Non-Conformance";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(393, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 63;
            this.label4.Text = "Die Correction";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(394, 39);
            this.textBox4.Multiline = true;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(249, 203);
            this.textBox4.TabIndex = 11;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(6, 39);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(249, 203);
            this.textBox3.TabIndex = 9;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "OK",
            "NA"});
            this.comboBox2.Location = new System.Drawing.Point(108, 30);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(100, 21);
            this.comboBox2.TabIndex = 14;
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "OK",
            "NA"});
            this.comboBox3.Location = new System.Drawing.Point(108, 57);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(100, 21);
            this.comboBox3.TabIndex = 16;
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            "OK",
            "NA"});
            this.comboBox4.Location = new System.Drawing.Point(108, 84);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(100, 21);
            this.comboBox4.TabIndex = 18;
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(214, 31);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(333, 20);
            this.textBox9.TabIndex = 15;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(214, 58);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(333, 20);
            this.textBox10.TabIndex = 17;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(214, 85);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(333, 20);
            this.textBox11.TabIndex = 19;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(41, 33);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 13);
            this.label9.TabIndex = 78;
            this.label9.Text = "Weld Scrap";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(65, 60);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 13);
            this.label10.TabIndex = 79;
            this.label10.Text = "Coring";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 88);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 13);
            this.label11.TabIndex = 80;
            this.label11.Text = "Tearing / Bubbles";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.comboBox2);
            this.groupBox3.Controls.Add(this.textBox11);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.textBox10);
            this.groupBox3.Controls.Add(this.textBox9);
            this.groupBox3.Controls.Add(this.comboBox3);
            this.groupBox3.Controls.Add(this.comboBox4);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Location = new System.Drawing.Point(473, 378);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(654, 124);
            this.groupBox3.TabIndex = 81;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Initial Die Trial Process Verification";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(553, 17);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(92, 44);
            this.button2.TabIndex = 20;
            this.button2.Text = "Update Record";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(553, 62);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(92, 44);
            this.button1.TabIndex = 21;
            this.button1.Text = "Submit New Record";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(67, 39);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(10, 13);
            this.label29.TabIndex = 69;
            this.label29.Text = "-";
            // 
            // textBox24
            // 
            this.textBox24.Location = new System.Drawing.Point(83, 36);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(26, 20);
            this.textBox24.TabIndex = 2;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(19, 62);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(90, 32);
            this.button9.TabIndex = 3;
            this.button9.Text = "Search";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.Button9_Click);
            // 
            // textBox28
            // 
            this.textBox28.Location = new System.Drawing.Point(19, 36);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(43, 20);
            this.textBox28.TabIndex = 1;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(6, 16);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(48, 13);
            this.label33.TabIndex = 66;
            this.label33.Text = "Die Num";
            // 
            // tblProdDieInspDataGridView
            // 
            this.tblProdDieInspDataGridView.AllowUserToAddRows = false;
            this.tblProdDieInspDataGridView.AllowUserToDeleteRows = false;
            this.tblProdDieInspDataGridView.AutoGenerateColumns = false;
            this.tblProdDieInspDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tblProdDieInspDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn9,
            this.Weld_scrap,
            this.Weld_scrap_comments,
            this.Coring,
            this.Coring_comments,
            this.TB,
            this.TB_comments,
            this.Comments,
            this.RejComments,
            this.InspBy,
            this.dataGridViewTextBoxColumn10,
            this.CorrectedBy,
            this.Vendor,
            this.DieTrial,
            this.Production});
            this.tblProdDieInspDataGridView.DataSource = this.tblProdDieInspBindingSource;
            this.tblProdDieInspDataGridView.Location = new System.Drawing.Point(3, 2);
            this.tblProdDieInspDataGridView.Name = "tblProdDieInspDataGridView";
            this.tblProdDieInspDataGridView.ReadOnly = true;
            this.tblProdDieInspDataGridView.Size = new System.Drawing.Size(464, 500);
            this.tblProdDieInspDataGridView.TabIndex = 82;
            this.tblProdDieInspDataGridView.Click += new System.EventHandler(this.TblProdDieInspDataGridView_Click);
            this.tblProdDieInspDataGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TblProdDieInspDataGridView_KeyDown);
            // 
            // tblProdDieInspBindingSource
            // 
            this.tblProdDieInspBindingSource.DataMember = "tblProdDieInsp";
            this.tblProdDieInspBindingSource.DataSource = this.gEI_ExtrusionDataSet2;
            // 
            // gEI_ExtrusionDataSet2
            // 
            this.gEI_ExtrusionDataSet2.DataSetName = "GEI_ExtrusionDataSet2";
            this.gEI_ExtrusionDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tblProdDieInspTableAdapter
            // 
            this.tblProdDieInspTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.tbl3DielogTableAdapter = null;
            this.tableAdapterManager.tblDieRingTableAdapter = null;
            this.tableAdapterManager.tblExtDieInspTableAdapter = null;
            this.tableAdapterManager.tblPress2_Die_Info_AlloyTableAdapter = null;
            this.tableAdapterManager.tblProdDieInspTableAdapter = this.tblProdDieInspTableAdapter;
            this.tableAdapterManager.UpdateOrder = ExtrusionOneStop.GEI_ExtrusionDataSet2TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label33);
            this.groupBox4.Controls.Add(this.button9);
            this.groupBox4.Controls.Add(this.textBox28);
            this.groupBox4.Controls.Add(this.label29);
            this.groupBox4.Controls.Add(this.textBox24);
            this.groupBox4.Location = new System.Drawing.Point(262, 84);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(126, 106);
            this.groupBox4.TabIndex = 70;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Menu";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Rec_id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Rec_id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 50;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Die_Number";
            this.dataGridViewTextBoxColumn2.HeaderText = "Die_Number";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 50;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Die_Suffix";
            this.dataGridViewTextBoxColumn3.HeaderText = "Die_Suffix";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 50;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "CorrectedDate";
            this.dataGridViewTextBoxColumn9.HeaderText = "CorrectedDate";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 50;
            // 
            // Weld_scrap
            // 
            this.Weld_scrap.DataPropertyName = "Weld_scrap";
            this.Weld_scrap.HeaderText = "Weld_scrap";
            this.Weld_scrap.Name = "Weld_scrap";
            this.Weld_scrap.ReadOnly = true;
            this.Weld_scrap.Width = 25;
            // 
            // Weld_scrap_comments
            // 
            this.Weld_scrap_comments.DataPropertyName = "Weld_scrap_comments";
            this.Weld_scrap_comments.HeaderText = "Weld_scrap_comments";
            this.Weld_scrap_comments.Name = "Weld_scrap_comments";
            this.Weld_scrap_comments.ReadOnly = true;
            this.Weld_scrap_comments.Width = 25;
            // 
            // Coring
            // 
            this.Coring.DataPropertyName = "Coring";
            this.Coring.HeaderText = "Coring";
            this.Coring.Name = "Coring";
            this.Coring.ReadOnly = true;
            this.Coring.Width = 25;
            // 
            // Coring_comments
            // 
            this.Coring_comments.DataPropertyName = "Coring_comments";
            this.Coring_comments.HeaderText = "Coring_comments";
            this.Coring_comments.Name = "Coring_comments";
            this.Coring_comments.ReadOnly = true;
            this.Coring_comments.Width = 25;
            // 
            // TB
            // 
            this.TB.DataPropertyName = "TB";
            this.TB.HeaderText = "TB";
            this.TB.Name = "TB";
            this.TB.ReadOnly = true;
            this.TB.Width = 25;
            // 
            // TB_comments
            // 
            this.TB_comments.DataPropertyName = "TB_comments";
            this.TB_comments.HeaderText = "TB_comments";
            this.TB_comments.Name = "TB_comments";
            this.TB_comments.ReadOnly = true;
            this.TB_comments.Width = 25;
            // 
            // Comments
            // 
            this.Comments.DataPropertyName = "Comments";
            this.Comments.HeaderText = "Comments";
            this.Comments.Name = "Comments";
            this.Comments.ReadOnly = true;
            this.Comments.Width = 25;
            // 
            // RejComments
            // 
            this.RejComments.DataPropertyName = "RejComments";
            this.RejComments.HeaderText = "RejComments";
            this.RejComments.Name = "RejComments";
            this.RejComments.ReadOnly = true;
            this.RejComments.Width = 25;
            // 
            // InspBy
            // 
            this.InspBy.DataPropertyName = "InspBy";
            this.InspBy.HeaderText = "InspBy";
            this.InspBy.Name = "InspBy";
            this.InspBy.ReadOnly = true;
            this.InspBy.Width = 25;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "CurrentDate";
            this.dataGridViewTextBoxColumn10.HeaderText = "CurrentDate";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 25;
            // 
            // CorrectedBy
            // 
            this.CorrectedBy.DataPropertyName = "CorrectedBy";
            this.CorrectedBy.HeaderText = "CorrectedBy";
            this.CorrectedBy.Name = "CorrectedBy";
            this.CorrectedBy.ReadOnly = true;
            this.CorrectedBy.Width = 25;
            // 
            // Vendor
            // 
            this.Vendor.DataPropertyName = "Vendor";
            this.Vendor.HeaderText = "Vendor";
            this.Vendor.Name = "Vendor";
            this.Vendor.ReadOnly = true;
            this.Vendor.Width = 25;
            // 
            // DieTrial
            // 
            this.DieTrial.DataPropertyName = "DieTrial";
            this.DieTrial.HeaderText = "DieTrial";
            this.DieTrial.Name = "DieTrial";
            this.DieTrial.ReadOnly = true;
            this.DieTrial.Width = 25;
            // 
            // Production
            // 
            this.Production.DataPropertyName = "Production";
            this.Production.HeaderText = "Production";
            this.Production.Name = "Production";
            this.Production.ReadOnly = true;
            this.Production.Width = 25;
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1134, 511);
            this.Controls.Add(this.tblProdDieInspDataGridView);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form4";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "Search Production Die Inspection Records";
            this.Load += new System.EventHandler(this.Form4_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tblProdDieInspDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblProdDieInspBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gEI_ExtrusionDataSet2)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button button2;
        private GEI_ExtrusionDataSet2 gEI_ExtrusionDataSet2;
        private System.Windows.Forms.BindingSource tblProdDieInspBindingSource;
        private GEI_ExtrusionDataSet2TableAdapters.tblProdDieInspTableAdapter tblProdDieInspTableAdapter;
        private GEI_ExtrusionDataSet2TableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView tblProdDieInspDataGridView;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Weld_scrap;
        private System.Windows.Forms.DataGridViewTextBoxColumn Weld_scrap_comments;
        private System.Windows.Forms.DataGridViewTextBoxColumn Coring;
        private System.Windows.Forms.DataGridViewTextBoxColumn Coring_comments;
        private System.Windows.Forms.DataGridViewTextBoxColumn TB;
        private System.Windows.Forms.DataGridViewTextBoxColumn TB_comments;
        private System.Windows.Forms.DataGridViewTextBoxColumn Comments;
        private System.Windows.Forms.DataGridViewTextBoxColumn RejComments;
        private System.Windows.Forms.DataGridViewTextBoxColumn InspBy;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn CorrectedBy;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vendor;
        private System.Windows.Forms.DataGridViewCheckBoxColumn DieTrial;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Production;
    }
}