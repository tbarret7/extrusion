﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data.SqlTypes;
using Oracle.ManagedDataAccess.Client;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Security.Principal;
using System.Security.Authentication;
using System.Drawing.Printing;
using System.IO;

namespace ExtrusionOneStop
{
    public partial class ExtDieInspection : Form
    {
        public ExtDieInspection()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            

        }

        private void Button9_Click(object sender, EventArgs e)
        {
            string geiDieNum = textBox28.Text;
            string geiDieSuff = textBox24.Text;

            string connString = System.Configuration.ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString.ToString();
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand("Select Die_Number, Die_Suffix, Vendor, RecDate, DueDate, Rev, Hardness, ODThick, Taper, LocPins, Stamp, RelClear, " +
                "BearDrop, Support, BolClear, DOpenPin, ScrewBossOD, WeldCham, EntryPort, BolKeyway, LiftHole, VisApp, BackClear, Comments, Status, RejComments, " +
                "InspBy, CurrentDate, DieSuper, DieDifficulty, ExpediteFee, DieCost, NewDieToVendor from tblExtDieInsp WHERE Die_Number = '"
                + geiDieNum + "' and Die_Suffix = '" + geiDieSuff + "'", conn);
            SqlDataReader myreader;

            conn.Open();

            myreader = cmd.ExecuteReader();

            while (myreader.Read())
            {
                try
                {
                    textBox1.Text = (myreader["Die_Number"].ToString());
                    textBox2.Text = (myreader["Die_Suffix"].ToString());
                    textBox4.Text = (myreader["RecDate"].ToString()); 
                    textBox5.Text = (myreader["DueDate"].ToString());
                    textBox6.Text = (myreader["DieCost"].ToString());

                    comboBox1.Text = (myreader["Vendor"].ToString());
                    comboBox2.Text = (myreader["NewDieToVendor"].ToString());
                    comboBox3.Text = (myreader["ExpediteFee"].ToString());
                    comboBox4.Text = (myreader["DieDifficulty"].ToString());
                    comboBox5.Text = (myreader["Status"].ToString());

                    textBox7.Text = (myreader["Hardness"].ToString());
                    textBox8.Text = (myreader["ODThick"].ToString());
                    textBox9.Text = (myreader["Taper"].ToString());
                    textBox10.Text = (myreader["LocPins"].ToString());
                    textBox11.Text = (myreader["Stamp"].ToString());
                    textBox12.Text = (myreader["RelClear"].ToString());
                    textBox13.Text = (myreader["BearDrop"].ToString());
                    textBox14.Text = (myreader["Support"].ToString());
                    textBox15.Text = (myreader["BolClear"].ToString());
                    textBox16.Text = (myreader["DOpenPin"].ToString());
                    textBox17.Text = (myreader["ScrewBossOD"].ToString());
                    textBox18.Text = (myreader["WeldCham"].ToString());
                    textBox19.Text = (myreader["EntryPort"].ToString());
                    textBox20.Text = (myreader["BolKeyway"].ToString());
                    textBox21.Text = (myreader["LiftHole"].ToString());
                    textBox22.Text = (myreader["VisApp"].ToString());
                    textBox23.Text = (myreader["BackClear"].ToString());
                    textBox25.Text = (myreader["Comments"].ToString());
                    textBox26.Text = (myreader["RejComments"].ToString());
                    
                    textBox29.Text = (myreader["InspBy"].ToString());
                    textBox30.Text = (myreader["DieSuper"].ToString());
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                

            }
            conn.Close();
            myreader.Close();
            cmd.Dispose();
        }

       
        private void Button1_Click(object sender, EventArgs e)
        {

            string geiDieNum = textBox1.Text;
            string geiDieSuff = textBox2.Text;
            string geiVendor = comboBox1.Text.ToString();
            string currentDate = "";
            currentDate = DateTime.Now.ToString("MM/dd/yyyy HH:mm");
            string recDate = textBox4.Text;
            string dueDate = textBox5.Text;
            string dieCost = textBox6.Text;
            string newDieToVendor = comboBox2.Text.ToString();
            string expediteFee = comboBox3.Text.ToString();
            string diffLevel = comboBox4.Text.ToString();
            string hardness = textBox7.Text;
            string ODThick = textBox8.Text;
            string taper = textBox9.Text;
            string locPins = textBox10.Text;
            string stampings = textBox11.Text;
            string relClear = textBox12.Text;
            string bearDrops = textBox13.Text;
            string support = textBox14.Text;
            string bolClear = textBox15.Text;
            string DOpenPin = textBox16.Text;
            string screwBossOD = textBox17.Text;
            string weldCham = textBox18.Text;
            string entryPorts = textBox19.Text;
            string bolKeyWay = textBox20.Text;
            string liftHoles = textBox21.Text;
            string visApp = textBox22.Text;
            string backClear = textBox23.Text;
            string status = comboBox5.Text.ToString();
            string comments = textBox25.Text;
            string rejComments = textBox26.Text;
            string dieSuper = textBox30.Text;
            string inspBy = textBox29.Text;

            try
            {
                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString.ToString();
                SqlConnection conn = new SqlConnection(connString);
                SqlCommand cmd = new SqlCommand("Insert into tblExtDieInsp (Die_Number, Die_Suffix, Vendor, RecDate, DueDate, Hardness, ODThick, Taper, LocPins, Stamp, RelClear, " +
                "BearDrop, Support, BolClear, DOpenPin, ScrewBossOD, WeldCham, EntryPort, BolKeyway, LiftHole, VisApp, BackClear, Comments, Status, RejComments, " +
                "InspBy, CurrentDate, DieSuper, DieDifficulty, ExpediteFee, DieCost, NewDieToVendor) Select '" + geiDieNum + "' as Die_Number_Ins, '"
                + geiDieSuff + "' as Die_Suffix_Ins, '" + geiVendor + "' as Vendor_Ins, '" + recDate + "' as RecDate_Ins, '" + dueDate + "' as DueDate_Ins, '"
                + hardness + "' as Hardness_Ins, '" + ODThick + "' as ODThick_Ins, '" + taper + "' as Taper_Ins, '" + locPins + "' as LocPins_Ins, '"
                + stampings + "' as Stamp_Ins, '" + relClear + "' as RelClear_Ins, '" + bearDrops + "' as BearDrop_Ins, '" + support + "' as Support_Ins, '"
                + bolClear + "' as BolClear_Ins, '" + DOpenPin + "' as DOpenPin_Ins, '" + screwBossOD + "' as ScrewBossOD_Ins, '" + weldCham + "' as WeldCham_Ins, '"
                + entryPorts + "' as EntryPort_Ins, '" + bolKeyWay + "' as BolKeyway_Ins, '" + liftHoles + "' as LiftHole_Ins, '" + visApp + "' as VisApp_Ins, '" 
                + backClear + "' as BackClear_Ins, '" + comments + "' as Comments_Ins, '" + status + "' as Status_Ins, '" + rejComments + "' as RejComments_Ins, '"
                + inspBy + "' as InspBy_Ins, '" + currentDate + "' as CurrentDate_Ins, '" + dieSuper + "' as DieSuper_Ins, '" + diffLevel + "' as DieDifficulty_Ins, '"
                + expediteFee + "' as ExpediteFee_Ins, '" + dieCost + "' as DieCost_Ins, '" + newDieToVendor + "' as NewDieToVendor_Ins", conn);

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable table = new DataTable();

                conn.Open();
                adapter.Fill(table);
                conn.Close();

                MessageBox.Show("You have succesfully submitted this record.");
                

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

            }


        }
    }
}
