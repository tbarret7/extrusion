﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data.SqlTypes;
using Oracle.ManagedDataAccess.Client;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Security.Principal;
using System.Security.Authentication;
using System.Drawing.Printing;
using System.IO;

namespace ExtrusionOneStop
{
    public partial class DieRingInspection : Form
    {
        public DieRingInspection()
        {
            InitializeComponent();
        }
        private static DataTable GetData(string sqlCommand, string ring)
        {
            string geiDieRing = ring;
            string connectionString = @"Data Source = 160.106.20.239,1433\sqlexpress; Initial Catalog = GEI_Extrusion; Integrated Security = True";

            SqlConnection conn = new SqlConnection(connectionString);

            SqlCommand command = new SqlCommand(sqlCommand, conn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;

            DataTable table = new DataTable();
            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
            command.Parameters.Add("@geiDieRing", geiDieRing);
            

            adapter.Fill(table);

            return table;
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            
            DateTime thisDay = DateTime.Today;
            textBox12.Text = thisDay.ToShortDateString();
        }

        private void Button9_Click(object sender, EventArgs e)
        {
            string geiDieRing = textBox28.Text + "-" + textBox24.Text;
            button2.Hide();
            
            try
            {
                tblDieRingDataGridView.DataSource = GetData("SELECT Rec_id, Die_Ring, Vendor, Date, Hardness, Thickness, Diameters, " +
                "Keyway, DoveTailFeature, CriticalDim, Comments, RejComments, InspBy, " +
                "CurrentDate FROM dbo.tblDieRing WHERE Die_Ring = @geiDieRing", geiDieRing);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }
                      
        private void TblDieRingDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                textBox1.Text = tblDieRingDataGridView.CurrentRow.Cells[0].Value.ToString();
                textBox2.Text = tblDieRingDataGridView.CurrentRow.Cells[1].Value.ToString();
                comboBox1.Text = tblDieRingDataGridView.CurrentRow.Cells[2].Value.ToString();
                textBox3.Text = tblDieRingDataGridView.CurrentRow.Cells[3].Value.ToString();
                textBox4.Text = tblDieRingDataGridView.CurrentRow.Cells[4].Value.ToString();
                textBox5.Text = tblDieRingDataGridView.CurrentRow.Cells[6].Value.ToString();
                textBox6.Text = tblDieRingDataGridView.CurrentRow.Cells[8].Value.ToString();
                textBox7.Text = tblDieRingDataGridView.CurrentRow.Cells[5].Value.ToString();
                textBox8.Text = tblDieRingDataGridView.CurrentRow.Cells[7].Value.ToString();
                textBox9.Text = tblDieRingDataGridView.CurrentRow.Cells[9].Value.ToString();
                textBox10.Text = tblDieRingDataGridView.CurrentRow.Cells[10].Value.ToString();
                textBox11.Text = tblDieRingDataGridView.CurrentRow.Cells[11].Value.ToString();
                textBox13.Text = tblDieRingDataGridView.CurrentRow.Cells[12].Value.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void TblDieRingDataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                SelectNextControl(tblDieRingDataGridView, true, true, true, true);
                // or Parent.SelectNextControl() if the grid is an only child, etc.
                e.Handled = true;
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            string geiDieRing = textBox2.Text;
            string vendor = comboBox1.Text;
            string rdate = textBox3.Text;
            string hardness = textBox4.Text;
            string diameters = textBox5.Text;
            string dtf = textBox6.Text;
            string thickness = textBox7.Text;
            string keyway = textBox8.Text;
            string critdim = textBox9.Text;
            string currDate = textBox12.Text;
            string inspby = textBox13.Text;
            string recid = textBox1.Text;
            string comments = textBox10.Text;
            string rejComments = textBox11.Text;
            try
            {


                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString.ToString();
                SqlConnection conn = new SqlConnection(connString);
                SqlCommand cmd = new SqlCommand("Update tblDieRing SET Die_Ring=@dr, Vendor=@vendor, Date=@rdate, Hardness=@hard, Thickness=@thick, " +
                    "Diameters=@diam, Keyway=@keyway, DoveTailFeature=@dtf, CriticalDim=@critdim, Comments=@comm, RejComments=@rejcomm, InspBy=@inspby, " +
                    "CurrentDate=@currdate WHERE Rec_id = '" + recid + "'", conn);

                cmd.Parameters.AddWithValue("@dr", geiDieRing);
                cmd.Parameters.AddWithValue("@vendor", vendor);
                cmd.Parameters.AddWithValue("@rdate", rdate);
                cmd.Parameters.AddWithValue("@hard", hardness);
                cmd.Parameters.AddWithValue("@thick", thickness);
                cmd.Parameters.AddWithValue("@diam", diameters);
                cmd.Parameters.AddWithValue("@keyway", keyway);
                cmd.Parameters.AddWithValue("@dtf", dtf);
                cmd.Parameters.AddWithValue("@critdim", critdim);
                cmd.Parameters.AddWithValue("@comm", comments);
                cmd.Parameters.AddWithValue("@rejcomm", rejComments);
                cmd.Parameters.AddWithValue("@inspby", inspby);
                cmd.Parameters.AddWithValue("@currdate", currDate);

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable table = new DataTable();

                conn.Open();
                adapter.Fill(table);
                conn.Close();
                MessageBox.Show("You have successfully updated this Die Ring record");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            string geiDieRing = textBox2.Text;
            string vendor = comboBox1.Text;
            string rdate = textBox3.Text;
            string hardness = textBox4.Text;
            string diameters = textBox5.Text;
            string dtf = textBox6.Text;
            string thickness = textBox7.Text;
            string keyway = textBox8.Text;
            string critdim = textBox9.Text;
            string currDate = textBox12.Text;
            string inspby = textBox13.Text;
            string recid = textBox1.Text;
            string comments = textBox10.Text;
            string rejComments = textBox11.Text;
            try
            {
                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString.ToString();
                SqlConnection conn = new SqlConnection(connString);
                SqlCommand cmd = new SqlCommand("Insert into tblDieRing (Die_Ring, Vendor, Date, Hardness, Thickness, " +
                    "Diameters, Keyway, DoveTailFeature, CriticalDim, Comments, RejComments, InspBy, " +
                    "CurrentDate) Select '" + geiDieRing + "' as Die_Ring_Ins, '" + vendor + "' as Vendor_Ins, '"
                    + rdate + "' as Date_Ins, '" + hardness + "' as Hardness_Ins, '" + thickness + "' as Thickness_Ins, '" 
                    + diameters + "' as Diameters_Ins, '" + keyway + "' as Keyway_Ins, '" + dtf + "' as DoveTailFeature_Ins, '"
                    + critdim + "' as CriticalDim_Ins, '" + comments + "' as Comments_Ins, '" + rejComments + "' as RejComments_Ins, '"
                    + inspby + "' as InspBy_Ins, '" + currDate + "' as CurrentDate_Ins", conn);

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable table = new DataTable();

                conn.Open();
                adapter.Fill(table);
                conn.Close();
                MessageBox.Show("You have successfully input a new Die Ring record");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void GroupBox2_Enter(object sender, EventArgs e)
        {

        }
    }
}
