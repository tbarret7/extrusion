﻿namespace ExtrusionOneStop
{
    partial class DieRecipe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DieRecipe));
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button9 = new System.Windows.Forms.Button();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button8 = new System.Windows.Forms.Button();
            this.checkBox101 = new System.Windows.Forms.CheckBox();
            this.button3 = new System.Windows.Forms.Button();
            this.checkBox102 = new System.Windows.Forms.CheckBox();
            this.button7 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.checkBox103 = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBox104 = new System.Windows.Forms.CheckBox();
            this.button6 = new System.Windows.Forms.Button();
            this.checkBox105 = new System.Windows.Forms.CheckBox();
            this.checkBox106 = new System.Windows.Forms.CheckBox();
            this.checkBox107 = new System.Windows.Forms.CheckBox();
            this.button5 = new System.Windows.Forms.Button();
            this.checkBox108 = new System.Windows.Forms.CheckBox();
            this.checkBox109 = new System.Windows.Forms.CheckBox();
            this.checkBox110 = new System.Windows.Forms.CheckBox();
            this.button4 = new System.Windows.Forms.Button();
            this.checkBox111 = new System.Windows.Forms.CheckBox();
            this.checkBox112 = new System.Windows.Forms.CheckBox();
            this.checkBox113 = new System.Windows.Forms.CheckBox();
            this.checkBox114 = new System.Windows.Forms.CheckBox();
            this.checkBox115 = new System.Windows.Forms.CheckBox();
            this.checkBox116 = new System.Windows.Forms.CheckBox();
            this.checkBox117 = new System.Windows.Forms.CheckBox();
            this.checkBox118 = new System.Windows.Forms.CheckBox();
            this.checkBox119 = new System.Windows.Forms.CheckBox();
            this.checkBox120 = new System.Windows.Forms.CheckBox();
            this.checkBox81 = new System.Windows.Forms.CheckBox();
            this.checkBox82 = new System.Windows.Forms.CheckBox();
            this.checkBox83 = new System.Windows.Forms.CheckBox();
            this.checkBox84 = new System.Windows.Forms.CheckBox();
            this.checkBox85 = new System.Windows.Forms.CheckBox();
            this.checkBox86 = new System.Windows.Forms.CheckBox();
            this.checkBox87 = new System.Windows.Forms.CheckBox();
            this.checkBox88 = new System.Windows.Forms.CheckBox();
            this.checkBox89 = new System.Windows.Forms.CheckBox();
            this.checkBox90 = new System.Windows.Forms.CheckBox();
            this.checkBox91 = new System.Windows.Forms.CheckBox();
            this.checkBox92 = new System.Windows.Forms.CheckBox();
            this.checkBox93 = new System.Windows.Forms.CheckBox();
            this.checkBox94 = new System.Windows.Forms.CheckBox();
            this.checkBox95 = new System.Windows.Forms.CheckBox();
            this.checkBox96 = new System.Windows.Forms.CheckBox();
            this.checkBox97 = new System.Windows.Forms.CheckBox();
            this.checkBox98 = new System.Windows.Forms.CheckBox();
            this.checkBox99 = new System.Windows.Forms.CheckBox();
            this.checkBox100 = new System.Windows.Forms.CheckBox();
            this.checkBox61 = new System.Windows.Forms.CheckBox();
            this.checkBox62 = new System.Windows.Forms.CheckBox();
            this.checkBox63 = new System.Windows.Forms.CheckBox();
            this.checkBox64 = new System.Windows.Forms.CheckBox();
            this.checkBox65 = new System.Windows.Forms.CheckBox();
            this.checkBox66 = new System.Windows.Forms.CheckBox();
            this.checkBox67 = new System.Windows.Forms.CheckBox();
            this.checkBox68 = new System.Windows.Forms.CheckBox();
            this.checkBox69 = new System.Windows.Forms.CheckBox();
            this.checkBox70 = new System.Windows.Forms.CheckBox();
            this.checkBox71 = new System.Windows.Forms.CheckBox();
            this.checkBox72 = new System.Windows.Forms.CheckBox();
            this.checkBox73 = new System.Windows.Forms.CheckBox();
            this.checkBox74 = new System.Windows.Forms.CheckBox();
            this.checkBox75 = new System.Windows.Forms.CheckBox();
            this.checkBox76 = new System.Windows.Forms.CheckBox();
            this.checkBox77 = new System.Windows.Forms.CheckBox();
            this.checkBox78 = new System.Windows.Forms.CheckBox();
            this.checkBox79 = new System.Windows.Forms.CheckBox();
            this.checkBox80 = new System.Windows.Forms.CheckBox();
            this.checkBox41 = new System.Windows.Forms.CheckBox();
            this.checkBox42 = new System.Windows.Forms.CheckBox();
            this.checkBox43 = new System.Windows.Forms.CheckBox();
            this.checkBox44 = new System.Windows.Forms.CheckBox();
            this.checkBox45 = new System.Windows.Forms.CheckBox();
            this.checkBox46 = new System.Windows.Forms.CheckBox();
            this.checkBox47 = new System.Windows.Forms.CheckBox();
            this.checkBox48 = new System.Windows.Forms.CheckBox();
            this.checkBox49 = new System.Windows.Forms.CheckBox();
            this.checkBox50 = new System.Windows.Forms.CheckBox();
            this.checkBox51 = new System.Windows.Forms.CheckBox();
            this.checkBox52 = new System.Windows.Forms.CheckBox();
            this.checkBox53 = new System.Windows.Forms.CheckBox();
            this.checkBox54 = new System.Windows.Forms.CheckBox();
            this.checkBox55 = new System.Windows.Forms.CheckBox();
            this.checkBox56 = new System.Windows.Forms.CheckBox();
            this.checkBox57 = new System.Windows.Forms.CheckBox();
            this.checkBox58 = new System.Windows.Forms.CheckBox();
            this.checkBox59 = new System.Windows.Forms.CheckBox();
            this.checkBox60 = new System.Windows.Forms.CheckBox();
            this.checkBox21 = new System.Windows.Forms.CheckBox();
            this.checkBox22 = new System.Windows.Forms.CheckBox();
            this.checkBox23 = new System.Windows.Forms.CheckBox();
            this.checkBox24 = new System.Windows.Forms.CheckBox();
            this.checkBox25 = new System.Windows.Forms.CheckBox();
            this.checkBox26 = new System.Windows.Forms.CheckBox();
            this.checkBox27 = new System.Windows.Forms.CheckBox();
            this.checkBox28 = new System.Windows.Forms.CheckBox();
            this.checkBox29 = new System.Windows.Forms.CheckBox();
            this.checkBox30 = new System.Windows.Forms.CheckBox();
            this.checkBox31 = new System.Windows.Forms.CheckBox();
            this.checkBox32 = new System.Windows.Forms.CheckBox();
            this.checkBox33 = new System.Windows.Forms.CheckBox();
            this.checkBox34 = new System.Windows.Forms.CheckBox();
            this.checkBox35 = new System.Windows.Forms.CheckBox();
            this.checkBox36 = new System.Windows.Forms.CheckBox();
            this.checkBox37 = new System.Windows.Forms.CheckBox();
            this.checkBox38 = new System.Windows.Forms.CheckBox();
            this.checkBox39 = new System.Windows.Forms.CheckBox();
            this.checkBox40 = new System.Windows.Forms.CheckBox();
            this.checkBox20 = new System.Windows.Forms.CheckBox();
            this.checkBox19 = new System.Windows.Forms.CheckBox();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.gEI_ExtrusionDataSet1 = new ExtrusionOneStop.GEI_ExtrusionDataSet1();
            this.tbl3DielogBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tbl3DielogTableAdapter = new ExtrusionOneStop.GEI_ExtrusionDataSet1TableAdapters.tbl3DielogTableAdapter();
            this.tableAdapterManager = new ExtrusionOneStop.GEI_ExtrusionDataSet1TableAdapters.TableAdapterManager();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button17 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gEI_ExtrusionDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbl3DielogBindingSource)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(95, 19);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 47;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(95, 71);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 49;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(95, 97);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 50;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(95, 126);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(100, 20);
            this.textBox5.TabIndex = 51;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(95, 152);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(100, 20);
            this.textBox6.TabIndex = 52;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(95, 181);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(100, 20);
            this.textBox7.TabIndex = 53;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(95, 207);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(100, 20);
            this.textBox8.TabIndex = 54;
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(95, 233);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(100, 20);
            this.textBox9.TabIndex = 55;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(310, 16);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(100, 20);
            this.textBox10.TabIndex = 64;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(310, 42);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(100, 20);
            this.textBox11.TabIndex = 65;
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(310, 68);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(100, 20);
            this.textBox12.TabIndex = 66;
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(310, 94);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(100, 20);
            this.textBox13.TabIndex = 67;
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(355, 499);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(100, 20);
            this.textBox14.TabIndex = 81;
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(355, 525);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(100, 20);
            this.textBox15.TabIndex = 82;
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(310, 123);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(100, 20);
            this.textBox16.TabIndex = 68;
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(310, 149);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(100, 20);
            this.textBox17.TabIndex = 69;
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(310, 178);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(100, 20);
            this.textBox18.TabIndex = 70;
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(310, 204);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(100, 20);
            this.textBox19.TabIndex = 71;
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(310, 230);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(100, 20);
            this.textBox20.TabIndex = 72;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox42);
            this.groupBox1.Controls.Add(this.textBox43);
            this.groupBox1.Controls.Add(this.textBox40);
            this.groupBox1.Controls.Add(this.textBox39);
            this.groupBox1.Controls.Add(this.textBox38);
            this.groupBox1.Controls.Add(this.textBox37);
            this.groupBox1.Controls.Add(this.textBox36);
            this.groupBox1.Controls.Add(this.textBox35);
            this.groupBox1.Controls.Add(this.textBox34);
            this.groupBox1.Controls.Add(this.textBox33);
            this.groupBox1.Controls.Add(this.textBox32);
            this.groupBox1.Controls.Add(this.textBox31);
            this.groupBox1.Controls.Add(this.textBox30);
            this.groupBox1.Controls.Add(this.textBox29);
            this.groupBox1.Controls.Add(this.textBox41);
            this.groupBox1.Controls.Add(this.label43);
            this.groupBox1.Controls.Add(this.label42);
            this.groupBox1.Controls.Add(this.label41);
            this.groupBox1.Controls.Add(this.label40);
            this.groupBox1.Controls.Add(this.label39);
            this.groupBox1.Controls.Add(this.label38);
            this.groupBox1.Controls.Add(this.label37);
            this.groupBox1.Controls.Add(this.label36);
            this.groupBox1.Controls.Add(this.label35);
            this.groupBox1.Controls.Add(this.label34);
            this.groupBox1.Controls.Add(this.label33);
            this.groupBox1.Controls.Add(this.label32);
            this.groupBox1.Controls.Add(this.label31);
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.button11);
            this.groupBox1.Controls.Add(this.button10);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.textBox21);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.textBox6);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.textBox20);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.textBox15);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.textBox7);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.textBox16);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.textBox14);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.textBox11);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.textBox5);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.textBox19);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.textBox8);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.textBox10);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.textBox17);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBox13);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBox12);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBox18);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBox9);
            this.groupBox1.Location = new System.Drawing.Point(187, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(506, 596);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Die Recipe";
            // 
            // textBox42
            // 
            this.textBox42.Location = new System.Drawing.Point(95, 443);
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(100, 20);
            this.textBox42.TabIndex = 63;
            // 
            // textBox43
            // 
            this.textBox43.Location = new System.Drawing.Point(310, 417);
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(100, 20);
            this.textBox43.TabIndex = 79;
            // 
            // textBox40
            // 
            this.textBox40.Location = new System.Drawing.Point(310, 362);
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new System.Drawing.Size(100, 20);
            this.textBox40.TabIndex = 77;
            // 
            // textBox39
            // 
            this.textBox39.Location = new System.Drawing.Point(310, 334);
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(100, 20);
            this.textBox39.TabIndex = 76;
            // 
            // textBox38
            // 
            this.textBox38.Location = new System.Drawing.Point(310, 308);
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(100, 20);
            this.textBox38.TabIndex = 75;
            // 
            // textBox37
            // 
            this.textBox37.Location = new System.Drawing.Point(310, 282);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(100, 20);
            this.textBox37.TabIndex = 74;
            // 
            // textBox36
            // 
            this.textBox36.Location = new System.Drawing.Point(310, 256);
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(100, 20);
            this.textBox36.TabIndex = 73;
            // 
            // textBox35
            // 
            this.textBox35.Location = new System.Drawing.Point(95, 418);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(100, 20);
            this.textBox35.TabIndex = 62;
            // 
            // textBox34
            // 
            this.textBox34.Location = new System.Drawing.Point(95, 392);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(100, 20);
            this.textBox34.TabIndex = 61;
            // 
            // textBox33
            // 
            this.textBox33.Location = new System.Drawing.Point(95, 366);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(100, 20);
            this.textBox33.TabIndex = 60;
            // 
            // textBox32
            // 
            this.textBox32.Location = new System.Drawing.Point(95, 340);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(100, 20);
            this.textBox32.TabIndex = 59;
            // 
            // textBox31
            // 
            this.textBox31.Location = new System.Drawing.Point(95, 314);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(100, 20);
            this.textBox31.TabIndex = 58;
            // 
            // textBox30
            // 
            this.textBox30.Location = new System.Drawing.Point(95, 288);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(100, 20);
            this.textBox30.TabIndex = 57;
            // 
            // textBox29
            // 
            this.textBox29.Location = new System.Drawing.Point(95, 262);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(100, 20);
            this.textBox29.TabIndex = 56;
            // 
            // textBox41
            // 
            this.textBox41.Location = new System.Drawing.Point(310, 388);
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(100, 20);
            this.textBox41.TabIndex = 78;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(20, 447);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(71, 13);
            this.label43.TabIndex = 63;
            this.label43.Text = "Expedite Y/N";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(255, 421);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(49, 13);
            this.label42.TabIndex = 62;
            this.label42.Text = "Die price";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(253, 392);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(53, 13);
            this.label41.TabIndex = 61;
            this.label41.Text = "Received";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(271, 366);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(27, 13);
            this.label40.TabIndex = 60;
            this.label40.Text = "Due";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(232, 337);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(69, 13);
            this.label39.TabIndex = 59;
            this.label39.Text = "Date ordered";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(265, 311);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(34, 13);
            this.label38.TabIndex = 58;
            this.label38.Text = "Temp";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(279, 284);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(20, 13);
            this.label37.TabIndex = 57;
            this.label37.Text = "FP";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(271, 259);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(28, 13);
            this.label36.TabIndex = 56;
            this.label36.Text = "BOL";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(61, 424);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(29, 13);
            this.label35.TabIndex = 55;
            this.label35.Text = "BKR";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(35, 395);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(54, 13);
            this.label34.TabIndex = 54;
            this.label34.Text = "Holes - int";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(59, 369);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(27, 13);
            this.label33.TabIndex = 53;
            this.label33.Text = "Size";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(47, 342);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(41, 13);
            this.label32.TabIndex = 52;
            this.label32.Text = "Vendor";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(37, 318);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(51, 13);
            this.label31.TabIndex = 51;
            this.label31.Text = "Customer";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(23, 291);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(67, 13);
            this.label30.TabIndex = 50;
            this.label30.Text = "Req Number";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(27, 264);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(62, 13);
            this.label29.TabIndex = 49;
            this.label29.Text = "PO Number";
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(393, 551);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(100, 34);
            this.button11.TabIndex = 84;
            this.button11.Text = "Update";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.Button11_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(287, 551);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(100, 34);
            this.button10.TabIndex = 83;
            this.button10.Text = "Submit";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.Button10_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "A73003",
            "A76005",
            "A76060",
            "A76061",
            "A76063",
            "A76063HS",
            "A76065",
            "A76101",
            "A76463",
            "A76560",
            "A76663"});
            this.comboBox1.Location = new System.Drawing.Point(95, 45);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(100, 21);
            this.comboBox1.TabIndex = 48;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(14, 486);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(56, 13);
            this.label21.TabIndex = 42;
            this.label21.Text = "Comments";
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(11, 502);
            this.textBox21.Multiline = true;
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(259, 80);
            this.textBox21.TabIndex = 80;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(225, 233);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(81, 13);
            this.label20.TabIndex = 40;
            this.label20.Text = "Speed Chg Pos";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(238, 207);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(61, 13);
            this.label19.TabIndex = 39;
            this.label19.Text = "Ramp Time";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(230, 181);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(71, 13);
            this.label18.TabIndex = 38;
            this.label18.Text = "Ramp Start %";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(238, 152);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(65, 13);
            this.label17.TabIndex = 37;
            this.label17.Text = "Ext Speed 2";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(227, 126);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(81, 13);
            this.label16.TabIndex = 36;
            this.label16.Text = "Ext Speed Goal";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(284, 528);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(64, 13);
            this.label15.TabIndex = 35;
            this.label15.Text = "Changed by";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(273, 502);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(75, 13);
            this.label14.TabIndex = 34;
            this.label14.Text = "Date changed";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(242, 97);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 13);
            this.label13.TabIndex = 33;
            this.label13.Text = "Butt Length";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(200, 71);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(104, 13);
            this.label12.TabIndex = 32;
            this.label12.Text = "Break Thru Pressure";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(214, 45);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(94, 13);
            this.label11.TabIndex = 31;
            this.label11.Text = "Weld to Die Scrap";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(210, 19);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(99, 13);
            this.label10.TabIndex = 30;
            this.label10.Text = "Saw to Weld Scrap";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(24, 236);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 13);
            this.label9.TabIndex = 29;
            this.label9.Text = "Fan Percent";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(46, 210);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 28;
            this.label8.Text = "Curves";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 184);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 13);
            this.label7.TabIndex = 27;
            this.label7.Text = "Container Temp";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(28, 155);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "Billet Length";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(30, 129);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "Billet Temp";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(33, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Ext Speed";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Date Entered";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(57, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Alloy";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Die Num";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBox2);
            this.groupBox3.Controls.Add(this.button9);
            this.groupBox3.Controls.Add(this.textBox28);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Location = new System.Drawing.Point(1, 290);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(180, 119);
            this.groupBox3.TabIndex = 45;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Search";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(111, 21);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(44, 20);
            this.textBox2.TabIndex = 45;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(63, 55);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(77, 38);
            this.button9.TabIndex = 46;
            this.button9.Text = "Search";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.Button9_Click);
            // 
            // textBox28
            // 
            this.textBox28.Location = new System.Drawing.Point(63, 21);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(44, 20);
            this.textBox28.TabIndex = 43;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(9, 24);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(48, 13);
            this.label28.TabIndex = 44;
            this.label28.Text = "Die Num";
            // 
            // textBox22
            // 
            this.textBox22.Location = new System.Drawing.Point(117, 421);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(66, 20);
            this.textBox22.TabIndex = 22;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(195, 424);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(85, 13);
            this.label24.TabIndex = 51;
            this.label24.Text = "Lower Air Speed";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(390, 450);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(129, 13);
            this.label27.TabIndex = 54;
            this.label27.Text = "Water Pump Dead Speed";
            // 
            // textBox27
            // 
            this.textBox27.Location = new System.Drawing.Point(525, 447);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(66, 20);
            this.textBox27.TabIndex = 27;
            // 
            // textBox24
            // 
            this.textBox24.Location = new System.Drawing.Point(315, 421);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(66, 20);
            this.textBox24.TabIndex = 24;
            // 
            // textBox26
            // 
            this.textBox26.Location = new System.Drawing.Point(525, 421);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(66, 20);
            this.textBox26.TabIndex = 26;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(390, 425);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(100, 13);
            this.label26.TabIndex = 53;
            this.label26.Text = "Water Pump Speed";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(3, 450);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(114, 13);
            this.label23.TabIndex = 50;
            this.label23.Text = "Upper Air Dead Speed";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(3, 428);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(85, 13);
            this.label22.TabIndex = 49;
            this.label22.Text = "Upper Air Speed";
            // 
            // textBox23
            // 
            this.textBox23.Location = new System.Drawing.Point(117, 447);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(66, 20);
            this.textBox23.TabIndex = 23;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(195, 450);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(114, 13);
            this.label25.TabIndex = 52;
            this.label25.Text = "Lower Air Dead Speed";
            // 
            // textBox25
            // 
            this.textBox25.Location = new System.Drawing.Point(315, 447);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(66, 20);
            this.textBox25.TabIndex = 25;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button8);
            this.groupBox2.Controls.Add(this.checkBox101);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.checkBox102);
            this.groupBox2.Controls.Add(this.button7);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.checkBox103);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.checkBox104);
            this.groupBox2.Controls.Add(this.button6);
            this.groupBox2.Controls.Add(this.checkBox105);
            this.groupBox2.Controls.Add(this.checkBox106);
            this.groupBox2.Controls.Add(this.checkBox107);
            this.groupBox2.Controls.Add(this.button5);
            this.groupBox2.Controls.Add(this.checkBox108);
            this.groupBox2.Controls.Add(this.checkBox109);
            this.groupBox2.Controls.Add(this.checkBox110);
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.checkBox111);
            this.groupBox2.Controls.Add(this.checkBox112);
            this.groupBox2.Controls.Add(this.checkBox113);
            this.groupBox2.Controls.Add(this.checkBox114);
            this.groupBox2.Controls.Add(this.checkBox115);
            this.groupBox2.Controls.Add(this.checkBox116);
            this.groupBox2.Controls.Add(this.checkBox117);
            this.groupBox2.Controls.Add(this.checkBox118);
            this.groupBox2.Controls.Add(this.checkBox119);
            this.groupBox2.Controls.Add(this.checkBox120);
            this.groupBox2.Controls.Add(this.checkBox81);
            this.groupBox2.Controls.Add(this.checkBox82);
            this.groupBox2.Controls.Add(this.checkBox83);
            this.groupBox2.Controls.Add(this.checkBox84);
            this.groupBox2.Controls.Add(this.checkBox85);
            this.groupBox2.Controls.Add(this.checkBox86);
            this.groupBox2.Controls.Add(this.checkBox87);
            this.groupBox2.Controls.Add(this.checkBox88);
            this.groupBox2.Controls.Add(this.checkBox89);
            this.groupBox2.Controls.Add(this.checkBox90);
            this.groupBox2.Controls.Add(this.checkBox91);
            this.groupBox2.Controls.Add(this.checkBox92);
            this.groupBox2.Controls.Add(this.checkBox93);
            this.groupBox2.Controls.Add(this.checkBox94);
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.textBox27);
            this.groupBox2.Controls.Add(this.textBox22);
            this.groupBox2.Controls.Add(this.textBox26);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.checkBox95);
            this.groupBox2.Controls.Add(this.checkBox96);
            this.groupBox2.Controls.Add(this.textBox24);
            this.groupBox2.Controls.Add(this.checkBox97);
            this.groupBox2.Controls.Add(this.checkBox98);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.checkBox99);
            this.groupBox2.Controls.Add(this.textBox25);
            this.groupBox2.Controls.Add(this.checkBox100);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.checkBox61);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.checkBox62);
            this.groupBox2.Controls.Add(this.textBox23);
            this.groupBox2.Controls.Add(this.checkBox63);
            this.groupBox2.Controls.Add(this.checkBox64);
            this.groupBox2.Controls.Add(this.checkBox65);
            this.groupBox2.Controls.Add(this.checkBox66);
            this.groupBox2.Controls.Add(this.checkBox67);
            this.groupBox2.Controls.Add(this.checkBox68);
            this.groupBox2.Controls.Add(this.checkBox69);
            this.groupBox2.Controls.Add(this.checkBox70);
            this.groupBox2.Controls.Add(this.checkBox71);
            this.groupBox2.Controls.Add(this.checkBox72);
            this.groupBox2.Controls.Add(this.checkBox73);
            this.groupBox2.Controls.Add(this.checkBox74);
            this.groupBox2.Controls.Add(this.checkBox75);
            this.groupBox2.Controls.Add(this.checkBox76);
            this.groupBox2.Controls.Add(this.checkBox77);
            this.groupBox2.Controls.Add(this.checkBox78);
            this.groupBox2.Controls.Add(this.checkBox79);
            this.groupBox2.Controls.Add(this.checkBox80);
            this.groupBox2.Controls.Add(this.checkBox41);
            this.groupBox2.Controls.Add(this.checkBox42);
            this.groupBox2.Controls.Add(this.checkBox43);
            this.groupBox2.Controls.Add(this.checkBox44);
            this.groupBox2.Controls.Add(this.checkBox45);
            this.groupBox2.Controls.Add(this.checkBox46);
            this.groupBox2.Controls.Add(this.checkBox47);
            this.groupBox2.Controls.Add(this.checkBox48);
            this.groupBox2.Controls.Add(this.checkBox49);
            this.groupBox2.Controls.Add(this.checkBox50);
            this.groupBox2.Controls.Add(this.checkBox51);
            this.groupBox2.Controls.Add(this.checkBox52);
            this.groupBox2.Controls.Add(this.checkBox53);
            this.groupBox2.Controls.Add(this.checkBox54);
            this.groupBox2.Controls.Add(this.checkBox55);
            this.groupBox2.Controls.Add(this.checkBox56);
            this.groupBox2.Controls.Add(this.checkBox57);
            this.groupBox2.Controls.Add(this.checkBox58);
            this.groupBox2.Controls.Add(this.checkBox59);
            this.groupBox2.Controls.Add(this.checkBox60);
            this.groupBox2.Controls.Add(this.checkBox21);
            this.groupBox2.Controls.Add(this.checkBox22);
            this.groupBox2.Controls.Add(this.checkBox23);
            this.groupBox2.Controls.Add(this.checkBox24);
            this.groupBox2.Controls.Add(this.checkBox25);
            this.groupBox2.Controls.Add(this.checkBox26);
            this.groupBox2.Controls.Add(this.checkBox27);
            this.groupBox2.Controls.Add(this.checkBox28);
            this.groupBox2.Controls.Add(this.checkBox29);
            this.groupBox2.Controls.Add(this.checkBox30);
            this.groupBox2.Controls.Add(this.checkBox31);
            this.groupBox2.Controls.Add(this.checkBox32);
            this.groupBox2.Controls.Add(this.checkBox33);
            this.groupBox2.Controls.Add(this.checkBox34);
            this.groupBox2.Controls.Add(this.checkBox35);
            this.groupBox2.Controls.Add(this.checkBox36);
            this.groupBox2.Controls.Add(this.checkBox37);
            this.groupBox2.Controls.Add(this.checkBox38);
            this.groupBox2.Controls.Add(this.checkBox39);
            this.groupBox2.Controls.Add(this.checkBox40);
            this.groupBox2.Controls.Add(this.checkBox20);
            this.groupBox2.Controls.Add(this.checkBox19);
            this.groupBox2.Controls.Add(this.checkBox18);
            this.groupBox2.Controls.Add(this.checkBox17);
            this.groupBox2.Controls.Add(this.checkBox16);
            this.groupBox2.Controls.Add(this.checkBox15);
            this.groupBox2.Controls.Add(this.checkBox14);
            this.groupBox2.Controls.Add(this.checkBox13);
            this.groupBox2.Controls.Add(this.checkBox12);
            this.groupBox2.Controls.Add(this.checkBox11);
            this.groupBox2.Controls.Add(this.checkBox10);
            this.groupBox2.Controls.Add(this.checkBox9);
            this.groupBox2.Controls.Add(this.checkBox8);
            this.groupBox2.Controls.Add(this.checkBox7);
            this.groupBox2.Controls.Add(this.checkBox6);
            this.groupBox2.Controls.Add(this.checkBox5);
            this.groupBox2.Controls.Add(this.checkBox4);
            this.groupBox2.Controls.Add(this.checkBox3);
            this.groupBox2.Controls.Add(this.checkBox2);
            this.groupBox2.Controls.Add(this.checkBox1);
            this.groupBox2.Location = new System.Drawing.Point(699, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(656, 604);
            this.groupBox2.TabIndex = 55;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Parameters";
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(489, 528);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(111, 46);
            this.button8.TabIndex = 62;
            this.button8.Text = "LZ Odd Dead On";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.Button8_Click);
            // 
            // checkBox101
            // 
            this.checkBox101.AutoSize = true;
            this.checkBox101.Location = new System.Drawing.Point(447, 226);
            this.checkBox101.Name = "checkBox101";
            this.checkBox101.Size = new System.Drawing.Size(83, 17);
            this.checkBox101.TabIndex = 128;
            this.checkBox101.Text = "LZ21 Water";
            this.checkBox101.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(351, 474);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(111, 45);
            this.button3.TabIndex = 57;
            this.button3.Text = "UZ Odd Water On";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // checkBox102
            // 
            this.checkBox102.AutoSize = true;
            this.checkBox102.Location = new System.Drawing.Point(447, 245);
            this.checkBox102.Name = "checkBox102";
            this.checkBox102.Size = new System.Drawing.Size(80, 17);
            this.checkBox102.TabIndex = 129;
            this.checkBox102.Text = "LZ21 Dead";
            this.checkBox102.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(351, 528);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(111, 46);
            this.button7.TabIndex = 61;
            this.button7.Text = "LZ Odd Water On";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.Button7_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(213, 473);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(111, 46);
            this.button2.TabIndex = 56;
            this.button2.Text = "UZ Even Dead On";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // checkBox103
            // 
            this.checkBox103.AutoSize = true;
            this.checkBox103.Location = new System.Drawing.Point(447, 262);
            this.checkBox103.Name = "checkBox103";
            this.checkBox103.Size = new System.Drawing.Size(83, 17);
            this.checkBox103.TabIndex = 130;
            this.checkBox103.Text = "LZ22 Water";
            this.checkBox103.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(76, 473);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 46);
            this.button1.TabIndex = 55;
            this.button1.Text = "UZ Even Water On";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // checkBox104
            // 
            this.checkBox104.AutoSize = true;
            this.checkBox104.Location = new System.Drawing.Point(447, 283);
            this.checkBox104.Name = "checkBox104";
            this.checkBox104.Size = new System.Drawing.Size(80, 17);
            this.checkBox104.TabIndex = 131;
            this.checkBox104.Text = "LZ22 Dead";
            this.checkBox104.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(213, 528);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(111, 46);
            this.button6.TabIndex = 60;
            this.button6.Text = "LZ Even Dead On";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.Button6_Click);
            // 
            // checkBox105
            // 
            this.checkBox105.AutoSize = true;
            this.checkBox105.Location = new System.Drawing.Point(447, 302);
            this.checkBox105.Name = "checkBox105";
            this.checkBox105.Size = new System.Drawing.Size(83, 17);
            this.checkBox105.TabIndex = 132;
            this.checkBox105.Text = "LZ23 Water";
            this.checkBox105.UseVisualStyleBackColor = true;
            // 
            // checkBox106
            // 
            this.checkBox106.AutoSize = true;
            this.checkBox106.Location = new System.Drawing.Point(447, 322);
            this.checkBox106.Name = "checkBox106";
            this.checkBox106.Size = new System.Drawing.Size(80, 17);
            this.checkBox106.TabIndex = 133;
            this.checkBox106.Text = "LZ23 Dead";
            this.checkBox106.UseVisualStyleBackColor = true;
            // 
            // checkBox107
            // 
            this.checkBox107.AutoSize = true;
            this.checkBox107.Location = new System.Drawing.Point(447, 341);
            this.checkBox107.Name = "checkBox107";
            this.checkBox107.Size = new System.Drawing.Size(83, 17);
            this.checkBox107.TabIndex = 134;
            this.checkBox107.Text = "LZ24 Water";
            this.checkBox107.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(76, 528);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(112, 46);
            this.button5.TabIndex = 59;
            this.button5.Text = "LZ Even Water On";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.Button5_Click);
            // 
            // checkBox108
            // 
            this.checkBox108.AutoSize = true;
            this.checkBox108.Location = new System.Drawing.Point(447, 361);
            this.checkBox108.Name = "checkBox108";
            this.checkBox108.Size = new System.Drawing.Size(80, 17);
            this.checkBox108.TabIndex = 135;
            this.checkBox108.Text = "LZ24 Dead";
            this.checkBox108.UseVisualStyleBackColor = true;
            // 
            // checkBox109
            // 
            this.checkBox109.AutoSize = true;
            this.checkBox109.Location = new System.Drawing.Point(447, 380);
            this.checkBox109.Name = "checkBox109";
            this.checkBox109.Size = new System.Drawing.Size(83, 17);
            this.checkBox109.TabIndex = 136;
            this.checkBox109.Text = "LZ25 Water";
            this.checkBox109.UseVisualStyleBackColor = true;
            // 
            // checkBox110
            // 
            this.checkBox110.AutoSize = true;
            this.checkBox110.Location = new System.Drawing.Point(447, 398);
            this.checkBox110.Name = "checkBox110";
            this.checkBox110.Size = new System.Drawing.Size(80, 17);
            this.checkBox110.TabIndex = 137;
            this.checkBox110.Text = "LZ25 Dead";
            this.checkBox110.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(489, 473);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(111, 46);
            this.button4.TabIndex = 58;
            this.button4.Text = "UZ Odd Dead On";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.Button4_Click);
            // 
            // checkBox111
            // 
            this.checkBox111.AutoSize = true;
            this.checkBox111.Location = new System.Drawing.Point(550, 226);
            this.checkBox111.Name = "checkBox111";
            this.checkBox111.Size = new System.Drawing.Size(83, 17);
            this.checkBox111.TabIndex = 138;
            this.checkBox111.Text = "LZ26 Water";
            this.checkBox111.UseVisualStyleBackColor = true;
            // 
            // checkBox112
            // 
            this.checkBox112.AutoSize = true;
            this.checkBox112.Location = new System.Drawing.Point(550, 246);
            this.checkBox112.Name = "checkBox112";
            this.checkBox112.Size = new System.Drawing.Size(80, 17);
            this.checkBox112.TabIndex = 139;
            this.checkBox112.Text = "LZ26 Dead";
            this.checkBox112.UseVisualStyleBackColor = true;
            // 
            // checkBox113
            // 
            this.checkBox113.AutoSize = true;
            this.checkBox113.Location = new System.Drawing.Point(550, 263);
            this.checkBox113.Name = "checkBox113";
            this.checkBox113.Size = new System.Drawing.Size(83, 17);
            this.checkBox113.TabIndex = 140;
            this.checkBox113.Text = "LZ27 Water";
            this.checkBox113.UseVisualStyleBackColor = true;
            // 
            // checkBox114
            // 
            this.checkBox114.AutoSize = true;
            this.checkBox114.Location = new System.Drawing.Point(550, 283);
            this.checkBox114.Name = "checkBox114";
            this.checkBox114.Size = new System.Drawing.Size(80, 17);
            this.checkBox114.TabIndex = 141;
            this.checkBox114.Text = "LZ27 Dead";
            this.checkBox114.UseVisualStyleBackColor = true;
            // 
            // checkBox115
            // 
            this.checkBox115.AutoSize = true;
            this.checkBox115.Location = new System.Drawing.Point(550, 302);
            this.checkBox115.Name = "checkBox115";
            this.checkBox115.Size = new System.Drawing.Size(83, 17);
            this.checkBox115.TabIndex = 142;
            this.checkBox115.Text = "LZ28 Water";
            this.checkBox115.UseVisualStyleBackColor = true;
            // 
            // checkBox116
            // 
            this.checkBox116.AutoSize = true;
            this.checkBox116.Location = new System.Drawing.Point(550, 322);
            this.checkBox116.Name = "checkBox116";
            this.checkBox116.Size = new System.Drawing.Size(80, 17);
            this.checkBox116.TabIndex = 143;
            this.checkBox116.Text = "LZ28 Dead";
            this.checkBox116.UseVisualStyleBackColor = true;
            // 
            // checkBox117
            // 
            this.checkBox117.AutoSize = true;
            this.checkBox117.Location = new System.Drawing.Point(550, 341);
            this.checkBox117.Name = "checkBox117";
            this.checkBox117.Size = new System.Drawing.Size(83, 17);
            this.checkBox117.TabIndex = 144;
            this.checkBox117.Text = "LZ29 Water";
            this.checkBox117.UseVisualStyleBackColor = true;
            // 
            // checkBox118
            // 
            this.checkBox118.AutoSize = true;
            this.checkBox118.Location = new System.Drawing.Point(550, 361);
            this.checkBox118.Name = "checkBox118";
            this.checkBox118.Size = new System.Drawing.Size(80, 17);
            this.checkBox118.TabIndex = 145;
            this.checkBox118.Text = "LZ29 Dead";
            this.checkBox118.UseVisualStyleBackColor = true;
            // 
            // checkBox119
            // 
            this.checkBox119.AutoSize = true;
            this.checkBox119.Location = new System.Drawing.Point(550, 380);
            this.checkBox119.Name = "checkBox119";
            this.checkBox119.Size = new System.Drawing.Size(83, 17);
            this.checkBox119.TabIndex = 146;
            this.checkBox119.Text = "LZ30 Water";
            this.checkBox119.UseVisualStyleBackColor = true;
            // 
            // checkBox120
            // 
            this.checkBox120.AutoSize = true;
            this.checkBox120.Location = new System.Drawing.Point(550, 398);
            this.checkBox120.Name = "checkBox120";
            this.checkBox120.Size = new System.Drawing.Size(80, 17);
            this.checkBox120.TabIndex = 147;
            this.checkBox120.Text = "LZ30 Dead";
            this.checkBox120.UseVisualStyleBackColor = true;
            // 
            // checkBox81
            // 
            this.checkBox81.AutoSize = true;
            this.checkBox81.Location = new System.Drawing.Point(223, 226);
            this.checkBox81.Name = "checkBox81";
            this.checkBox81.Size = new System.Drawing.Size(83, 17);
            this.checkBox81.TabIndex = 108;
            this.checkBox81.Text = "LZ11 Water";
            this.checkBox81.UseVisualStyleBackColor = true;
            // 
            // checkBox82
            // 
            this.checkBox82.AutoSize = true;
            this.checkBox82.Location = new System.Drawing.Point(223, 245);
            this.checkBox82.Name = "checkBox82";
            this.checkBox82.Size = new System.Drawing.Size(80, 17);
            this.checkBox82.TabIndex = 109;
            this.checkBox82.Text = "LZ11 Dead";
            this.checkBox82.UseVisualStyleBackColor = true;
            // 
            // checkBox83
            // 
            this.checkBox83.AutoSize = true;
            this.checkBox83.Location = new System.Drawing.Point(223, 262);
            this.checkBox83.Name = "checkBox83";
            this.checkBox83.Size = new System.Drawing.Size(83, 17);
            this.checkBox83.TabIndex = 110;
            this.checkBox83.Text = "LZ12 Water";
            this.checkBox83.UseVisualStyleBackColor = true;
            // 
            // checkBox84
            // 
            this.checkBox84.AutoSize = true;
            this.checkBox84.Location = new System.Drawing.Point(223, 283);
            this.checkBox84.Name = "checkBox84";
            this.checkBox84.Size = new System.Drawing.Size(80, 17);
            this.checkBox84.TabIndex = 111;
            this.checkBox84.Text = "LZ12 Dead";
            this.checkBox84.UseVisualStyleBackColor = true;
            // 
            // checkBox85
            // 
            this.checkBox85.AutoSize = true;
            this.checkBox85.Location = new System.Drawing.Point(223, 302);
            this.checkBox85.Name = "checkBox85";
            this.checkBox85.Size = new System.Drawing.Size(83, 17);
            this.checkBox85.TabIndex = 112;
            this.checkBox85.Text = "LZ13 Water";
            this.checkBox85.UseVisualStyleBackColor = true;
            // 
            // checkBox86
            // 
            this.checkBox86.AutoSize = true;
            this.checkBox86.Location = new System.Drawing.Point(223, 322);
            this.checkBox86.Name = "checkBox86";
            this.checkBox86.Size = new System.Drawing.Size(80, 17);
            this.checkBox86.TabIndex = 113;
            this.checkBox86.Text = "LZ13 Dead";
            this.checkBox86.UseVisualStyleBackColor = true;
            // 
            // checkBox87
            // 
            this.checkBox87.AutoSize = true;
            this.checkBox87.Location = new System.Drawing.Point(223, 341);
            this.checkBox87.Name = "checkBox87";
            this.checkBox87.Size = new System.Drawing.Size(83, 17);
            this.checkBox87.TabIndex = 114;
            this.checkBox87.Text = "LZ14 Water";
            this.checkBox87.UseVisualStyleBackColor = true;
            // 
            // checkBox88
            // 
            this.checkBox88.AutoSize = true;
            this.checkBox88.Location = new System.Drawing.Point(223, 361);
            this.checkBox88.Name = "checkBox88";
            this.checkBox88.Size = new System.Drawing.Size(80, 17);
            this.checkBox88.TabIndex = 115;
            this.checkBox88.Text = "LZ14 Dead";
            this.checkBox88.UseVisualStyleBackColor = true;
            // 
            // checkBox89
            // 
            this.checkBox89.AutoSize = true;
            this.checkBox89.Location = new System.Drawing.Point(223, 380);
            this.checkBox89.Name = "checkBox89";
            this.checkBox89.Size = new System.Drawing.Size(83, 17);
            this.checkBox89.TabIndex = 116;
            this.checkBox89.Text = "LZ15 Water";
            this.checkBox89.UseVisualStyleBackColor = true;
            // 
            // checkBox90
            // 
            this.checkBox90.AutoSize = true;
            this.checkBox90.Location = new System.Drawing.Point(223, 398);
            this.checkBox90.Name = "checkBox90";
            this.checkBox90.Size = new System.Drawing.Size(80, 17);
            this.checkBox90.TabIndex = 117;
            this.checkBox90.Text = "LZ15 Dead";
            this.checkBox90.UseVisualStyleBackColor = true;
            // 
            // checkBox91
            // 
            this.checkBox91.AutoSize = true;
            this.checkBox91.Location = new System.Drawing.Point(338, 226);
            this.checkBox91.Name = "checkBox91";
            this.checkBox91.Size = new System.Drawing.Size(83, 17);
            this.checkBox91.TabIndex = 118;
            this.checkBox91.Text = "LZ16 Water";
            this.checkBox91.UseVisualStyleBackColor = true;
            // 
            // checkBox92
            // 
            this.checkBox92.AutoSize = true;
            this.checkBox92.Location = new System.Drawing.Point(338, 245);
            this.checkBox92.Name = "checkBox92";
            this.checkBox92.Size = new System.Drawing.Size(80, 17);
            this.checkBox92.TabIndex = 119;
            this.checkBox92.Text = "LZ16 Dead";
            this.checkBox92.UseVisualStyleBackColor = true;
            // 
            // checkBox93
            // 
            this.checkBox93.AutoSize = true;
            this.checkBox93.Location = new System.Drawing.Point(338, 263);
            this.checkBox93.Name = "checkBox93";
            this.checkBox93.Size = new System.Drawing.Size(83, 17);
            this.checkBox93.TabIndex = 120;
            this.checkBox93.Text = "LZ17 Water";
            this.checkBox93.UseVisualStyleBackColor = true;
            // 
            // checkBox94
            // 
            this.checkBox94.AutoSize = true;
            this.checkBox94.Location = new System.Drawing.Point(338, 283);
            this.checkBox94.Name = "checkBox94";
            this.checkBox94.Size = new System.Drawing.Size(80, 17);
            this.checkBox94.TabIndex = 121;
            this.checkBox94.Text = "LZ17 Dead";
            this.checkBox94.UseVisualStyleBackColor = true;
            // 
            // checkBox95
            // 
            this.checkBox95.AutoSize = true;
            this.checkBox95.Location = new System.Drawing.Point(338, 302);
            this.checkBox95.Name = "checkBox95";
            this.checkBox95.Size = new System.Drawing.Size(83, 17);
            this.checkBox95.TabIndex = 122;
            this.checkBox95.Text = "LZ18 Water";
            this.checkBox95.UseVisualStyleBackColor = true;
            // 
            // checkBox96
            // 
            this.checkBox96.AutoSize = true;
            this.checkBox96.Location = new System.Drawing.Point(338, 322);
            this.checkBox96.Name = "checkBox96";
            this.checkBox96.Size = new System.Drawing.Size(80, 17);
            this.checkBox96.TabIndex = 123;
            this.checkBox96.Text = "LZ18 Dead";
            this.checkBox96.UseVisualStyleBackColor = true;
            // 
            // checkBox97
            // 
            this.checkBox97.AutoSize = true;
            this.checkBox97.Location = new System.Drawing.Point(338, 341);
            this.checkBox97.Name = "checkBox97";
            this.checkBox97.Size = new System.Drawing.Size(83, 17);
            this.checkBox97.TabIndex = 124;
            this.checkBox97.Text = "LZ19 Water";
            this.checkBox97.UseVisualStyleBackColor = true;
            // 
            // checkBox98
            // 
            this.checkBox98.AutoSize = true;
            this.checkBox98.Location = new System.Drawing.Point(338, 361);
            this.checkBox98.Name = "checkBox98";
            this.checkBox98.Size = new System.Drawing.Size(80, 17);
            this.checkBox98.TabIndex = 125;
            this.checkBox98.Text = "LZ19 Dead";
            this.checkBox98.UseVisualStyleBackColor = true;
            // 
            // checkBox99
            // 
            this.checkBox99.AutoSize = true;
            this.checkBox99.Location = new System.Drawing.Point(338, 380);
            this.checkBox99.Name = "checkBox99";
            this.checkBox99.Size = new System.Drawing.Size(83, 17);
            this.checkBox99.TabIndex = 126;
            this.checkBox99.Text = "LZ20 Water";
            this.checkBox99.UseVisualStyleBackColor = true;
            // 
            // checkBox100
            // 
            this.checkBox100.AutoSize = true;
            this.checkBox100.Location = new System.Drawing.Point(338, 398);
            this.checkBox100.Name = "checkBox100";
            this.checkBox100.Size = new System.Drawing.Size(80, 17);
            this.checkBox100.TabIndex = 127;
            this.checkBox100.Text = "LZ20 Dead";
            this.checkBox100.UseVisualStyleBackColor = true;
            // 
            // checkBox61
            // 
            this.checkBox61.AutoSize = true;
            this.checkBox61.Location = new System.Drawing.Point(6, 226);
            this.checkBox61.Name = "checkBox61";
            this.checkBox61.Size = new System.Drawing.Size(77, 17);
            this.checkBox61.TabIndex = 88;
            this.checkBox61.Text = "LZ1 Water";
            this.checkBox61.UseVisualStyleBackColor = true;
            // 
            // checkBox62
            // 
            this.checkBox62.AutoSize = true;
            this.checkBox62.Location = new System.Drawing.Point(6, 245);
            this.checkBox62.Name = "checkBox62";
            this.checkBox62.Size = new System.Drawing.Size(74, 17);
            this.checkBox62.TabIndex = 89;
            this.checkBox62.Text = "LZ1 Dead";
            this.checkBox62.UseVisualStyleBackColor = true;
            // 
            // checkBox63
            // 
            this.checkBox63.AutoSize = true;
            this.checkBox63.Location = new System.Drawing.Point(6, 263);
            this.checkBox63.Name = "checkBox63";
            this.checkBox63.Size = new System.Drawing.Size(77, 17);
            this.checkBox63.TabIndex = 90;
            this.checkBox63.Text = "LZ2 Water";
            this.checkBox63.UseVisualStyleBackColor = true;
            // 
            // checkBox64
            // 
            this.checkBox64.AutoSize = true;
            this.checkBox64.Location = new System.Drawing.Point(6, 283);
            this.checkBox64.Name = "checkBox64";
            this.checkBox64.Size = new System.Drawing.Size(74, 17);
            this.checkBox64.TabIndex = 91;
            this.checkBox64.Text = "LZ2 Dead";
            this.checkBox64.UseVisualStyleBackColor = true;
            // 
            // checkBox65
            // 
            this.checkBox65.AutoSize = true;
            this.checkBox65.Location = new System.Drawing.Point(6, 302);
            this.checkBox65.Name = "checkBox65";
            this.checkBox65.Size = new System.Drawing.Size(77, 17);
            this.checkBox65.TabIndex = 92;
            this.checkBox65.Text = "LZ3 Water";
            this.checkBox65.UseVisualStyleBackColor = true;
            // 
            // checkBox66
            // 
            this.checkBox66.AutoSize = true;
            this.checkBox66.Location = new System.Drawing.Point(6, 322);
            this.checkBox66.Name = "checkBox66";
            this.checkBox66.Size = new System.Drawing.Size(74, 17);
            this.checkBox66.TabIndex = 93;
            this.checkBox66.Text = "LZ3 Dead";
            this.checkBox66.UseVisualStyleBackColor = true;
            // 
            // checkBox67
            // 
            this.checkBox67.AutoSize = true;
            this.checkBox67.Location = new System.Drawing.Point(6, 341);
            this.checkBox67.Name = "checkBox67";
            this.checkBox67.Size = new System.Drawing.Size(77, 17);
            this.checkBox67.TabIndex = 94;
            this.checkBox67.Text = "LZ4 Water";
            this.checkBox67.UseVisualStyleBackColor = true;
            // 
            // checkBox68
            // 
            this.checkBox68.AutoSize = true;
            this.checkBox68.Location = new System.Drawing.Point(6, 361);
            this.checkBox68.Name = "checkBox68";
            this.checkBox68.Size = new System.Drawing.Size(74, 17);
            this.checkBox68.TabIndex = 95;
            this.checkBox68.Text = "LZ4 Dead";
            this.checkBox68.UseVisualStyleBackColor = true;
            // 
            // checkBox69
            // 
            this.checkBox69.AutoSize = true;
            this.checkBox69.Location = new System.Drawing.Point(6, 380);
            this.checkBox69.Name = "checkBox69";
            this.checkBox69.Size = new System.Drawing.Size(77, 17);
            this.checkBox69.TabIndex = 96;
            this.checkBox69.Text = "LZ5 Water";
            this.checkBox69.UseVisualStyleBackColor = true;
            // 
            // checkBox70
            // 
            this.checkBox70.AutoSize = true;
            this.checkBox70.Location = new System.Drawing.Point(6, 398);
            this.checkBox70.Name = "checkBox70";
            this.checkBox70.Size = new System.Drawing.Size(74, 17);
            this.checkBox70.TabIndex = 97;
            this.checkBox70.Text = "LZ5 Dead";
            this.checkBox70.UseVisualStyleBackColor = true;
            // 
            // checkBox71
            // 
            this.checkBox71.AutoSize = true;
            this.checkBox71.Location = new System.Drawing.Point(111, 226);
            this.checkBox71.Name = "checkBox71";
            this.checkBox71.Size = new System.Drawing.Size(77, 17);
            this.checkBox71.TabIndex = 98;
            this.checkBox71.Text = "LZ6 Water";
            this.checkBox71.UseVisualStyleBackColor = true;
            // 
            // checkBox72
            // 
            this.checkBox72.AutoSize = true;
            this.checkBox72.Location = new System.Drawing.Point(111, 245);
            this.checkBox72.Name = "checkBox72";
            this.checkBox72.Size = new System.Drawing.Size(74, 17);
            this.checkBox72.TabIndex = 99;
            this.checkBox72.Text = "LZ6 Dead";
            this.checkBox72.UseVisualStyleBackColor = true;
            // 
            // checkBox73
            // 
            this.checkBox73.AutoSize = true;
            this.checkBox73.Location = new System.Drawing.Point(111, 263);
            this.checkBox73.Name = "checkBox73";
            this.checkBox73.Size = new System.Drawing.Size(77, 17);
            this.checkBox73.TabIndex = 100;
            this.checkBox73.Text = "LZ7 Water";
            this.checkBox73.UseVisualStyleBackColor = true;
            // 
            // checkBox74
            // 
            this.checkBox74.AutoSize = true;
            this.checkBox74.Location = new System.Drawing.Point(111, 283);
            this.checkBox74.Name = "checkBox74";
            this.checkBox74.Size = new System.Drawing.Size(74, 17);
            this.checkBox74.TabIndex = 101;
            this.checkBox74.Text = "LZ7 Dead";
            this.checkBox74.UseVisualStyleBackColor = true;
            // 
            // checkBox75
            // 
            this.checkBox75.AutoSize = true;
            this.checkBox75.Location = new System.Drawing.Point(111, 302);
            this.checkBox75.Name = "checkBox75";
            this.checkBox75.Size = new System.Drawing.Size(77, 17);
            this.checkBox75.TabIndex = 102;
            this.checkBox75.Text = "LZ8 Water";
            this.checkBox75.UseVisualStyleBackColor = true;
            // 
            // checkBox76
            // 
            this.checkBox76.AutoSize = true;
            this.checkBox76.Location = new System.Drawing.Point(111, 322);
            this.checkBox76.Name = "checkBox76";
            this.checkBox76.Size = new System.Drawing.Size(74, 17);
            this.checkBox76.TabIndex = 103;
            this.checkBox76.Text = "LZ8 Dead";
            this.checkBox76.UseVisualStyleBackColor = true;
            // 
            // checkBox77
            // 
            this.checkBox77.AutoSize = true;
            this.checkBox77.Location = new System.Drawing.Point(111, 341);
            this.checkBox77.Name = "checkBox77";
            this.checkBox77.Size = new System.Drawing.Size(77, 17);
            this.checkBox77.TabIndex = 104;
            this.checkBox77.Text = "LZ9 Water";
            this.checkBox77.UseVisualStyleBackColor = true;
            // 
            // checkBox78
            // 
            this.checkBox78.AutoSize = true;
            this.checkBox78.Location = new System.Drawing.Point(111, 361);
            this.checkBox78.Name = "checkBox78";
            this.checkBox78.Size = new System.Drawing.Size(74, 17);
            this.checkBox78.TabIndex = 105;
            this.checkBox78.Text = "LZ9 Dead";
            this.checkBox78.UseVisualStyleBackColor = true;
            // 
            // checkBox79
            // 
            this.checkBox79.AutoSize = true;
            this.checkBox79.Location = new System.Drawing.Point(111, 380);
            this.checkBox79.Name = "checkBox79";
            this.checkBox79.Size = new System.Drawing.Size(83, 17);
            this.checkBox79.TabIndex = 106;
            this.checkBox79.Text = "LZ10 Water";
            this.checkBox79.UseVisualStyleBackColor = true;
            // 
            // checkBox80
            // 
            this.checkBox80.AutoSize = true;
            this.checkBox80.Location = new System.Drawing.Point(111, 398);
            this.checkBox80.Name = "checkBox80";
            this.checkBox80.Size = new System.Drawing.Size(80, 17);
            this.checkBox80.TabIndex = 107;
            this.checkBox80.Text = "LZ10 Dead";
            this.checkBox80.UseVisualStyleBackColor = true;
            // 
            // checkBox41
            // 
            this.checkBox41.AutoSize = true;
            this.checkBox41.Location = new System.Drawing.Point(447, 22);
            this.checkBox41.Name = "checkBox41";
            this.checkBox41.Size = new System.Drawing.Size(85, 17);
            this.checkBox41.TabIndex = 68;
            this.checkBox41.Text = "UZ21 Water";
            this.checkBox41.UseVisualStyleBackColor = true;
            // 
            // checkBox42
            // 
            this.checkBox42.AutoSize = true;
            this.checkBox42.Location = new System.Drawing.Point(447, 41);
            this.checkBox42.Name = "checkBox42";
            this.checkBox42.Size = new System.Drawing.Size(82, 17);
            this.checkBox42.TabIndex = 69;
            this.checkBox42.Text = "UZ21 Dead";
            this.checkBox42.UseVisualStyleBackColor = true;
            // 
            // checkBox43
            // 
            this.checkBox43.AutoSize = true;
            this.checkBox43.Location = new System.Drawing.Point(447, 59);
            this.checkBox43.Name = "checkBox43";
            this.checkBox43.Size = new System.Drawing.Size(85, 17);
            this.checkBox43.TabIndex = 70;
            this.checkBox43.Text = "UZ22 Water";
            this.checkBox43.UseVisualStyleBackColor = true;
            // 
            // checkBox44
            // 
            this.checkBox44.AutoSize = true;
            this.checkBox44.Location = new System.Drawing.Point(447, 79);
            this.checkBox44.Name = "checkBox44";
            this.checkBox44.Size = new System.Drawing.Size(82, 17);
            this.checkBox44.TabIndex = 71;
            this.checkBox44.Text = "UZ22 Dead";
            this.checkBox44.UseVisualStyleBackColor = true;
            // 
            // checkBox45
            // 
            this.checkBox45.AutoSize = true;
            this.checkBox45.Location = new System.Drawing.Point(447, 98);
            this.checkBox45.Name = "checkBox45";
            this.checkBox45.Size = new System.Drawing.Size(85, 17);
            this.checkBox45.TabIndex = 72;
            this.checkBox45.Text = "UZ23 Water";
            this.checkBox45.UseVisualStyleBackColor = true;
            // 
            // checkBox46
            // 
            this.checkBox46.AutoSize = true;
            this.checkBox46.Location = new System.Drawing.Point(447, 118);
            this.checkBox46.Name = "checkBox46";
            this.checkBox46.Size = new System.Drawing.Size(82, 17);
            this.checkBox46.TabIndex = 73;
            this.checkBox46.Text = "UZ23 Dead";
            this.checkBox46.UseVisualStyleBackColor = true;
            // 
            // checkBox47
            // 
            this.checkBox47.AutoSize = true;
            this.checkBox47.Location = new System.Drawing.Point(447, 137);
            this.checkBox47.Name = "checkBox47";
            this.checkBox47.Size = new System.Drawing.Size(85, 17);
            this.checkBox47.TabIndex = 74;
            this.checkBox47.Text = "UZ24 Water";
            this.checkBox47.UseVisualStyleBackColor = true;
            // 
            // checkBox48
            // 
            this.checkBox48.AutoSize = true;
            this.checkBox48.Location = new System.Drawing.Point(447, 157);
            this.checkBox48.Name = "checkBox48";
            this.checkBox48.Size = new System.Drawing.Size(82, 17);
            this.checkBox48.TabIndex = 75;
            this.checkBox48.Text = "UZ24 Dead";
            this.checkBox48.UseVisualStyleBackColor = true;
            // 
            // checkBox49
            // 
            this.checkBox49.AutoSize = true;
            this.checkBox49.Location = new System.Drawing.Point(447, 176);
            this.checkBox49.Name = "checkBox49";
            this.checkBox49.Size = new System.Drawing.Size(85, 17);
            this.checkBox49.TabIndex = 76;
            this.checkBox49.Text = "UZ25 Water";
            this.checkBox49.UseVisualStyleBackColor = true;
            // 
            // checkBox50
            // 
            this.checkBox50.AutoSize = true;
            this.checkBox50.Location = new System.Drawing.Point(447, 194);
            this.checkBox50.Name = "checkBox50";
            this.checkBox50.Size = new System.Drawing.Size(82, 17);
            this.checkBox50.TabIndex = 77;
            this.checkBox50.Text = "UZ25 Dead";
            this.checkBox50.UseVisualStyleBackColor = true;
            // 
            // checkBox51
            // 
            this.checkBox51.AutoSize = true;
            this.checkBox51.Location = new System.Drawing.Point(550, 22);
            this.checkBox51.Name = "checkBox51";
            this.checkBox51.Size = new System.Drawing.Size(85, 17);
            this.checkBox51.TabIndex = 78;
            this.checkBox51.Text = "UZ26 Water";
            this.checkBox51.UseVisualStyleBackColor = true;
            // 
            // checkBox52
            // 
            this.checkBox52.AutoSize = true;
            this.checkBox52.Location = new System.Drawing.Point(550, 41);
            this.checkBox52.Name = "checkBox52";
            this.checkBox52.Size = new System.Drawing.Size(82, 17);
            this.checkBox52.TabIndex = 79;
            this.checkBox52.Text = "UZ26 Dead";
            this.checkBox52.UseVisualStyleBackColor = true;
            // 
            // checkBox53
            // 
            this.checkBox53.AutoSize = true;
            this.checkBox53.Location = new System.Drawing.Point(550, 59);
            this.checkBox53.Name = "checkBox53";
            this.checkBox53.Size = new System.Drawing.Size(85, 17);
            this.checkBox53.TabIndex = 80;
            this.checkBox53.Text = "UZ27 Water";
            this.checkBox53.UseVisualStyleBackColor = true;
            // 
            // checkBox54
            // 
            this.checkBox54.AutoSize = true;
            this.checkBox54.Location = new System.Drawing.Point(550, 79);
            this.checkBox54.Name = "checkBox54";
            this.checkBox54.Size = new System.Drawing.Size(82, 17);
            this.checkBox54.TabIndex = 81;
            this.checkBox54.Text = "UZ27 Dead";
            this.checkBox54.UseVisualStyleBackColor = true;
            // 
            // checkBox55
            // 
            this.checkBox55.AutoSize = true;
            this.checkBox55.Location = new System.Drawing.Point(550, 98);
            this.checkBox55.Name = "checkBox55";
            this.checkBox55.Size = new System.Drawing.Size(85, 17);
            this.checkBox55.TabIndex = 82;
            this.checkBox55.Text = "UZ28 Water";
            this.checkBox55.UseVisualStyleBackColor = true;
            // 
            // checkBox56
            // 
            this.checkBox56.AutoSize = true;
            this.checkBox56.Location = new System.Drawing.Point(550, 118);
            this.checkBox56.Name = "checkBox56";
            this.checkBox56.Size = new System.Drawing.Size(82, 17);
            this.checkBox56.TabIndex = 83;
            this.checkBox56.Text = "UZ28 Dead";
            this.checkBox56.UseVisualStyleBackColor = true;
            // 
            // checkBox57
            // 
            this.checkBox57.AutoSize = true;
            this.checkBox57.Location = new System.Drawing.Point(550, 137);
            this.checkBox57.Name = "checkBox57";
            this.checkBox57.Size = new System.Drawing.Size(85, 17);
            this.checkBox57.TabIndex = 84;
            this.checkBox57.Text = "UZ29 Water";
            this.checkBox57.UseVisualStyleBackColor = true;
            // 
            // checkBox58
            // 
            this.checkBox58.AutoSize = true;
            this.checkBox58.Location = new System.Drawing.Point(550, 157);
            this.checkBox58.Name = "checkBox58";
            this.checkBox58.Size = new System.Drawing.Size(82, 17);
            this.checkBox58.TabIndex = 85;
            this.checkBox58.Text = "UZ29 Dead";
            this.checkBox58.UseVisualStyleBackColor = true;
            // 
            // checkBox59
            // 
            this.checkBox59.AutoSize = true;
            this.checkBox59.Location = new System.Drawing.Point(550, 176);
            this.checkBox59.Name = "checkBox59";
            this.checkBox59.Size = new System.Drawing.Size(85, 17);
            this.checkBox59.TabIndex = 86;
            this.checkBox59.Text = "UZ30 Water";
            this.checkBox59.UseVisualStyleBackColor = true;
            // 
            // checkBox60
            // 
            this.checkBox60.AutoSize = true;
            this.checkBox60.Location = new System.Drawing.Point(550, 194);
            this.checkBox60.Name = "checkBox60";
            this.checkBox60.Size = new System.Drawing.Size(82, 17);
            this.checkBox60.TabIndex = 87;
            this.checkBox60.Text = "UZ30 Dead";
            this.checkBox60.UseVisualStyleBackColor = true;
            // 
            // checkBox21
            // 
            this.checkBox21.AutoSize = true;
            this.checkBox21.Location = new System.Drawing.Point(223, 22);
            this.checkBox21.Name = "checkBox21";
            this.checkBox21.Size = new System.Drawing.Size(85, 17);
            this.checkBox21.TabIndex = 48;
            this.checkBox21.Text = "UZ11 Water";
            this.checkBox21.UseVisualStyleBackColor = true;
            // 
            // checkBox22
            // 
            this.checkBox22.AutoSize = true;
            this.checkBox22.Location = new System.Drawing.Point(223, 41);
            this.checkBox22.Name = "checkBox22";
            this.checkBox22.Size = new System.Drawing.Size(82, 17);
            this.checkBox22.TabIndex = 49;
            this.checkBox22.Text = "UZ11 Dead";
            this.checkBox22.UseVisualStyleBackColor = true;
            // 
            // checkBox23
            // 
            this.checkBox23.AutoSize = true;
            this.checkBox23.Location = new System.Drawing.Point(223, 59);
            this.checkBox23.Name = "checkBox23";
            this.checkBox23.Size = new System.Drawing.Size(85, 17);
            this.checkBox23.TabIndex = 50;
            this.checkBox23.Text = "UZ12 Water";
            this.checkBox23.UseVisualStyleBackColor = true;
            // 
            // checkBox24
            // 
            this.checkBox24.AutoSize = true;
            this.checkBox24.Location = new System.Drawing.Point(223, 79);
            this.checkBox24.Name = "checkBox24";
            this.checkBox24.Size = new System.Drawing.Size(82, 17);
            this.checkBox24.TabIndex = 51;
            this.checkBox24.Text = "UZ12 Dead";
            this.checkBox24.UseVisualStyleBackColor = true;
            // 
            // checkBox25
            // 
            this.checkBox25.AutoSize = true;
            this.checkBox25.Location = new System.Drawing.Point(223, 98);
            this.checkBox25.Name = "checkBox25";
            this.checkBox25.Size = new System.Drawing.Size(85, 17);
            this.checkBox25.TabIndex = 52;
            this.checkBox25.Text = "UZ13 Water";
            this.checkBox25.UseVisualStyleBackColor = true;
            // 
            // checkBox26
            // 
            this.checkBox26.AutoSize = true;
            this.checkBox26.Location = new System.Drawing.Point(223, 118);
            this.checkBox26.Name = "checkBox26";
            this.checkBox26.Size = new System.Drawing.Size(82, 17);
            this.checkBox26.TabIndex = 53;
            this.checkBox26.Text = "UZ13 Dead";
            this.checkBox26.UseVisualStyleBackColor = true;
            // 
            // checkBox27
            // 
            this.checkBox27.AutoSize = true;
            this.checkBox27.Location = new System.Drawing.Point(223, 137);
            this.checkBox27.Name = "checkBox27";
            this.checkBox27.Size = new System.Drawing.Size(85, 17);
            this.checkBox27.TabIndex = 54;
            this.checkBox27.Text = "UZ14 Water";
            this.checkBox27.UseVisualStyleBackColor = true;
            // 
            // checkBox28
            // 
            this.checkBox28.AutoSize = true;
            this.checkBox28.Location = new System.Drawing.Point(223, 157);
            this.checkBox28.Name = "checkBox28";
            this.checkBox28.Size = new System.Drawing.Size(82, 17);
            this.checkBox28.TabIndex = 55;
            this.checkBox28.Text = "UZ14 Dead";
            this.checkBox28.UseVisualStyleBackColor = true;
            // 
            // checkBox29
            // 
            this.checkBox29.AutoSize = true;
            this.checkBox29.Location = new System.Drawing.Point(223, 176);
            this.checkBox29.Name = "checkBox29";
            this.checkBox29.Size = new System.Drawing.Size(85, 17);
            this.checkBox29.TabIndex = 56;
            this.checkBox29.Text = "UZ15 Water";
            this.checkBox29.UseVisualStyleBackColor = true;
            // 
            // checkBox30
            // 
            this.checkBox30.AutoSize = true;
            this.checkBox30.Location = new System.Drawing.Point(223, 194);
            this.checkBox30.Name = "checkBox30";
            this.checkBox30.Size = new System.Drawing.Size(82, 17);
            this.checkBox30.TabIndex = 57;
            this.checkBox30.Text = "UZ15 Dead";
            this.checkBox30.UseVisualStyleBackColor = true;
            // 
            // checkBox31
            // 
            this.checkBox31.AutoSize = true;
            this.checkBox31.Location = new System.Drawing.Point(338, 22);
            this.checkBox31.Name = "checkBox31";
            this.checkBox31.Size = new System.Drawing.Size(85, 17);
            this.checkBox31.TabIndex = 58;
            this.checkBox31.Text = "UZ16 Water";
            this.checkBox31.UseVisualStyleBackColor = true;
            // 
            // checkBox32
            // 
            this.checkBox32.AutoSize = true;
            this.checkBox32.Location = new System.Drawing.Point(338, 41);
            this.checkBox32.Name = "checkBox32";
            this.checkBox32.Size = new System.Drawing.Size(82, 17);
            this.checkBox32.TabIndex = 59;
            this.checkBox32.Text = "UZ16 Dead";
            this.checkBox32.UseVisualStyleBackColor = true;
            // 
            // checkBox33
            // 
            this.checkBox33.AutoSize = true;
            this.checkBox33.Location = new System.Drawing.Point(338, 59);
            this.checkBox33.Name = "checkBox33";
            this.checkBox33.Size = new System.Drawing.Size(85, 17);
            this.checkBox33.TabIndex = 60;
            this.checkBox33.Text = "UZ17 Water";
            this.checkBox33.UseVisualStyleBackColor = true;
            // 
            // checkBox34
            // 
            this.checkBox34.AutoSize = true;
            this.checkBox34.Location = new System.Drawing.Point(338, 79);
            this.checkBox34.Name = "checkBox34";
            this.checkBox34.Size = new System.Drawing.Size(82, 17);
            this.checkBox34.TabIndex = 61;
            this.checkBox34.Text = "UZ17 Dead";
            this.checkBox34.UseVisualStyleBackColor = true;
            // 
            // checkBox35
            // 
            this.checkBox35.AutoSize = true;
            this.checkBox35.Location = new System.Drawing.Point(338, 98);
            this.checkBox35.Name = "checkBox35";
            this.checkBox35.Size = new System.Drawing.Size(85, 17);
            this.checkBox35.TabIndex = 62;
            this.checkBox35.Text = "UZ18 Water";
            this.checkBox35.UseVisualStyleBackColor = true;
            // 
            // checkBox36
            // 
            this.checkBox36.AutoSize = true;
            this.checkBox36.Location = new System.Drawing.Point(338, 118);
            this.checkBox36.Name = "checkBox36";
            this.checkBox36.Size = new System.Drawing.Size(82, 17);
            this.checkBox36.TabIndex = 63;
            this.checkBox36.Text = "UZ18 Dead";
            this.checkBox36.UseVisualStyleBackColor = true;
            // 
            // checkBox37
            // 
            this.checkBox37.AutoSize = true;
            this.checkBox37.Location = new System.Drawing.Point(338, 137);
            this.checkBox37.Name = "checkBox37";
            this.checkBox37.Size = new System.Drawing.Size(85, 17);
            this.checkBox37.TabIndex = 64;
            this.checkBox37.Text = "UZ19 Water";
            this.checkBox37.UseVisualStyleBackColor = true;
            // 
            // checkBox38
            // 
            this.checkBox38.AutoSize = true;
            this.checkBox38.Location = new System.Drawing.Point(338, 157);
            this.checkBox38.Name = "checkBox38";
            this.checkBox38.Size = new System.Drawing.Size(82, 17);
            this.checkBox38.TabIndex = 65;
            this.checkBox38.Text = "UZ19 Dead";
            this.checkBox38.UseVisualStyleBackColor = true;
            // 
            // checkBox39
            // 
            this.checkBox39.AutoSize = true;
            this.checkBox39.Location = new System.Drawing.Point(338, 176);
            this.checkBox39.Name = "checkBox39";
            this.checkBox39.Size = new System.Drawing.Size(85, 17);
            this.checkBox39.TabIndex = 66;
            this.checkBox39.Text = "UZ20 Water";
            this.checkBox39.UseVisualStyleBackColor = true;
            // 
            // checkBox40
            // 
            this.checkBox40.AutoSize = true;
            this.checkBox40.Location = new System.Drawing.Point(338, 194);
            this.checkBox40.Name = "checkBox40";
            this.checkBox40.Size = new System.Drawing.Size(82, 17);
            this.checkBox40.TabIndex = 67;
            this.checkBox40.Text = "UZ20 Dead";
            this.checkBox40.UseVisualStyleBackColor = true;
            // 
            // checkBox20
            // 
            this.checkBox20.AutoSize = true;
            this.checkBox20.Location = new System.Drawing.Point(111, 194);
            this.checkBox20.Name = "checkBox20";
            this.checkBox20.Size = new System.Drawing.Size(82, 17);
            this.checkBox20.TabIndex = 47;
            this.checkBox20.Text = "UZ10 Dead";
            this.checkBox20.UseVisualStyleBackColor = true;
            // 
            // checkBox19
            // 
            this.checkBox19.AutoSize = true;
            this.checkBox19.Location = new System.Drawing.Point(111, 176);
            this.checkBox19.Name = "checkBox19";
            this.checkBox19.Size = new System.Drawing.Size(85, 17);
            this.checkBox19.TabIndex = 46;
            this.checkBox19.Text = "UZ10 Water";
            this.checkBox19.UseVisualStyleBackColor = true;
            // 
            // checkBox18
            // 
            this.checkBox18.AutoSize = true;
            this.checkBox18.Location = new System.Drawing.Point(111, 157);
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new System.Drawing.Size(76, 17);
            this.checkBox18.TabIndex = 45;
            this.checkBox18.Text = "UZ9 Dead";
            this.checkBox18.UseVisualStyleBackColor = true;
            // 
            // checkBox17
            // 
            this.checkBox17.AutoSize = true;
            this.checkBox17.Location = new System.Drawing.Point(111, 137);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new System.Drawing.Size(79, 17);
            this.checkBox17.TabIndex = 44;
            this.checkBox17.Text = "UZ9 Water";
            this.checkBox17.UseVisualStyleBackColor = true;
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Location = new System.Drawing.Point(111, 118);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(76, 17);
            this.checkBox16.TabIndex = 43;
            this.checkBox16.Text = "UZ8 Dead";
            this.checkBox16.UseVisualStyleBackColor = true;
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(111, 98);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(79, 17);
            this.checkBox15.TabIndex = 42;
            this.checkBox15.Text = "UZ8 Water";
            this.checkBox15.UseVisualStyleBackColor = true;
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Location = new System.Drawing.Point(111, 79);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(76, 17);
            this.checkBox14.TabIndex = 41;
            this.checkBox14.Text = "UZ7 Dead";
            this.checkBox14.UseVisualStyleBackColor = true;
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Location = new System.Drawing.Point(111, 60);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(79, 17);
            this.checkBox13.TabIndex = 40;
            this.checkBox13.Text = "UZ7 Water";
            this.checkBox13.UseVisualStyleBackColor = true;
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Location = new System.Drawing.Point(111, 41);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(76, 17);
            this.checkBox12.TabIndex = 39;
            this.checkBox12.Text = "UZ6 Dead";
            this.checkBox12.UseVisualStyleBackColor = true;
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Location = new System.Drawing.Point(111, 22);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(79, 17);
            this.checkBox11.TabIndex = 38;
            this.checkBox11.Text = "UZ6 Water";
            this.checkBox11.UseVisualStyleBackColor = true;
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(6, 194);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(76, 17);
            this.checkBox10.TabIndex = 37;
            this.checkBox10.Text = "UZ5 Dead";
            this.checkBox10.UseVisualStyleBackColor = true;
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(6, 176);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(79, 17);
            this.checkBox9.TabIndex = 36;
            this.checkBox9.Text = "UZ5 Water";
            this.checkBox9.UseVisualStyleBackColor = true;
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(6, 157);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(76, 17);
            this.checkBox8.TabIndex = 35;
            this.checkBox8.Text = "UZ4 Dead";
            this.checkBox8.UseVisualStyleBackColor = true;
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(6, 137);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(79, 17);
            this.checkBox7.TabIndex = 34;
            this.checkBox7.Text = "UZ4 Water";
            this.checkBox7.UseVisualStyleBackColor = true;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(6, 118);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(76, 17);
            this.checkBox6.TabIndex = 33;
            this.checkBox6.Text = "UZ3 Dead";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(6, 98);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(79, 17);
            this.checkBox5.TabIndex = 32;
            this.checkBox5.Text = "UZ3 Water";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(6, 79);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(76, 17);
            this.checkBox4.TabIndex = 31;
            this.checkBox4.Text = "UZ2 Dead";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(6, 59);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(79, 17);
            this.checkBox3.TabIndex = 30;
            this.checkBox3.Text = "UZ2 Water";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(6, 41);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(76, 17);
            this.checkBox2.TabIndex = 29;
            this.checkBox2.Text = "UZ1 Dead";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(6, 22);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(79, 17);
            this.checkBox1.TabIndex = 28;
            this.checkBox1.Text = "UZ1 Water";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // gEI_ExtrusionDataSet1
            // 
            this.gEI_ExtrusionDataSet1.DataSetName = "GEI_ExtrusionDataSet1";
            this.gEI_ExtrusionDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tbl3DielogBindingSource
            // 
            this.tbl3DielogBindingSource.DataMember = "tbl3Dielog";
            this.tbl3DielogBindingSource.DataSource = this.gEI_ExtrusionDataSet1;
            // 
            // tbl3DielogTableAdapter
            // 
            this.tbl3DielogTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.tbl3DielogTableAdapter = this.tbl3DielogTableAdapter;
            this.tableAdapterManager.tblPress2_Die_Info_AlloyTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = ExtrusionOneStop.GEI_ExtrusionDataSet1TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button19);
            this.groupBox4.Controls.Add(this.button18);
            this.groupBox4.Controls.Add(this.button17);
            this.groupBox4.Controls.Add(this.button16);
            this.groupBox4.Controls.Add(this.button15);
            this.groupBox4.Controls.Add(this.button14);
            this.groupBox4.Controls.Add(this.button13);
            this.groupBox4.Controls.Add(this.button12);
            this.groupBox4.Location = new System.Drawing.Point(1, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(180, 277);
            this.groupBox4.TabIndex = 56;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Menu";
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(94, 99);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(76, 34);
            this.button17.TabIndex = 5;
            this.button17.Text = "Die Status";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.Button17_Click);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(11, 99);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(76, 34);
            this.button16.TabIndex = 4;
            this.button16.Text = "Die Ring";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.Button16_Click);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(94, 60);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(76, 34);
            this.button15.TabIndex = 3;
            this.button15.Text = "Search Prod Die Insp";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.Button15_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(12, 59);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(76, 34);
            this.button14.TabIndex = 2;
            this.button14.Text = "Ext Die Insp";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.Button14_Click);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(94, 19);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(76, 34);
            this.button13.TabIndex = 1;
            this.button13.Text = "3DieLog Table";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.Button13_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(12, 19);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(76, 34);
            this.button12.TabIndex = 0;
            this.button12.Text = "Die Recipe";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.Button12_Click);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(12, 139);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(76, 34);
            this.button18.TabIndex = 6;
            this.button18.Text = "Ext Dispatch";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.Button18_Click);
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(94, 140);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(76, 34);
            this.button19.TabIndex = 7;
            this.button19.Text = "button19";
            this.button19.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1359, 608);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Extrusion One Stop";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gEI_ExtrusionDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbl3DielogBindingSource)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkBox20;
        private System.Windows.Forms.CheckBox checkBox19;
        private System.Windows.Forms.CheckBox checkBox18;
        private System.Windows.Forms.CheckBox checkBox17;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox101;
        private System.Windows.Forms.CheckBox checkBox102;
        private System.Windows.Forms.CheckBox checkBox103;
        private System.Windows.Forms.CheckBox checkBox104;
        private System.Windows.Forms.CheckBox checkBox105;
        private System.Windows.Forms.CheckBox checkBox106;
        private System.Windows.Forms.CheckBox checkBox107;
        private System.Windows.Forms.CheckBox checkBox108;
        private System.Windows.Forms.CheckBox checkBox109;
        private System.Windows.Forms.CheckBox checkBox110;
        private System.Windows.Forms.CheckBox checkBox111;
        private System.Windows.Forms.CheckBox checkBox112;
        private System.Windows.Forms.CheckBox checkBox113;
        private System.Windows.Forms.CheckBox checkBox114;
        private System.Windows.Forms.CheckBox checkBox115;
        private System.Windows.Forms.CheckBox checkBox116;
        private System.Windows.Forms.CheckBox checkBox117;
        private System.Windows.Forms.CheckBox checkBox118;
        private System.Windows.Forms.CheckBox checkBox119;
        private System.Windows.Forms.CheckBox checkBox120;
        private System.Windows.Forms.CheckBox checkBox81;
        private System.Windows.Forms.CheckBox checkBox82;
        private System.Windows.Forms.CheckBox checkBox83;
        private System.Windows.Forms.CheckBox checkBox84;
        private System.Windows.Forms.CheckBox checkBox85;
        private System.Windows.Forms.CheckBox checkBox86;
        private System.Windows.Forms.CheckBox checkBox87;
        private System.Windows.Forms.CheckBox checkBox88;
        private System.Windows.Forms.CheckBox checkBox89;
        private System.Windows.Forms.CheckBox checkBox90;
        private System.Windows.Forms.CheckBox checkBox91;
        private System.Windows.Forms.CheckBox checkBox92;
        private System.Windows.Forms.CheckBox checkBox93;
        private System.Windows.Forms.CheckBox checkBox94;
        private System.Windows.Forms.CheckBox checkBox95;
        private System.Windows.Forms.CheckBox checkBox96;
        private System.Windows.Forms.CheckBox checkBox97;
        private System.Windows.Forms.CheckBox checkBox98;
        private System.Windows.Forms.CheckBox checkBox99;
        private System.Windows.Forms.CheckBox checkBox100;
        private System.Windows.Forms.CheckBox checkBox61;
        private System.Windows.Forms.CheckBox checkBox62;
        private System.Windows.Forms.CheckBox checkBox63;
        private System.Windows.Forms.CheckBox checkBox64;
        private System.Windows.Forms.CheckBox checkBox65;
        private System.Windows.Forms.CheckBox checkBox66;
        private System.Windows.Forms.CheckBox checkBox67;
        private System.Windows.Forms.CheckBox checkBox68;
        private System.Windows.Forms.CheckBox checkBox69;
        private System.Windows.Forms.CheckBox checkBox70;
        private System.Windows.Forms.CheckBox checkBox71;
        private System.Windows.Forms.CheckBox checkBox72;
        private System.Windows.Forms.CheckBox checkBox73;
        private System.Windows.Forms.CheckBox checkBox74;
        private System.Windows.Forms.CheckBox checkBox75;
        private System.Windows.Forms.CheckBox checkBox76;
        private System.Windows.Forms.CheckBox checkBox77;
        private System.Windows.Forms.CheckBox checkBox78;
        private System.Windows.Forms.CheckBox checkBox79;
        private System.Windows.Forms.CheckBox checkBox80;
        private System.Windows.Forms.CheckBox checkBox41;
        private System.Windows.Forms.CheckBox checkBox42;
        private System.Windows.Forms.CheckBox checkBox43;
        private System.Windows.Forms.CheckBox checkBox44;
        private System.Windows.Forms.CheckBox checkBox45;
        private System.Windows.Forms.CheckBox checkBox46;
        private System.Windows.Forms.CheckBox checkBox47;
        private System.Windows.Forms.CheckBox checkBox48;
        private System.Windows.Forms.CheckBox checkBox49;
        private System.Windows.Forms.CheckBox checkBox50;
        private System.Windows.Forms.CheckBox checkBox51;
        private System.Windows.Forms.CheckBox checkBox52;
        private System.Windows.Forms.CheckBox checkBox53;
        private System.Windows.Forms.CheckBox checkBox54;
        private System.Windows.Forms.CheckBox checkBox55;
        private System.Windows.Forms.CheckBox checkBox56;
        private System.Windows.Forms.CheckBox checkBox57;
        private System.Windows.Forms.CheckBox checkBox58;
        private System.Windows.Forms.CheckBox checkBox59;
        private System.Windows.Forms.CheckBox checkBox60;
        private System.Windows.Forms.CheckBox checkBox21;
        private System.Windows.Forms.CheckBox checkBox22;
        private System.Windows.Forms.CheckBox checkBox23;
        private System.Windows.Forms.CheckBox checkBox24;
        private System.Windows.Forms.CheckBox checkBox25;
        private System.Windows.Forms.CheckBox checkBox26;
        private System.Windows.Forms.CheckBox checkBox27;
        private System.Windows.Forms.CheckBox checkBox28;
        private System.Windows.Forms.CheckBox checkBox29;
        private System.Windows.Forms.CheckBox checkBox30;
        private System.Windows.Forms.CheckBox checkBox31;
        private System.Windows.Forms.CheckBox checkBox32;
        private System.Windows.Forms.CheckBox checkBox33;
        private System.Windows.Forms.CheckBox checkBox34;
        private System.Windows.Forms.CheckBox checkBox35;
        private System.Windows.Forms.CheckBox checkBox36;
        private System.Windows.Forms.CheckBox checkBox37;
        private System.Windows.Forms.CheckBox checkBox38;
        private System.Windows.Forms.CheckBox checkBox39;
        private System.Windows.Forms.CheckBox checkBox40;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBox2;
        private GEI_ExtrusionDataSet1 gEI_ExtrusionDataSet1;
        private System.Windows.Forms.BindingSource tbl3DielogBindingSource;
        private GEI_ExtrusionDataSet1TableAdapters.tbl3DielogTableAdapter tbl3DielogTableAdapter;
        private GEI_ExtrusionDataSet1TableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button18;
    }
}

