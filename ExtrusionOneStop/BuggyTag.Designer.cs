﻿namespace ExtrusionOneStop
{
    partial class BuggyTag
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.BuggyTagBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.GEI_ExtrusionDataSet = new ExtrusionOneStop.GEI_ExtrusionDataSet();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.BuggyTagBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GEI_ExtrusionDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // BuggyTagBindingSource
            // 
            this.BuggyTagBindingSource.DataMember = "BuggyTag";
            this.BuggyTagBindingSource.DataSource = this.GEI_ExtrusionDataSet;
            // 
            // GEI_ExtrusionDataSet
            // 
            this.GEI_ExtrusionDataSet.DataSetName = "GEI_ExtrusionDataSet";
            this.GEI_ExtrusionDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportViewer1
            // 
            reportDataSource1.Name = "BuggyTag";
            reportDataSource1.Value = this.BuggyTagBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "ExtrusionOneStop.BuggyTag.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(5, 12);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(521, 385);
            this.reportViewer1.TabIndex = 0;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(5, 403);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(521, 128);
            this.dataGridView1.TabIndex = 1;
            // 
            // BuggyTag
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(535, 543);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.reportViewer1);
            this.Name = "BuggyTag";
            this.Text = "BuggyTag";
            this.Load += new System.EventHandler(this.BuggyTag_Load);
            ((System.ComponentModel.ISupportInitialize)(this.BuggyTagBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GEI_ExtrusionDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource BuggyTagBindingSource;
        private GEI_ExtrusionDataSet GEI_ExtrusionDataSet;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}