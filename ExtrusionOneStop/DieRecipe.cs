﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data.SqlTypes;
using Oracle.ManagedDataAccess.Client;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Security.Principal;
using System.Security.Authentication;
using System.Drawing.Printing;
using System.IO;

namespace ExtrusionOneStop
{
   
    public partial class DieRecipe : Form
    {
        bool flag = true;
        bool flag2 = true;
        bool flag3 = true;
        bool flag4 = true;
        bool flag5 = true;
        bool flag6 = true;
        bool flag7 = true;
        bool flag8 = true;

        public DieRecipe()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            groupBox1.Hide();
            groupBox2.Hide();
            groupBox3.Hide();


        }
        private void Button9_Click(object sender, EventArgs e)
        {

            string geiDieNum = textBox28.Text + textBox2.Text;
            geiDieNum = geiDieNum.Substring(1, 5);
            string geiDieNum2 = textBox28.Text + textBox2.Text;

            MessageBox.Show(geiDieNum);

            string connString = System.Configuration.ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString.ToString();
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand("Select Die_Number, Alloy, Date_entered, Date_changed, Changed_by, Ext_speed, Ext_speed_goal, Billet_temp, " +
                "Container_temp, Curves, Fan_percent, Saw_to_weld_scrap, Weld_to_die_scrap, Billet_length, Upper_Zone_Even_Water, Upper_Zone_Odd_Water, " +
                "Upper_Zone_Even_Dead, Upper_Zone_Odd_Dead, Upper_Zone1_Water, Upper_Zone1_Dead, Upper_Zone2_Water, Upper_Zone2_Dead, Upper_Zone3_Water, " +
                "Upper_Zone3_Dead, Upper_Zone4_Water, Upper_Zone4_Dead, Upper_Zone5_Water, Upper_Zone5_Dead, Upper_Zone6_Water, Upper_Zone6_Dead, " +
                "Upper_Zone7_Water, Upper_Zone7_Dead, Upper_Zone8_Water, Upper_Zone8_Dead, Upper_Zone9_Water, Upper_Zone9_Dead, Upper_Zone10_Water, " +
                "Upper_Zone10_Dead, Upper_Zone11_Water, Upper_Zone11_Dead, Upper_Zone12_Water, Upper_Zone12_Dead, Upper_Zone13_Water, Upper_Zone13_Dead, " +
                "Upper_Zone14_Water, Upper_Zone14_Dead, Upper_Zone15_Water, Upper_Zone15_Dead, Upper_Zone16_Water, Upper_Zone16_Dead, Upper_Zone17_Water, " +
                "Upper_Zone17_Dead, Upper_Zone18_Water, Upper_Zone18_Dead, Upper_Zone19_Water, Upper_Zone19_Dead, Upper_Zone20_Water, Upper_Zone20_Dead, " +
                "Upper_Zone21_Water, Upper_Zone21_Dead, Upper_Zone22_Water, Upper_Zone22_Dead, Upper_Zone23_Water, Upper_Zone23_Dead, Upper_Zone24_Water, " +
                "Upper_Zone24_Dead, Upper_Zone25_Water, Upper_Zone25_Dead, Upper_Zone26_Water, Upper_Zone26_Dead, Upper_Zone27_Water, Upper_Zone27_Dead, " +
                "Upper_Zone28_Water, Upper_Zone28_Dead, Upper_Zone29_Water, Upper_Zone29_Dead, Upper_Zone30_Water, Upper_Zone30_Dead, Lower_Zone_Even_Water, " +
                "Lower_Zone_Odd_Water, Lower_Zone_Even_Dead, Lower_Zone_Odd_Dead, Lower_Zone1_Water, Lower_Zone1_Dead, Lower_Zone2_Water, Lower_Zone2_Dead, " +
                "Lower_Zone3_Water, Lower_Zone3_Dead, Lower_Zone4_Water, Lower_Zone4_Dead, Lower_Zone5_Water, Lower_Zone5_Dead, Lower_Zone6_Water, " +
                "Lower_Zone6_Dead, Lower_Zone7_Water, Lower_Zone7_Dead, Lower_Zone8_Water, Lower_Zone8_Dead, Lower_Zone9_Water, Lower_Zone9_Dead, " +
                "Lower_Zone10_Water, Lower_Zone10_Dead, Lower_Zone11_Water, Lower_Zone11_Dead, Lower_Zone12_Water, Lower_Zone12_Dead, Lower_Zone13_Water, " +
                "Lower_Zone13_Dead, Lower_Zone14_Water, Lower_Zone14_Dead, Lower_Zone15_Water, Lower_Zone15_Dead, Lower_Zone16_Water, Lower_Zone16_Dead, " +
                "Lower_Zone17_Water, Lower_Zone17_Dead, Lower_Zone18_Water, Lower_Zone18_Dead, Lower_Zone19_Water, Lower_Zone19_Dead, Lower_Zone20_Water, " +
                "Lower_Zone20_Dead, Lower_Zone21_Water, Lower_Zone21_Dead, Lower_Zone22_Water, Lower_Zone22_Dead, Lower_Zone23_Water, Lower_Zone23_Dead, " +
                "Lower_Zone24_Water, Lower_Zone24_Dead, Lower_Zone25_Water, Lower_Zone25_Dead, Lower_Zone26_Water, Lower_Zone26_Dead, Lower_Zone27_Water, " +
                "Lower_Zone27_Dead, Lower_Zone28_Water, Lower_Zone28_Dead, Lower_Zone29_Water, Lower_Zone29_Dead, Lower_Zone30_Water, Lower_Zone30_Dead, " +
                "Upper_Air_Speed, Upper_Air_Dead_Speed, Lower_Air_Speed, Lower_Air_Dead_Speed, Water_Pump_Speed, Water_Pump_Dead_Speed, BTP, Butt_length, " +
                "Ramp_start_perc, Ramp_time, Comment, Ext_speed_2, Speed_change_pos FROM dbo.tblPress2_Die_Info_Alloy " +
                "WHERE Die_Number = '" + geiDieNum + "'", conn);

            SqlCommand cmd2 = new SqlCommand("Select Die_Number, PO_Number, Req_Number, Customer, Vendor, Size, Holes, BKR, BOL, FP, Temp, Date_ordered, " +
                "Due, Received, Die_price, Expedite FROM dbo.tbl3Dielog where Die_Number = '" + geiDieNum2 + "'", conn);
            



            conn.Open();
            SqlDataReader myreader;
            SqlDataReader myreader2;

            myreader = cmd.ExecuteReader();

            while (myreader.Read())
            {
               try
               {            

                    textBox1.Text = (myreader["Die_Number"].ToString());
                    comboBox1.Text = (myreader["Alloy"].ToString());
                    textBox3.Text = (myreader["Date_entered"].ToString());
                    textBox4.Text = (myreader["Ext_speed"].ToString());
                    textBox5.Text = (myreader["Billet_temp"].ToString());
                    textBox6.Text = (myreader["Billet_length"].ToString());
                    textBox7.Text = (myreader["Container_temp"].ToString());
                    textBox8.Text = (myreader["Curves"].ToString());
                    textBox9.Text = (myreader["Fan_percent"].ToString());
                    textBox10.Text = (myreader["Saw_to_weld_scrap"].ToString());
                    textBox11.Text = (myreader["Weld_to_die_scrap"].ToString());
                    textBox12.Text = (myreader["BTP"].ToString());
                    textBox13.Text = (myreader["Butt_length"].ToString());
                    textBox16.Text = (myreader["Ext_speed_goal"].ToString());
                    textBox17.Text = (myreader["Ext_speed_2"].ToString());
                    textBox18.Text = (myreader["Ramp_start_perc"].ToString());
                    textBox19.Text = (myreader["Ramp_time"].ToString());
                    textBox20.Text = (myreader["Speed_change_pos"].ToString());

                    textBox14.Text = (myreader["Date_changed"].ToString());
                    textBox15.Text = (myreader["Changed_by"].ToString());
                    textBox21.Text = (myreader["Comment"].ToString());

                    textBox22.Text = (myreader["Upper_Air_Speed"].ToString());
                    textBox23.Text = (myreader["Upper_Air_Dead_Speed"].ToString());
                    textBox24.Text = (myreader["Lower_Air_Speed"].ToString());
                    textBox25.Text = (myreader["Lower_Air_Dead_Speed"].ToString());
                    textBox26.Text = (myreader["Water_Pump_Speed"].ToString());
                    textBox27.Text = (myreader["Water_Pump_Dead_Speed"].ToString());
                    //UZ 1-30
                    checkBox1.Checked = Convert.ToBoolean(myreader["Upper_Zone1_Water"]);
                    checkBox2.Checked = Convert.ToBoolean(myreader["Upper_Zone1_Dead"]);
                    checkBox3.Checked = Convert.ToBoolean(myreader["Upper_Zone2_Water"]);
                    checkBox4.Checked = Convert.ToBoolean(myreader["Upper_Zone2_Dead"]);
                    checkBox5.Checked = Convert.ToBoolean(myreader["Upper_Zone3_Water"]);
                    checkBox6.Checked = Convert.ToBoolean(myreader["Upper_Zone3_Dead"]);
                    checkBox7.Checked = Convert.ToBoolean(myreader["Upper_Zone4_Water"]);
                    checkBox8.Checked = Convert.ToBoolean(myreader["Upper_Zone4_Dead"]);
                    checkBox9.Checked = Convert.ToBoolean(myreader["Upper_Zone5_Water"]);
                    checkBox10.Checked = Convert.ToBoolean(myreader["Upper_Zone5_Dead"]);
                    checkBox11.Checked = Convert.ToBoolean(myreader["Upper_Zone6_Water"]);
                    checkBox12.Checked = Convert.ToBoolean(myreader["Upper_Zone6_Dead"]);
                    checkBox13.Checked = Convert.ToBoolean(myreader["Upper_Zone7_Water"]);
                    checkBox14.Checked = Convert.ToBoolean(myreader["Upper_Zone7_Dead"]);
                    checkBox15.Checked = Convert.ToBoolean(myreader["Upper_Zone8_Water"]);
                    checkBox16.Checked = Convert.ToBoolean(myreader["Upper_Zone8_Dead"]);
                    checkBox17.Checked = Convert.ToBoolean(myreader["Upper_Zone9_Water"]);
                    checkBox18.Checked = Convert.ToBoolean(myreader["Upper_Zone9_Dead"]);
                    checkBox19.Checked = Convert.ToBoolean(myreader["Upper_Zone10_Water"]);
                    checkBox20.Checked = Convert.ToBoolean(myreader["Upper_Zone10_Dead"]);
                    checkBox21.Checked = Convert.ToBoolean(myreader["Upper_Zone11_Water"]);
                    checkBox22.Checked = Convert.ToBoolean(myreader["Upper_Zone11_Dead"]);
                    checkBox23.Checked = Convert.ToBoolean(myreader["Upper_Zone12_Water"]);
                    checkBox24.Checked = Convert.ToBoolean(myreader["Upper_Zone12_Dead"]);
                    checkBox25.Checked = Convert.ToBoolean(myreader["Upper_Zone13_Water"]);
                    checkBox26.Checked = Convert.ToBoolean(myreader["Upper_Zone13_Dead"]);
                    checkBox27.Checked = Convert.ToBoolean(myreader["Upper_Zone14_Water"]);
                    checkBox28.Checked = Convert.ToBoolean(myreader["Upper_Zone14_Dead"]);
                    checkBox29.Checked = Convert.ToBoolean(myreader["Upper_Zone15_Water"]);
                    checkBox30.Checked = Convert.ToBoolean(myreader["Upper_Zone15_Dead"]);
                    checkBox31.Checked = Convert.ToBoolean(myreader["Upper_Zone16_Water"]);
                    checkBox32.Checked = Convert.ToBoolean(myreader["Upper_Zone16_Dead"]);
                    checkBox33.Checked = Convert.ToBoolean(myreader["Upper_Zone17_Water"]);
                    checkBox34.Checked = Convert.ToBoolean(myreader["Upper_Zone17_Dead"]);
                    checkBox35.Checked = Convert.ToBoolean(myreader["Upper_Zone18_Water"]);
                    checkBox36.Checked = Convert.ToBoolean(myreader["Upper_Zone18_Dead"]);
                    checkBox37.Checked = Convert.ToBoolean(myreader["Upper_Zone19_Water"]);
                    checkBox38.Checked = Convert.ToBoolean(myreader["Upper_Zone19_Dead"]);
                    checkBox39.Checked = Convert.ToBoolean(myreader["Upper_Zone20_Water"]);
                    checkBox40.Checked = Convert.ToBoolean(myreader["Upper_Zone20_Dead"]);
                    checkBox41.Checked = Convert.ToBoolean(myreader["Upper_Zone21_Water"]);
                    checkBox42.Checked = Convert.ToBoolean(myreader["Upper_Zone21_Dead"]);
                    checkBox43.Checked = Convert.ToBoolean(myreader["Upper_Zone22_Water"]);
                    checkBox44.Checked = Convert.ToBoolean(myreader["Upper_Zone22_Dead"]);
                    checkBox45.Checked = Convert.ToBoolean(myreader["Upper_Zone23_Water"]);
                    checkBox46.Checked = Convert.ToBoolean(myreader["Upper_Zone23_Dead"]);
                    checkBox47.Checked = Convert.ToBoolean(myreader["Upper_Zone24_Water"]);
                    checkBox48.Checked = Convert.ToBoolean(myreader["Upper_Zone24_Dead"]);
                    checkBox49.Checked = Convert.ToBoolean(myreader["Upper_Zone25_Water"]);
                    checkBox50.Checked = Convert.ToBoolean(myreader["Upper_Zone25_Dead"]);
                    checkBox51.Checked = Convert.ToBoolean(myreader["Upper_Zone26_Water"]);
                    checkBox52.Checked = Convert.ToBoolean(myreader["Upper_Zone26_Dead"]);
                    checkBox53.Checked = Convert.ToBoolean(myreader["Upper_Zone27_Water"]);
                    checkBox54.Checked = Convert.ToBoolean(myreader["Upper_Zone27_Dead"]);
                    checkBox55.Checked = Convert.ToBoolean(myreader["Upper_Zone28_Water"]);
                    checkBox56.Checked = Convert.ToBoolean(myreader["Upper_Zone28_Dead"]);
                    checkBox57.Checked = Convert.ToBoolean(myreader["Upper_Zone29_Water"]);
                    checkBox58.Checked = Convert.ToBoolean(myreader["Upper_Zone29_Dead"]);
                    checkBox59.Checked = Convert.ToBoolean(myreader["Upper_Zone30_Water"]);
                    checkBox60.Checked = Convert.ToBoolean(myreader["Upper_Zone30_Dead"]);
                    //LZ 1-30
                    checkBox61.Checked = Convert.ToBoolean(myreader["Lower_Zone1_Water"]);
                    checkBox62.Checked = Convert.ToBoolean(myreader["Lower_Zone1_Dead"]);
                    checkBox63.Checked = Convert.ToBoolean(myreader["Lower_Zone2_Water"]);
                    checkBox64.Checked = Convert.ToBoolean(myreader["Lower_Zone2_Dead"]);
                    checkBox65.Checked = Convert.ToBoolean(myreader["Lower_Zone3_Water"]);
                    checkBox66.Checked = Convert.ToBoolean(myreader["Lower_Zone3_Dead"]);
                    checkBox67.Checked = Convert.ToBoolean(myreader["Lower_Zone4_Water"]);
                    checkBox68.Checked = Convert.ToBoolean(myreader["Lower_Zone4_Dead"]);
                    checkBox69.Checked = Convert.ToBoolean(myreader["Lower_Zone5_Water"]);
                    checkBox70.Checked = Convert.ToBoolean(myreader["Lower_Zone5_Dead"]);
                    checkBox71.Checked = Convert.ToBoolean(myreader["Lower_Zone6_Water"]);
                    checkBox72.Checked = Convert.ToBoolean(myreader["Lower_Zone6_Dead"]);
                    checkBox73.Checked = Convert.ToBoolean(myreader["Lower_Zone7_Water"]);
                    checkBox74.Checked = Convert.ToBoolean(myreader["Lower_Zone7_Dead"]);
                    checkBox75.Checked = Convert.ToBoolean(myreader["Lower_Zone8_Water"]);
                    checkBox76.Checked = Convert.ToBoolean(myreader["Lower_Zone8_Dead"]);
                    checkBox77.Checked = Convert.ToBoolean(myreader["Lower_Zone9_Water"]);
                    checkBox78.Checked = Convert.ToBoolean(myreader["Lower_Zone9_Dead"]);
                    checkBox79.Checked = Convert.ToBoolean(myreader["Lower_Zone10_Water"]);
                    checkBox80.Checked = Convert.ToBoolean(myreader["Lower_Zone10_Dead"]);
                    checkBox81.Checked = Convert.ToBoolean(myreader["Lower_Zone11_Water"]);
                    checkBox82.Checked = Convert.ToBoolean(myreader["Lower_Zone11_Dead"]);
                    checkBox83.Checked = Convert.ToBoolean(myreader["Lower_Zone12_Water"]);
                    checkBox84.Checked = Convert.ToBoolean(myreader["Lower_Zone12_Dead"]);
                    checkBox85.Checked = Convert.ToBoolean(myreader["Lower_Zone13_Water"]);
                    checkBox86.Checked = Convert.ToBoolean(myreader["Lower_Zone13_Dead"]);
                    checkBox87.Checked = Convert.ToBoolean(myreader["Lower_Zone14_Water"]);
                    checkBox88.Checked = Convert.ToBoolean(myreader["Lower_Zone14_Dead"]);
                    checkBox89.Checked = Convert.ToBoolean(myreader["Lower_Zone15_Water"]);
                    checkBox90.Checked = Convert.ToBoolean(myreader["Lower_Zone15_Dead"]);
                    checkBox91.Checked = Convert.ToBoolean(myreader["Lower_Zone16_Water"]);
                    checkBox92.Checked = Convert.ToBoolean(myreader["Lower_Zone16_Dead"]);
                    checkBox93.Checked = Convert.ToBoolean(myreader["Lower_Zone17_Water"]);
                    checkBox94.Checked = Convert.ToBoolean(myreader["Lower_Zone17_Dead"]);
                    checkBox95.Checked = Convert.ToBoolean(myreader["Lower_Zone18_Water"]);
                    checkBox96.Checked = Convert.ToBoolean(myreader["Lower_Zone18_Dead"]);
                    checkBox97.Checked = Convert.ToBoolean(myreader["Lower_Zone19_Water"]);
                    checkBox98.Checked = Convert.ToBoolean(myreader["Lower_Zone19_Dead"]);
                    checkBox99.Checked = Convert.ToBoolean(myreader["Lower_Zone20_Water"]);
                    checkBox100.Checked = Convert.ToBoolean(myreader["Lower_Zone20_Dead"]);
                    checkBox101.Checked = Convert.ToBoolean(myreader["Lower_Zone21_Water"]);
                    checkBox102.Checked = Convert.ToBoolean(myreader["Lower_Zone21_Dead"]);
                    checkBox103.Checked = Convert.ToBoolean(myreader["Lower_Zone22_Water"]);
                    checkBox104.Checked = Convert.ToBoolean(myreader["Lower_Zone22_Dead"]);
                    checkBox105.Checked = Convert.ToBoolean(myreader["Lower_Zone23_Water"]);
                    checkBox106.Checked = Convert.ToBoolean(myreader["Lower_Zone23_Dead"]);
                    checkBox107.Checked = Convert.ToBoolean(myreader["Lower_Zone24_Water"]);
                    checkBox108.Checked = Convert.ToBoolean(myreader["Lower_Zone24_Dead"]);
                    checkBox109.Checked = Convert.ToBoolean(myreader["Lower_Zone25_Water"]);
                    checkBox110.Checked = Convert.ToBoolean(myreader["Lower_Zone25_Dead"]);
                    checkBox111.Checked = Convert.ToBoolean(myreader["Lower_Zone26_Water"]);
                    checkBox112.Checked = Convert.ToBoolean(myreader["Lower_Zone26_Dead"]);
                    checkBox113.Checked = Convert.ToBoolean(myreader["Lower_Zone27_Water"]);
                    checkBox114.Checked = Convert.ToBoolean(myreader["Lower_Zone27_Dead"]);
                    checkBox115.Checked = Convert.ToBoolean(myreader["Lower_Zone28_Water"]);
                    checkBox116.Checked = Convert.ToBoolean(myreader["Lower_Zone28_Dead"]);
                    checkBox117.Checked = Convert.ToBoolean(myreader["Lower_Zone29_Water"]);
                    checkBox118.Checked = Convert.ToBoolean(myreader["Lower_Zone29_Dead"]);
                    checkBox119.Checked = Convert.ToBoolean(myreader["Lower_Zone30_Water"]);
                    checkBox120.Checked = Convert.ToBoolean(myreader["Lower_Zone30_Dead"]);
                    

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

            }
            myreader.Close();
            cmd.Dispose();

            myreader2 = cmd2.ExecuteReader();

            while (myreader2.Read())
            {
                try
                {
                    textBox29.Text = (myreader2["Die_Number"].ToString());
                    textBox30.Text = (myreader2["Req_Number"].ToString());
                    textBox31.Text = (myreader2["Customer"].ToString());
                    textBox32.Text = (myreader2["Vendor"].ToString());
                    textBox33.Text = (myreader2["Size"].ToString());
                    textBox34.Text = (myreader2["Holes"].ToString());
                    textBox35.Text = (myreader2["BKR"].ToString());
                    textBox36.Text = (myreader2["BOL"].ToString());
                    textBox37.Text = (myreader2["FP"].ToString());
                    textBox38.Text = (myreader2["Temp"].ToString());
                    textBox39.Text = (myreader2["Date_ordered"].ToString());
                    textBox40.Text = (myreader2["Due"].ToString());
                    textBox41.Text = (myreader2["Received"].ToString());
                    textBox42.Text = (myreader2["Expedite"].ToString());
                    textBox43.Text = (myreader2["Die_price"].ToString());
                }
                catch (Exception ex)
                {
                    //do some shit
                }
            }
            myreader2.Close();
            cmd2.Dispose();
            conn.Close();
            button10.Hide();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            //UZ Even Water On          
            if (flag)
            {
                button1.Text = "UZ Even Water Off";
                checkBox3.Checked = true;
                checkBox7.Checked = true;
                checkBox11.Checked = true;
                checkBox15.Checked = true;
                checkBox19.Checked = true;
                checkBox23.Checked = true;
                checkBox27.Checked = true;
                checkBox31.Checked = true;
                checkBox35.Checked = true;
                checkBox39.Checked = true;
                checkBox43.Checked = true;
                checkBox47.Checked = true;
                checkBox51.Checked = true;
                checkBox55.Checked = true;
                checkBox59.Checked = true;
                flag = false;

            }
            else
            {
                button1.Text = "UZ Even Water On";
                checkBox3.Checked = false;
                checkBox7.Checked = false;
                checkBox11.Checked = false;
                checkBox15.Checked = false;
                checkBox19.Checked = false;
                checkBox23.Checked = false;
                checkBox27.Checked = false;
                checkBox31.Checked = false;
                checkBox35.Checked = false;
                checkBox39.Checked = false;
                checkBox43.Checked = false;
                checkBox47.Checked = false;
                checkBox51.Checked = false;
                checkBox55.Checked = false;
                checkBox59.Checked = false;
                flag = true;

            }


        }

        private void Button2_Click(object sender, EventArgs e)
        {
            //UZ Even Dead On
            if (flag2)
            {
                button2.Text = "UZ Even Dead Off";
                checkBox4.Checked = true;
                checkBox8.Checked = true;
                checkBox12.Checked = true;
                checkBox16.Checked = true;
                checkBox20.Checked = true;
                checkBox24.Checked = true;
                checkBox28.Checked = true;
                checkBox32.Checked = true;
                checkBox36.Checked = true;
                checkBox40.Checked = true;
                checkBox44.Checked = true;
                checkBox48.Checked = true;
                checkBox52.Checked = true;
                checkBox56.Checked = true;
                checkBox60.Checked = true;
                flag2 = false;

            }
            else
            {
                button2.Text = "UZ Even Dead On";
                checkBox4.Checked = false;
                checkBox8.Checked = false;
                checkBox12.Checked = false;
                checkBox16.Checked = false;
                checkBox20.Checked = false;
                checkBox24.Checked = false;
                checkBox28.Checked = false;
                checkBox32.Checked = false;
                checkBox36.Checked = false;
                checkBox40.Checked = false;
                checkBox44.Checked = false;
                checkBox48.Checked = false;
                checkBox52.Checked = false;
                checkBox56.Checked = false;
                checkBox60.Checked = false;
                flag2 = true;

            }
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            //UZ Odd Water On
            if (flag3)
            {
                button3.Text = "UZ Odd Water Off";
                checkBox1.Checked = true;
                checkBox5.Checked = true;
                checkBox9.Checked = true;
                checkBox13.Checked = true;
                checkBox17.Checked = true;
                checkBox21.Checked = true;
                checkBox25.Checked = true;
                checkBox29.Checked = true;
                checkBox33.Checked = true;
                checkBox37.Checked = true;
                checkBox41.Checked = true;
                checkBox45.Checked = true;
                checkBox49.Checked = true;
                checkBox53.Checked = true;
                checkBox57.Checked = true;
                
                flag3 = false;

            }
            else
            {
                button3.Text = "UZ Odd Water On";
                checkBox1.Checked = false;
                checkBox5.Checked = false;
                checkBox9.Checked = false;
                checkBox13.Checked = false;
                checkBox17.Checked = false;
                checkBox21.Checked = false;
                checkBox25.Checked = false;
                checkBox29.Checked = false;
                checkBox33.Checked = false;
                checkBox37.Checked = false;
                checkBox41.Checked = false;
                checkBox45.Checked = false;
                checkBox49.Checked = false;
                checkBox53.Checked = false;
                checkBox57.Checked = false;
                flag3 = true;

            }
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            //UZ Odd Dead On
            if (flag4)
            {
                button4.Text = "UZ Odd Dead Off";
                checkBox2.Checked = true;
                checkBox6.Checked = true;
                checkBox10.Checked = true;
                checkBox14.Checked = true;
                checkBox18.Checked = true;
                checkBox22.Checked = true;
                checkBox26.Checked = true;
                checkBox30.Checked = true;
                checkBox34.Checked = true;
                checkBox38.Checked = true;
                checkBox42.Checked = true;
                checkBox46.Checked = true;
                checkBox50.Checked = true;
                checkBox54.Checked = true;
                checkBox58.Checked = true;

                flag4 = false;

            }
            else
            {
                button4.Text = "UZ Odd Dead On";
                checkBox2.Checked = false;
                checkBox6.Checked = false;
                checkBox10.Checked = false;
                checkBox14.Checked = false;
                checkBox18.Checked = false;
                checkBox22.Checked = false;
                checkBox26.Checked = false;
                checkBox30.Checked = false;
                checkBox34.Checked = false;
                checkBox38.Checked = false;
                checkBox42.Checked = false;
                checkBox46.Checked = false;
                checkBox50.Checked = false;
                checkBox54.Checked = false;
                checkBox58.Checked = false;
                flag4 = true;

            }
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            //LZ Even Water On
            if (flag5)
            {
                button5.Text = "LZ Even Water Off";
                checkBox63.Checked = true;
                checkBox67.Checked = true;
                checkBox71.Checked = true;
                checkBox75.Checked = true;
                checkBox79.Checked = true;
                checkBox83.Checked = true;
                checkBox87.Checked = true;
                checkBox91.Checked = true;
                checkBox95.Checked = true;
                checkBox99.Checked = true;
                checkBox103.Checked = true;
                checkBox107.Checked = true;
                checkBox111.Checked = true;
                checkBox115.Checked = true;
                checkBox119.Checked = true;

                flag5 = false;

            }
            else
            {
                button5.Text = "LZ Even Water On";
                checkBox63.Checked = false;
                checkBox67.Checked = false;
                checkBox71.Checked = false;
                checkBox75.Checked = false;
                checkBox79.Checked = false;
                checkBox83.Checked = false;
                checkBox87.Checked = false;
                checkBox91.Checked = false;
                checkBox95.Checked = false;
                checkBox99.Checked = false;
                checkBox103.Checked = false;
                checkBox107.Checked = false;
                checkBox111.Checked = false;
                checkBox115.Checked = false;
                checkBox119.Checked = false;
                flag5 = true;

            }

        }

        private void Button6_Click(object sender, EventArgs e)
        {
            //LZ Even Dead On
            if (flag6)
            {
                button6.Text = "LZ Even Dead Off";
                checkBox64.Checked = true;
                checkBox68.Checked = true;
                checkBox72.Checked = true;
                checkBox76.Checked = true;
                checkBox80.Checked = true;
                checkBox84.Checked = true;
                checkBox88.Checked = true;
                checkBox92.Checked = true;
                checkBox96.Checked = true;
                checkBox100.Checked = true;
                checkBox104.Checked = true;
                checkBox108.Checked = true;
                checkBox112.Checked = true;
                checkBox116.Checked = true;
                checkBox120.Checked = true;

                flag6 = false;

            }
            else
            {
                button6.Text = "LZ Even Dead On";
                checkBox64.Checked = false;
                checkBox68.Checked = false;
                checkBox72.Checked = false;
                checkBox76.Checked = false;
                checkBox80.Checked = false;
                checkBox84.Checked = false;
                checkBox88.Checked = false;
                checkBox92.Checked = false;
                checkBox96.Checked = false;
                checkBox100.Checked = false;
                checkBox104.Checked = false;
                checkBox108.Checked = false;
                checkBox112.Checked = false;
                checkBox116.Checked = false;
                checkBox120.Checked = false;
                flag6 = true;

            }
        }

        private void Button7_Click(object sender, EventArgs e)
        {

            //LZ Odd Water On
            if (flag7)
            {
                button7.Text = "LZ Odd Water Off";
                checkBox61.Checked = true;
                checkBox65.Checked = true;
                checkBox69.Checked = true;
                checkBox73.Checked = true;
                checkBox77.Checked = true;
                checkBox81.Checked = true;
                checkBox85.Checked = true;
                checkBox89.Checked = true;
                checkBox93.Checked = true;
                checkBox97.Checked = true;
                checkBox101.Checked = true;
                checkBox105.Checked = true;
                checkBox109.Checked = true;
                checkBox113.Checked = true;
                checkBox117.Checked = true;

                flag7 = false;

            }
            else
            {
                button7.Text = "LZ Odd Water On";
                checkBox61.Checked = false;
                checkBox65.Checked = false;
                checkBox69.Checked = false;
                checkBox73.Checked = false;
                checkBox77.Checked = false;
                checkBox81.Checked = false;
                checkBox85.Checked = false;
                checkBox89.Checked = false;
                checkBox93.Checked = false;
                checkBox97.Checked = false;
                checkBox101.Checked = false;
                checkBox105.Checked = false;
                checkBox109.Checked = false;
                checkBox113.Checked = false;
                checkBox117.Checked = false;
                flag7 = true;

            }
            
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            //LZ Odd Dead On
            if (flag8)
            {
                button8.Text = "LZ Odd Dead Off";
                checkBox62.Checked = true;
                checkBox66.Checked = true;
                checkBox70.Checked = true;
                checkBox74.Checked = true;
                checkBox78.Checked = true;
                checkBox82.Checked = true;
                checkBox86.Checked = true;
                checkBox90.Checked = true;
                checkBox94.Checked = true;
                checkBox98.Checked = true;
                checkBox102.Checked = true;
                checkBox106.Checked = true;
                checkBox110.Checked = true;
                checkBox114.Checked = true;
                checkBox118.Checked = true;

                flag8 = false;

            }
            else
            {
                button8.Text = "LZ Odd Dead On";
                checkBox62.Checked = false;
                checkBox66.Checked = false;
                checkBox70.Checked = false;
                checkBox74.Checked = false;
                checkBox78.Checked = false;
                checkBox82.Checked = false;
                checkBox86.Checked = false;
                checkBox90.Checked = false;
                checkBox94.Checked = false;
                checkBox98.Checked = false;
                checkBox102.Checked = false;
                checkBox106.Checked = false;
                checkBox110.Checked = false;
                checkBox114.Checked = false;
                checkBox118.Checked = false;
                flag8 = true;

            }
        }

        private void Button10_Click(object sender, EventArgs e)
        {
            string dieNum = ""; 
            string alloy = ""; 
            string dateEntered = ""; 
            string dateChanged = "";
            string changedBy = ""; 
            double extSpeed = 0;
            double billetTemp = 0;            
            int billetLength = 0;            
            double containerTemp = 0;            
            double curves = 0;            
            double fanPercent = 0;            
            int sawtoweldScrap = 0;            
            int weldtodieScrap = 0;            
            int BTP = 0;            
            int buttLength = 0;            
            double extspeedGoal = 0;            
            double extspeed2 = 0;            
            int rampstartPercent = 0;            
            int rampTime = 0;            
            double speedchangePos = 0;
            string comments = ""; 
            int upperairSpeed = 0;            
            int upperairdeadSpeed = 0;            
            int lowerairSpeed = 0;            
            int lowerairdeadSpeed = 0;            
            int waterpumpSpeed = 0;            
            int waterpumpdeadSpeed = 0;
            try
            {
                dieNum = textBox1.Text;
                alloy = comboBox1.SelectedItem.ToString();
                dateEntered = textBox3.Text;
                dateChanged = textBox14.Text;
                comments = textBox21.Text;
                changedBy = textBox15.Text; ;
                extSpeed = Convert.ToDouble(textBox4.Text);
                billetTemp = Convert.ToDouble(textBox5.Text);
                billetLength = Convert.ToInt32(textBox6.Text);
                containerTemp = Convert.ToDouble(textBox7.Text);
                curves = Convert.ToDouble(textBox8.Text);
                fanPercent = Convert.ToDouble(textBox9.Text);
                sawtoweldScrap = Convert.ToInt32(textBox10.Text);
                weldtodieScrap = Convert.ToInt32(textBox11.Text);
                BTP = Convert.ToInt32(textBox12.Text);
                extspeedGoal = Convert.ToDouble(textBox16.Text);
                buttLength = Convert.ToInt32(textBox13.Text);
                extspeed2 = Convert.ToDouble(textBox17.Text);
                rampstartPercent = Convert.ToInt32(textBox18.Text);
                rampTime = Convert.ToInt32(textBox19.Text);
                speedchangePos = Convert.ToDouble(textBox20.Text);
                upperairSpeed = Convert.ToInt32(textBox22.Text);
                upperairdeadSpeed = Convert.ToInt32(textBox23.Text);
                lowerairSpeed = Convert.ToInt32(textBox24.Text);
                lowerairdeadSpeed = Convert.ToInt32(textBox25.Text);
                waterpumpSpeed = Convert.ToInt32(textBox26.Text);
                waterpumpdeadSpeed = Convert.ToInt32(textBox27.Text);
            }
            catch (Exception ex)
            {
                //do somethin
            }
            bool var1 = checkBox1.Checked;
            bool var2 = checkBox2.Checked;
            bool var3 = checkBox3.Checked;
            bool var4 = checkBox4.Checked;
            bool var5 = checkBox5.Checked;
            bool var6 = checkBox6.Checked;
            bool var7 = checkBox7.Checked;
            bool var8 = checkBox8.Checked;
            bool var9 = checkBox9.Checked;
            bool var10 = checkBox10.Checked;
            bool var11 = checkBox11.Checked;
            bool var12 = checkBox12.Checked;
            bool var13 = checkBox13.Checked;
            bool var14 = checkBox14.Checked;
            bool var15 = checkBox15.Checked;
            bool var16 = checkBox16.Checked;
            bool var17 = checkBox17.Checked;
            bool var18 = checkBox18.Checked;
            bool var19 = checkBox19.Checked;
            bool var20 = checkBox20.Checked;
            bool var21 = checkBox21.Checked;
            bool var22 = checkBox22.Checked;
            bool var23 = checkBox23.Checked;
            bool var24 = checkBox24.Checked;
            bool var25 = checkBox25.Checked;
            bool var26 = checkBox26.Checked;
            bool var27 = checkBox27.Checked;
            bool var28 = checkBox28.Checked;
            bool var29 = checkBox29.Checked;
            bool var30 = checkBox30.Checked;
            bool var31 = checkBox31.Checked;
            bool var32 = checkBox32.Checked;
            bool var33 = checkBox33.Checked;
            bool var34 = checkBox34.Checked;
            bool var35 = checkBox35.Checked;
            bool var36 = checkBox36.Checked;
            bool var37 = checkBox37.Checked;
            bool var38 = checkBox38.Checked;
            bool var39 = checkBox39.Checked;
            bool var40 = checkBox40.Checked;
            bool var41 = checkBox41.Checked;
            bool var42 = checkBox42.Checked;
            bool var43 = checkBox43.Checked;
            bool var44 = checkBox44.Checked;
            bool var45 = checkBox45.Checked;
            bool var46 = checkBox46.Checked;
            bool var47 = checkBox47.Checked;
            bool var48 = checkBox48.Checked;
            bool var49 = checkBox49.Checked;
            bool var50 = checkBox50.Checked;
            bool var51 = checkBox51.Checked;
            bool var52 = checkBox52.Checked;
            bool var53 = checkBox53.Checked;
            bool var54 = checkBox54.Checked;
            bool var55 = checkBox55.Checked;
            bool var56 = checkBox56.Checked;
            bool var57 = checkBox57.Checked;
            bool var58 = checkBox58.Checked;
            bool var59 = checkBox59.Checked;
            bool var60 = checkBox60.Checked;
            bool var61 = checkBox61.Checked;
            bool var62 = checkBox62.Checked;
            bool var63 = checkBox63.Checked;
            bool var64 = checkBox64.Checked;
            bool var65 = checkBox65.Checked;
            bool var66 = checkBox66.Checked;
            bool var67 = checkBox67.Checked;
            bool var68 = checkBox68.Checked;
            bool var69 = checkBox69.Checked;
            bool var70 = checkBox70.Checked;
            bool var71 = checkBox71.Checked;
            bool var72 = checkBox72.Checked;
            bool var73 = checkBox73.Checked;
            bool var74 = checkBox74.Checked;
            bool var75 = checkBox75.Checked;
            bool var76 = checkBox76.Checked;
            bool var77 = checkBox77.Checked;
            bool var78 = checkBox78.Checked;
            bool var79 = checkBox79.Checked;
            bool var80 = checkBox80.Checked;
            bool var81 = checkBox81.Checked;
            bool var82 = checkBox82.Checked;
            bool var83 = checkBox83.Checked;
            bool var84 = checkBox84.Checked;
            bool var85 = checkBox85.Checked;
            bool var86 = checkBox86.Checked;
            bool var87 = checkBox87.Checked;
            bool var88 = checkBox88.Checked;
            bool var89 = checkBox89.Checked;
            bool var90 = checkBox90.Checked;
            bool var91 = checkBox91.Checked;
            bool var92 = checkBox92.Checked;
            bool var93 = checkBox93.Checked;
            bool var94 = checkBox94.Checked;
            bool var95 = checkBox95.Checked;
            bool var96 = checkBox96.Checked;
            bool var97 = checkBox97.Checked;
            bool var98 = checkBox98.Checked;
            bool var99 = checkBox99.Checked;
            bool var100 = checkBox100.Checked;
            bool var101 = checkBox101.Checked;
            bool var102 = checkBox102.Checked;
            bool var103 = checkBox103.Checked;
            bool var104 = checkBox104.Checked;
            bool var105 = checkBox105.Checked;
            bool var106 = checkBox106.Checked;
            bool var107 = checkBox107.Checked;
            bool var108 = checkBox108.Checked;
            bool var109 = checkBox109.Checked;
            bool var110 = checkBox110.Checked;
            bool var111 = checkBox111.Checked;
            bool var112 = checkBox112.Checked;
            bool var113 = checkBox113.Checked;
            bool var114 = checkBox114.Checked;
            bool var115 = checkBox115.Checked;
            bool var116 = checkBox116.Checked;
            bool var117 = checkBox117.Checked;
            bool var118 = checkBox118.Checked;
            bool var119 = checkBox119.Checked;
            bool var120 = checkBox120.Checked;   


            string connString = System.Configuration.ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString.ToString();
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand("Insert into dbo.tblPress2_Die_Info_Alloy (Die_Number, Alloy, Date_entered, Date_changed, " +
                "Changed_by, Ext_speed, Ext_speed_goal, Billet_temp, " +
                "Container_temp, Curves, Fan_percent, Saw_to_weld_scrap, Weld_to_die_scrap, Billet_length, BTP, Upper_Air_Speed, " +
                "Upper_Air_Dead_Speed, Lower_Air_Speed, " +
                "Lower_Air_Dead_Speed, Water_Pump_Speed, Water_Pump_Dead_Speed, Butt_length, Ramp_start_perc, Ramp_time, Comment, Speed_change_pos, Ext_Speed_2, " +
                "Upper_Zone1_Water, Upper_Zone1_Dead, Upper_Zone2_Water, Upper_Zone2_Dead, Upper_Zone3_Water, " + //begin uz
                "Upper_Zone3_Dead, Upper_Zone4_Water, Upper_Zone4_Dead, Upper_Zone5_Water, Upper_Zone5_Dead, Upper_Zone6_Water, Upper_Zone6_Dead, " +
                "Upper_Zone7_Water, Upper_Zone7_Dead, Upper_Zone8_Water, Upper_Zone8_Dead, Upper_Zone9_Water, Upper_Zone9_Dead, Upper_Zone10_Water, " +
                "Upper_Zone10_Dead, Upper_Zone11_Water, Upper_Zone11_Dead, Upper_Zone12_Water, Upper_Zone12_Dead, Upper_Zone13_Water, Upper_Zone13_Dead, " +
                "Upper_Zone14_Water, Upper_Zone14_Dead, Upper_Zone15_Water, Upper_Zone15_Dead, Upper_Zone16_Water, Upper_Zone16_Dead, Upper_Zone17_Water, " +
                "Upper_Zone17_Dead, Upper_Zone18_Water, Upper_Zone18_Dead, Upper_Zone19_Water, Upper_Zone19_Dead, Upper_Zone20_Water, Upper_Zone20_Dead, " +
                "Upper_Zone21_Water, Upper_Zone21_Dead, Upper_Zone22_Water, Upper_Zone22_Dead, Upper_Zone23_Water, Upper_Zone23_Dead, Upper_Zone24_Water, " +
                "Upper_Zone24_Dead, Upper_Zone25_Water, Upper_Zone25_Dead, Upper_Zone26_Water, Upper_Zone26_Dead, Upper_Zone27_Water, Upper_Zone27_Dead, " +
                "Upper_Zone28_Water, Upper_Zone28_Dead, Upper_Zone29_Water, Upper_Zone29_Dead, Upper_Zone30_Water, Upper_Zone30_Dead, " +
                "Lower_Zone1_Water, Lower_Zone1_Dead, Lower_Zone2_Water, Lower_Zone2_Dead, " + //begin lz
                "Lower_Zone3_Water, Lower_Zone3_Dead, Lower_Zone4_Water, Lower_Zone4_Dead, Lower_Zone5_Water, Lower_Zone5_Dead, Lower_Zone6_Water, " +
                "Lower_Zone6_Dead, Lower_Zone7_Water, Lower_Zone7_Dead, Lower_Zone8_Water, Lower_Zone8_Dead, Lower_Zone9_Water, Lower_Zone9_Dead, " +
                "Lower_Zone10_Water, Lower_Zone10_Dead, Lower_Zone11_Water, Lower_Zone11_Dead, Lower_Zone12_Water, Lower_Zone12_Dead, Lower_Zone13_Water, " +
                "Lower_Zone13_Dead, Lower_Zone14_Water, Lower_Zone14_Dead, Lower_Zone15_Water, Lower_Zone15_Dead, Lower_Zone16_Water, Lower_Zone16_Dead, " +
                "Lower_Zone17_Water, Lower_Zone17_Dead, Lower_Zone18_Water, Lower_Zone18_Dead, Lower_Zone19_Water, Lower_Zone19_Dead, Lower_Zone20_Water, " +
                "Lower_Zone20_Dead, Lower_Zone21_Water, Lower_Zone21_Dead, Lower_Zone22_Water, Lower_Zone22_Dead, Lower_Zone23_Water, Lower_Zone23_Dead, " +
                "Lower_Zone24_Water, Lower_Zone24_Dead, Lower_Zone25_Water, Lower_Zone25_Dead, Lower_Zone26_Water, Lower_Zone26_Dead, Lower_Zone27_Water, " +
                "Lower_Zone27_Dead, Lower_Zone28_Water, Lower_Zone28_Dead, Lower_Zone29_Water, Lower_Zone29_Dead, Lower_Zone30_Water, Lower_Zone30_Dead) " +
                "Select '" + dieNum + "' as Die_Number_Ins, '" + alloy + "' as Alloy_Ins, '"
                + dateEntered + "' as Date_entered_Ins, '" + dateChanged + "' as Date_changed_Ins, '" + changedBy + "' as Changed_by_Ins, "
                + extSpeed + " as Ext_speed_Ins, " + extspeedGoal + " as Ext_speed_goal_Ins, " + billetTemp + " as Billet_temp_Ins, "
                + containerTemp + " as Container_temp_Ins, " + curves + " as Curves_Ins, " + fanPercent + " as Fan_percent_Ins, "
                + sawtoweldScrap + " as Saw_to_weld_scrap_Ins, " + weldtodieScrap + " as Weld_to_die_scrap_Ins, " + billetLength + " as Billet_length_Ins, "
                + BTP + " as BTP_Ins, " + upperairSpeed + " as Upper_Air_Speed_Ins, " + upperairdeadSpeed + " as Upper_Air_Dead_Speed_Ins, "
                + lowerairSpeed + " as Lower_Air_Speed_Ins, " + lowerairdeadSpeed + " as Lower_Air_Dead_Speed_Ins, "
                + waterpumpSpeed + " as Water_Pump_Speed_Ins, " + waterpumpdeadSpeed + " as Water_Pump_Dead_Speed_Ins, " + buttLength + " as Butt_length_Ins, "
                + rampstartPercent + " as Ramp_start_perc_Ins, " + rampTime + " as Ramp_time_Ins, '" + comments + "' as Comment_Ins, "
                + speedchangePos + " as Speed_change_pos_Ins, " + extspeed2 + " as Ext_speed_2_Ins, '" + var1 + "' as Upper_Zone_1_Water_Ins, '"
                + var2 + "' as Upper_Zone1_Dead_Ins, '" + var3 + "' as Upper_Zone2_Water_Ins, '" + var4 + "' as Upper_Zone2_Dead_Ins, '"
                + var5 + "' as Upper_Zone3_Water_Ins, '" + var6 + "' as Upper_Zone3_Dead_Ins, '" + var7 + "' as Upper_Zone4_Water_Ins, '"
                + var8 + "' as Upper_Zone4_Dead_Ins, '" + var9 + "' as Upper_Zone5_Water_Ins, '" + var10 + "' as Upper_Zone5_Dead_Ins, '"
                + var11 + "' as Upper_Zone6_Water_Ins, '" + var12 + "' as Upper_Zone6_Dead_Ins, '" + var13 + "' as Upper_Zone7_Water_Ins, '"
                + var14 + "' as Upper_Zone7_Dead_Ins, '" + var15 + "' as Upper_Zone8_Water_Ins, '" + var16 + "' as Upper_Zone8_Dead_Ins, '"
                + var17 + "' as Upper_Zone9_Water_Ins, '" + var18 + "' as Upper_Zone9_Dead_Ins, '" + var19 + "' as Upper_Zone10_Water_Ins, '"
                + var20 + "' as Upper_Zone10_Dead_Ins, '" + var21 + "' as Upper_Zone11_Water_Ins, '" + var22 + "' as Upper_Zone11_Dead_Ins, '"
                + var23 + "' as Upper_Zone12_Water_Ins, '" + var24 + "' as Upper_Zone12_Dead_Ins, '" + var25 + "' as Upper_Zone13_Water_Ins, '"
                + var26 + "' as Upper_Zone13_Dead_Ins, '" + var27 + "' as Upper_Zone14_Water_Ins, '" + var28 + "' as Upper_Zone14_Dead_Ins, '"
                + var29 + "' as Upper_Zone15_Water_Ins, '" + var30 + "' as Upper_Zone15_Dead_Ins, '" + var31 + "' as Upper_Zone16_Water_Ins, '"
                + var32 + "' as Upper_Zone16_Dead_Ins, '" + var33 + "' as Upper_Zone17_Water_Ins, '" + var34 + "' as Upper_Zone17_Dead_Ins, '"
                + var35 + "' as Upper_Zone18_Water_Ins, '" + var36 + "' as Upper_Zone18_Dead_Ins, '" + var37 + "' as Upper_Zone19_Water_Ins, '"
                + var38 + "' as Upper_Zone19_Dead_Ins, '" + var39 + "' as Upper_Zone20_Water_Ins, '" + var40 + "' as Upper_Zone20_Dead_Ins, '"
                + var41 + "' as Upper_Zone21_Water_Ins, '" + var42 + "' as Upper_Zone21_Dead_Ins, '" + var43 + "' as Upper_Zone22_Water_Ins, '"
                + var44 + "' as Upper_Zone22_Dead_Ins, '" + var45 + "' as Upper_Zone23_Water_Ins, '" + var46 + "' as Upper_Zone23_Dead_Ins, '"
                + var47 + "' as Upper_Zone24_Water_Ins, '" + var48 + "' as Upper_Zone24_Dead_Ins, '" + var49 + "' as Upper_Zone25_Water_Ins, '"
                + var50 + "' as Upper_Zone25_Dead_Ins, '" + var51 + "' as Upper_Zone26_Water_Ins, '" + var52 + "' as Upper_Zone26_Dead_Ins, '"
                + var53 + "' as Upper_Zone27_Water_Ins, '" + var54 + "' as Upper_Zone27_Dead_Ins, '" + var55 + "' as Upper_Zone28_Water_Ins, '"
                + var56 + "' as Upper_Zone28_Dead_Ins, '" + var57 + "' as Upper_Zone29_Water_Ins, '" + var58 + "' as Upper_Zone29_Dead_Ins, '"
                + var59 + "' as Upper_Zone30_Water_Ins, '" + var60 + "' as Upper_Zone30_Dead_Ins, '" + var61 + "' as Lower_Zone1_Water_Ins, '"
                + var62 + "' as Lower_Zone1_Dead_Ins, '" + var63 + "' as Lower_Zone2_Water_Ins, '" + var64 + "' as Lower_Zone2_Dead_Ins, '"
                + var65 + "' as Lower_Zone3_Water_Ins, '" + var66 + "' as Lower_Zone3_Dead_Ins, '" + var67 + "' as Lower_Zone4_Water_Ins, '"
                + var68 + "' as Lower_Zone4_Dead_Ins, '" + var69 + "' as Lower_Zone5_Water_Ins, '" + var70 + "' as Lower_Zone5_Dead_Ins, '"
                + var71 + "' as Lower_Zone6_Water_Ins, '" + var72 + "' as Lower_Zone6_Dead_Ins, '" + var73 + "' as Lower_Zone7_Water_Ins, '"
                + var74 + "' as Lower_Zone7_Dead_Ins, '" + var75 + "' as Lower_Zone8_Water_Ins, '" + var76 + "' as Lower_Zone8_Dead_Ins, '"
                + var77 + "' as Lower_Zone9_Water_Ins, '" + var78 + "' as Lower_Zone9_Dead_Ins, '" + var79 + "' as Lower_Zone10_Water_Ins, '"
                + var80 + "' as Lower_Zone10_Dead_Ins, '" + var81 + "' as Lower_Zone11_Water_Ins, '" + var82 + "' as Lower_Zone11_Dead_Ins, '"
                + var83 + "' as Lower_Zone12_Water_Ins, '" + var84 + "' as Lower_Zone12_Dead_Ins, '" + var85 + "' as Lower_Zone13_Water_Ins, '"
                + var86 + "' as Lower_Zone13_Dead_Ins, '" + var87 + "' as Lower_Zone14_Water_Ins, '" + var88 + "' as Lower_Zone14_Dead_Ins, '" 
                + var89 + "' as Lower_Zone15_Water_Ins, '" + var90 + "' as Lower_Zone15_Dead_Ins, '" + var91 + "' as Lower_Zone16_Water_Ins, '" 
                + var92 + "' as Lower_Zone16_Dead_Ins, '" + var93 + "' as Lower_Zone17_Water_Ins, '" + var94 + "' as Lower_Zone17_Dead_Ins, '" 
                + var95 + "' as Lower_Zone18_Water_Ins, '" + var96 + "' as Lower_Zone18_Dead_Ins, '" + var97 + "' as Lower_Zone19_Water_Ins, '" 
                + var98 + "' as Lower_Zone19_Dead_Ins, '" + var99 + "' as Lower_Zone20_Water_Ins, '" + var100 + "' as Lower_Zone20_Dead_Ins, '" 
                + var101 + "' as Lower_Zone21_Water_Ins, '" + var102 + "' as Lower_Zone21_Dead_Ins, '" + var103 + "' as Lower_Zone22_Water_Ins, '" 
                + var104 + "' as Lower_Zone22_Dead_Ins, '" + var105 + "' as Lower_Zone23_Water_Ins, '" + var106 + "' as Lower_Zone23_Dead_Ins, '" 
                + var107 + "' as Lower_Zone24_Water_Ins, '" + var108 + "' as Lower_Zone24_Dead_Ins, '" + var109 + "' as Lower_Zone25_Water_Ins, '" 
                + var110 + "' as Lower_Zone25_Dead_Ins, '" + var111 + "' as Lower_Zone26_Water_Ins, '" + var112 + "' as Lower_Zone26_Dead_Ins, '" 
                + var113 + "' as Lower_Zone27_Water_Ins, '" + var114 + "' as Lower_Zone27_Dead_Ins, '" + var115 + "' as Lower_Zone28_Water_Ins, '" 
                + var116 + "' as Lower_Zone28_Dead_Ins, '" + var117 + "' as Lower_Zone29_Water_Ins, '" + var118 + "' as Lower_Zone29_Dead_Ins, '"
                + var119 + "' as Lower_Zone30_Water_Ins, '" + var120 + "' as Lower_Zone30_Dead_Ins", conn); 
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable table = new DataTable();

                                 
            conn.Open();
            adapter.Fill(table);
            conn.Close();
            MessageBox.Show("You have successfully created the paramters for this die");

        }

        private void Button11_Click(object sender, EventArgs e)
        {

            string dieNum = "";
            string alloy = "";
            string dateEntered = "";
            string dateChanged = "";
            string changedBy = "";
            double extSpeed = 0;
            double billetTemp = 0;
            int billetLength = 0;
            double containerTemp = 0;
            double curves = 0;
            double fanPercent = 0;
            int sawtoweldScrap = 0;
            int weldtodieScrap = 0;
            int BTP = 0;
            int buttLength = 0;
            double extspeedGoal = 0;
            double extspeed2 = 0;
            int rampstartPercent = 0;
            int rampTime = 0;
            double speedchangePos = 0;
            string comments = "";
            int upperairSpeed = 0;
            int upperairdeadSpeed = 0;
            int lowerairSpeed = 0;
            int lowerairdeadSpeed = 0;
            int waterpumpSpeed = 0;
            int waterpumpdeadSpeed = 0;
            try
            {
                dieNum = textBox1.Text;
                alloy = comboBox1.SelectedItem.ToString();
                dateEntered = textBox3.Text;
                dateChanged = textBox14.Text;
                comments = textBox21.Text;
                changedBy = textBox15.Text; ;
                extSpeed = Convert.ToDouble(textBox4.Text);
                billetTemp = Convert.ToDouble(textBox5.Text);
                billetLength = Convert.ToInt32(textBox6.Text);
                containerTemp = Convert.ToDouble(textBox7.Text);
                curves = Convert.ToDouble(textBox8.Text);
                fanPercent = Convert.ToDouble(textBox9.Text);
                sawtoweldScrap = Convert.ToInt32(textBox10.Text);
                weldtodieScrap = Convert.ToInt32(textBox11.Text);
                BTP = Convert.ToInt32(textBox12.Text);
                extspeedGoal = Convert.ToDouble(textBox16.Text);
                buttLength = Convert.ToInt32(textBox13.Text);
                extspeed2 = Convert.ToDouble(textBox17.Text);
                rampstartPercent = Convert.ToInt32(textBox18.Text);
                rampTime = Convert.ToInt32(textBox19.Text);
                speedchangePos = Convert.ToDouble(textBox20.Text);
                upperairSpeed = Convert.ToInt32(textBox22.Text);
                upperairdeadSpeed = Convert.ToInt32(textBox23.Text);
                lowerairSpeed = Convert.ToInt32(textBox24.Text);
                lowerairdeadSpeed = Convert.ToInt32(textBox25.Text);
                waterpumpSpeed = Convert.ToInt32(textBox26.Text);
                waterpumpdeadSpeed = Convert.ToInt32(textBox27.Text);
            }
            catch (Exception ex)
            {
                //do somethin
            }
            bool var1 = checkBox1.Checked;
            bool var2 = checkBox2.Checked;
            bool var3 = checkBox3.Checked;
            bool var4 = checkBox4.Checked;
            bool var5 = checkBox5.Checked;
            bool var6 = checkBox6.Checked;
            bool var7 = checkBox7.Checked;
            bool var8 = checkBox8.Checked;
            bool var9 = checkBox9.Checked;
            bool var10 = checkBox10.Checked;
            bool var11 = checkBox11.Checked;
            bool var12 = checkBox12.Checked;
            bool var13 = checkBox13.Checked;
            bool var14 = checkBox14.Checked;
            bool var15 = checkBox15.Checked;
            bool var16 = checkBox16.Checked;
            bool var17 = checkBox17.Checked;
            bool var18 = checkBox18.Checked;
            bool var19 = checkBox19.Checked;
            bool var20 = checkBox20.Checked;
            bool var21 = checkBox21.Checked;
            bool var22 = checkBox22.Checked;
            bool var23 = checkBox23.Checked;
            bool var24 = checkBox24.Checked;
            bool var25 = checkBox25.Checked;
            bool var26 = checkBox26.Checked;
            bool var27 = checkBox27.Checked;
            bool var28 = checkBox28.Checked;
            bool var29 = checkBox29.Checked;
            bool var30 = checkBox30.Checked;
            bool var31 = checkBox31.Checked;
            bool var32 = checkBox32.Checked;
            bool var33 = checkBox33.Checked;
            bool var34 = checkBox34.Checked;
            bool var35 = checkBox35.Checked;
            bool var36 = checkBox36.Checked;
            bool var37 = checkBox37.Checked;
            bool var38 = checkBox38.Checked;
            bool var39 = checkBox39.Checked;
            bool var40 = checkBox40.Checked;
            bool var41 = checkBox41.Checked;
            bool var42 = checkBox42.Checked;
            bool var43 = checkBox43.Checked;
            bool var44 = checkBox44.Checked;
            bool var45 = checkBox45.Checked;
            bool var46 = checkBox46.Checked;
            bool var47 = checkBox47.Checked;
            bool var48 = checkBox48.Checked;
            bool var49 = checkBox49.Checked;
            bool var50 = checkBox50.Checked;
            bool var51 = checkBox51.Checked;
            bool var52 = checkBox52.Checked;
            bool var53 = checkBox53.Checked;
            bool var54 = checkBox54.Checked;
            bool var55 = checkBox55.Checked;
            bool var56 = checkBox56.Checked;
            bool var57 = checkBox57.Checked;
            bool var58 = checkBox58.Checked;
            bool var59 = checkBox59.Checked;
            bool var60 = checkBox60.Checked;
            bool var61 = checkBox61.Checked;
            bool var62 = checkBox62.Checked;
            bool var63 = checkBox63.Checked;
            bool var64 = checkBox64.Checked;
            bool var65 = checkBox65.Checked;
            bool var66 = checkBox66.Checked;
            bool var67 = checkBox67.Checked;
            bool var68 = checkBox68.Checked;
            bool var69 = checkBox69.Checked;
            bool var70 = checkBox70.Checked;
            bool var71 = checkBox71.Checked;
            bool var72 = checkBox72.Checked;
            bool var73 = checkBox73.Checked;
            bool var74 = checkBox74.Checked;
            bool var75 = checkBox75.Checked;
            bool var76 = checkBox76.Checked;
            bool var77 = checkBox77.Checked;
            bool var78 = checkBox78.Checked;
            bool var79 = checkBox79.Checked;
            bool var80 = checkBox80.Checked;
            bool var81 = checkBox81.Checked;
            bool var82 = checkBox82.Checked;
            bool var83 = checkBox83.Checked;
            bool var84 = checkBox84.Checked;
            bool var85 = checkBox85.Checked;
            bool var86 = checkBox86.Checked;
            bool var87 = checkBox87.Checked;
            bool var88 = checkBox88.Checked;
            bool var89 = checkBox89.Checked;
            bool var90 = checkBox90.Checked;
            bool var91 = checkBox91.Checked;
            bool var92 = checkBox92.Checked;
            bool var93 = checkBox93.Checked;
            bool var94 = checkBox94.Checked;
            bool var95 = checkBox95.Checked;
            bool var96 = checkBox96.Checked;
            bool var97 = checkBox97.Checked;
            bool var98 = checkBox98.Checked;
            bool var99 = checkBox99.Checked;
            bool var100 = checkBox100.Checked;
            bool var101 = checkBox101.Checked;
            bool var102 = checkBox102.Checked;
            bool var103 = checkBox103.Checked;
            bool var104 = checkBox104.Checked;
            bool var105 = checkBox105.Checked;
            bool var106 = checkBox106.Checked;
            bool var107 = checkBox107.Checked;
            bool var108 = checkBox108.Checked;
            bool var109 = checkBox109.Checked;
            bool var110 = checkBox110.Checked;
            bool var111 = checkBox111.Checked;
            bool var112 = checkBox112.Checked;
            bool var113 = checkBox113.Checked;
            bool var114 = checkBox114.Checked;
            bool var115 = checkBox115.Checked;
            bool var116 = checkBox116.Checked;
            bool var117 = checkBox117.Checked;
            bool var118 = checkBox118.Checked;
            bool var119 = checkBox119.Checked;
            bool var120 = checkBox120.Checked;

            string connString = System.Configuration.ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString.ToString();
            SqlConnection conn = new SqlConnection(connString);

            SqlCommand cmd = new SqlCommand("Update dbo.tblPress2_Die_Info_Alloy SET Die_Number=@dienum, Alloy=@alloy, Date_entered=@dateent, Date_changed=@datech, Changed_by=@chngby, Ext_speed=@extspeed, Ext_speed_goal=@extgoal, Billet_temp=@billtemp, " +
                "Container_temp=@conttemp, Curves=@curves, Fan_percent=@fp, Saw_to_weld_scrap=@sawweldscrap, Weld_to_die_scrap=@welddiescrap, Billet_length=@blength, BTP=@btp, Upper_Air_Speed=@uas, Upper_Air_Dead_Speed=@uads, Lower_Air_Speed=@las, " +
                "Lower_Air_Dead_Speed=@lads, Water_Pump_Speed=@wps, Water_Pump_Dead_Speed=@wpds, Butt_length=@bl, Ramp_start_perc=@rsp, Ramp_time=@rt, Comment=@com, Speed_change_pos=@scp, Ext_Speed_2=@extspeed2, " +
                "Upper_Zone1_Water=@uzw1, Upper_Zone1_Dead=@uzd1, Upper_Zone2_Water=@uzw2, Upper_Zone2_Dead=@uzd2, Upper_Zone3_Water=@uzw3, " + //begin uz
                "Upper_Zone3_Dead=@uzd3, Upper_Zone4_Water=@uzw4, Upper_Zone4_Dead=@uzd4, Upper_Zone5_Water=@uzw5, Upper_Zone5_Dead=@uzd5, Upper_Zone6_Water=@uzw6, Upper_Zone6_Dead=@uzd6, " +
                "Upper_Zone7_Water=@uzw7, Upper_Zone7_Dead=@uzd7, Upper_Zone8_Water=@uzw8, Upper_Zone8_Dead=@uzd8, Upper_Zone9_Water=@uzw9, Upper_Zone9_Dead=@uzd9, Upper_Zone10_Water=@uzw10, " +
                "Upper_Zone10_Dead=@uzd10, Upper_Zone11_Water=@uzw11, Upper_Zone11_Dead=@uzd11, Upper_Zone12_Water=@uzw12, Upper_Zone12_Dead=@uzd12, Upper_Zone13_Water=@uzw13, Upper_Zone13_Dead=@uzd13, " +
                "Upper_Zone14_Water=@uzw13, Upper_Zone14_Dead=@uzd14, Upper_Zone15_Water=@uzw15, Upper_Zone15_Dead=@uzd15, Upper_Zone16_Water=@uzw16, Upper_Zone16_Dead=@uzd16, Upper_Zone17_Water=@uzw17, " +
                "Upper_Zone17_Dead=@uzd17, Upper_Zone18_Water=@uzw18, Upper_Zone18_Dead=@uzd18, Upper_Zone19_Water=@uzw19, Upper_Zone19_Dead=@uzd19, Upper_Zone20_Water=@uzw20, Upper_Zone20_Dead=@uzd20, " +
                "Upper_Zone21_Water=@uzw21, Upper_Zone21_Dead=@uzd21, Upper_Zone22_Water=@uzw22, Upper_Zone22_Dead=@uzd22, Upper_Zone23_Water=@uzw23, Upper_Zone23_Dead=@uzd23, Upper_Zone24_Water=@uzw24, " +
                "Upper_Zone24_Dead=@uzd24, Upper_Zone25_Water=@uzw25, Upper_Zone25_Dead=@uzd25, Upper_Zone26_Water=@uzw26, Upper_Zone26_Dead=@uzd26, Upper_Zone27_Water=@uzw27, Upper_Zone27_Dead=@uzd27, " +
                "Upper_Zone28_Water=@uzw28, Upper_Zone28_Dead=@uzd28, Upper_Zone29_Water=@uzw29, Upper_Zone29_Dead=@uzd29, Upper_Zone30_Water=@uzw30, Upper_Zone30_Dead=@uzd30, " +
                "Lower_Zone1_Water=@lzw1, Lower_Zone1_Dead=@lzd1, Lower_Zone2_Water=@lzw2, Lower_Zone2_Dead=@lzd2, " + //begin lz
                "Lower_Zone3_Water=@lzw3, Lower_Zone3_Dead=@lzd3, Lower_Zone4_Water=@lzw4, Lower_Zone4_Dead=@lzd4, Lower_Zone5_Water=@lzw5, Lower_Zone5_Dead=@lzd5, Lower_Zone6_Water=@lzw6, " +
                "Lower_Zone6_Dead=@lzd6, Lower_Zone7_Water=@lzw7, Lower_Zone7_Dead=@lzd7, Lower_Zone8_Water=@lzw8, Lower_Zone8_Dead=@lzd8, Lower_Zone9_Water=@lzw9, Lower_Zone9_Dead=@lzd9, " +
                "Lower_Zone10_Water=@lzw10, Lower_Zone10_Dead=@lzd10, Lower_Zone11_Water=@lzw11, Lower_Zone11_Dead=@lzd11, Lower_Zone12_Water=@lzw12, Lower_Zone12_Dead=@lzd12, Lower_Zone13_Water=@lzw13, " +
                "Lower_Zone13_Dead=@lzd13, Lower_Zone14_Water=@lzw14, Lower_Zone14_Dead=@lzd14, Lower_Zone15_Water=@lzw15, Lower_Zone15_Dead=@lzd15, Lower_Zone16_Water=@lzw16, Lower_Zone16_Dead=@lzd16, " +
                "Lower_Zone17_Water=@lzw17, Lower_Zone17_Dead=@lzd17, Lower_Zone18_Water=@lzw18, Lower_Zone18_Dead=@lzd18, Lower_Zone19_Water=@lzw19, Lower_Zone19_Dead=@lzd19, Lower_Zone20_Water=@lzw20, " +
                "Lower_Zone20_Dead=@lzd20, Lower_Zone21_Water=@lzw21, Lower_Zone21_Dead=@lzd21, Lower_Zone22_Water=@lzw22, Lower_Zone22_Dead=@lzd22, Lower_Zone23_Water=@lzw23, Lower_Zone23_Dead=@lzd23, " +
                "Lower_Zone24_Water=@lzw24, Lower_Zone24_Dead=@lzd24, Lower_Zone25_Water=@lzw25, Lower_Zone25_Dead=@lzd25, Lower_Zone26_Water=@lzw26, Lower_Zone26_Dead=@lzd26, Lower_Zone27_Water=@lzw27, " +
                "Lower_Zone27_Dead=@lzd27, Lower_Zone28_Water=@lzw28, Lower_Zone28_Dead=@lzd28, Lower_Zone29_Water=@lzw29, Lower_Zone29_Dead=@lzd29, Lower_Zone30_Water=@lzw30, Lower_Zone30_Dead=@lzd30 " +
                "WHERE Die_Number = '" + dieNum + "'", conn);

                 cmd.Parameters.AddWithValue("@dienum", dieNum);
                 cmd.Parameters.AddWithValue("@alloy", alloy);
                 cmd.Parameters.AddWithValue("@dateent", dateEntered);
                 cmd.Parameters.AddWithValue("@datech", dateChanged);
                 cmd.Parameters.AddWithValue("@chngby", changedBy);
                 cmd.Parameters.AddWithValue("@extspeed", extSpeed);
                 cmd.Parameters.AddWithValue("@extgoal", extspeedGoal);
                 cmd.Parameters.AddWithValue("@billtemp", billetTemp);
                 cmd.Parameters.AddWithValue("@conttemp", containerTemp);
                 cmd.Parameters.AddWithValue("@curves", curves);
                 cmd.Parameters.AddWithValue("@fp", fanPercent);
                 cmd.Parameters.AddWithValue("@sawweldscrap", sawtoweldScrap);
                 cmd.Parameters.AddWithValue("@welddiescrap", weldtodieScrap);
                 cmd.Parameters.AddWithValue("@blength", billetLength);
                 cmd.Parameters.AddWithValue("@btp", BTP);
                 cmd.Parameters.AddWithValue("@uas", upperairSpeed);
                 cmd.Parameters.AddWithValue("@uads", upperairdeadSpeed);
                 cmd.Parameters.AddWithValue("@las", lowerairSpeed);
                 cmd.Parameters.AddWithValue("@lads", lowerairdeadSpeed);
                 cmd.Parameters.AddWithValue("@wps", waterpumpSpeed);
                 cmd.Parameters.AddWithValue("@wpds", waterpumpdeadSpeed);
                 cmd.Parameters.AddWithValue("@bl", billetLength);
                 cmd.Parameters.AddWithValue("@rsp", rampstartPercent);
                 cmd.Parameters.AddWithValue("@rt", rampTime);
                 cmd.Parameters.AddWithValue("@com", comments);
                 cmd.Parameters.AddWithValue("@scp", speedchangePos);
                 cmd.Parameters.AddWithValue("@extspeed2", extspeed2);
                 cmd.Parameters.AddWithValue("@uzw1", var1);
                 cmd.Parameters.AddWithValue("@uzd1", var2);
                 cmd.Parameters.AddWithValue("@uzw2", var3);
                 cmd.Parameters.AddWithValue("@uzd2", var4);
                 cmd.Parameters.AddWithValue("@uzw3", var5);
                 cmd.Parameters.AddWithValue("@uzd3", var6);
                 cmd.Parameters.AddWithValue("@uzw4", var7);
                 cmd.Parameters.AddWithValue("@uzd4", var8);
                 cmd.Parameters.AddWithValue("@uzw5", var9);
                 cmd.Parameters.AddWithValue("@uzd5", var10);
                 cmd.Parameters.AddWithValue("@uzw6", var11);
                 cmd.Parameters.AddWithValue("@uzd6", var12);
                 cmd.Parameters.AddWithValue("@uzw7", var13);
                 cmd.Parameters.AddWithValue("@uzd7", var14);
                 cmd.Parameters.AddWithValue("@uzw8", var15);
                 cmd.Parameters.AddWithValue("@uzd8", var16);
                 cmd.Parameters.AddWithValue("@uzw9", var17);
                 cmd.Parameters.AddWithValue("@uzd9", var18);
                 cmd.Parameters.AddWithValue("@uzw10", var19);
                 cmd.Parameters.AddWithValue("@uzd10", var20);
                 cmd.Parameters.AddWithValue("@uzw11", var21);
                 cmd.Parameters.AddWithValue("@uzd11", var22);
                 cmd.Parameters.AddWithValue("@uzw12", var23);
                 cmd.Parameters.AddWithValue("@uzd12", var24);
                 cmd.Parameters.AddWithValue("@uzw13", var25);
                 cmd.Parameters.AddWithValue("@uzd13", var26);
                 cmd.Parameters.AddWithValue("@uzw14", var27);
                 cmd.Parameters.AddWithValue("@uzd14", var28);
                 cmd.Parameters.AddWithValue("@uzw15", var29);
                 cmd.Parameters.AddWithValue("@uzd15", var30);
                 cmd.Parameters.AddWithValue("@uzw16", var31);
                 cmd.Parameters.AddWithValue("@uzd16", var32);
                 cmd.Parameters.AddWithValue("@uzw17", var33);
                 cmd.Parameters.AddWithValue("@uzd17", var34);
                 cmd.Parameters.AddWithValue("@uzw18", var35);
                 cmd.Parameters.AddWithValue("@uzd18", var36);
                 cmd.Parameters.AddWithValue("@uzw19", var37);
                 cmd.Parameters.AddWithValue("@uzd19", var38);
                 cmd.Parameters.AddWithValue("@uzw20", var39);
                 cmd.Parameters.AddWithValue("@uzd20", var40);
                 cmd.Parameters.AddWithValue("@uzw21", var41);
                 cmd.Parameters.AddWithValue("@uzd21", var42);
                 cmd.Parameters.AddWithValue("@uzw22", var43);
                 cmd.Parameters.AddWithValue("@uzd22", var44);
                 cmd.Parameters.AddWithValue("@uzw23", var45);
                 cmd.Parameters.AddWithValue("@uzd23", var46);
                 cmd.Parameters.AddWithValue("@uzw24", var47);
                 cmd.Parameters.AddWithValue("@uzd24", var48);
                 cmd.Parameters.AddWithValue("@uzw25", var49);
                 cmd.Parameters.AddWithValue("@uzd25", var50);
                 cmd.Parameters.AddWithValue("@uzw26", var51);
                 cmd.Parameters.AddWithValue("@uzd26", var52);
                 cmd.Parameters.AddWithValue("@uzw27", var53);
                 cmd.Parameters.AddWithValue("@uzd27", var54);
                 cmd.Parameters.AddWithValue("@uzw28", var55);
                 cmd.Parameters.AddWithValue("@uzd28", var56);
                 cmd.Parameters.AddWithValue("@uzw29", var57);
                 cmd.Parameters.AddWithValue("@uzd29", var58);
                 cmd.Parameters.AddWithValue("@uzw30", var59);
                 cmd.Parameters.AddWithValue("@uzd30", var60);
                 cmd.Parameters.AddWithValue("@lzw1", var61);
                 cmd.Parameters.AddWithValue("@lzd1", var62);
                 cmd.Parameters.AddWithValue("@lzw2", var63);
                 cmd.Parameters.AddWithValue("@lzd2", var64);
                 cmd.Parameters.AddWithValue("@lzw3", var65);
                 cmd.Parameters.AddWithValue("@lzd3", var66);
                 cmd.Parameters.AddWithValue("@lzw4", var67);
                 cmd.Parameters.AddWithValue("@lzd4", var68);
                 cmd.Parameters.AddWithValue("@lzw5", var69);
                 cmd.Parameters.AddWithValue("@lzd5", var70);
                 cmd.Parameters.AddWithValue("@lzw6", var71);
                 cmd.Parameters.AddWithValue("@lzd6", var72);
                 cmd.Parameters.AddWithValue("@lzw7", var73);
                 cmd.Parameters.AddWithValue("@lzd7", var74);
                 cmd.Parameters.AddWithValue("@lzw8", var75);
                 cmd.Parameters.AddWithValue("@lzd8", var76);
                 cmd.Parameters.AddWithValue("@lzw9", var77);
                 cmd.Parameters.AddWithValue("@lzd9", var78);
                 cmd.Parameters.AddWithValue("@lzw10", var79);
                 cmd.Parameters.AddWithValue("@lzd10", var80);
                 cmd.Parameters.AddWithValue("@lzw11", var81);
                 cmd.Parameters.AddWithValue("@lzd11", var82);
                 cmd.Parameters.AddWithValue("@lzw12", var83);
                 cmd.Parameters.AddWithValue("@lzd12", var84);
                 cmd.Parameters.AddWithValue("@lzw13", var85);
                 cmd.Parameters.AddWithValue("@lzd13", var86);
                 cmd.Parameters.AddWithValue("@lzw14", var87);
                 cmd.Parameters.AddWithValue("@lzd14", var88);
                 cmd.Parameters.AddWithValue("@lzw15", var89);
                 cmd.Parameters.AddWithValue("@lzd15", var90);
                 cmd.Parameters.AddWithValue("@lzw16", var91);
                 cmd.Parameters.AddWithValue("@lzd16", var92);
                 cmd.Parameters.AddWithValue("@lzw17", var93);
                 cmd.Parameters.AddWithValue("@lzd17", var94);
                 cmd.Parameters.AddWithValue("@lzw18", var95);
                 cmd.Parameters.AddWithValue("@lzd18", var96);
                 cmd.Parameters.AddWithValue("@lzw19", var97);
                 cmd.Parameters.AddWithValue("@lzd19", var98);
                 cmd.Parameters.AddWithValue("@lzw20", var99);
                 cmd.Parameters.AddWithValue("@lzd20", var100);
                 cmd.Parameters.AddWithValue("@lzw21", var101);
                 cmd.Parameters.AddWithValue("@lzd21", var102);
                 cmd.Parameters.AddWithValue("@lzw22", var103);
                 cmd.Parameters.AddWithValue("@lzd22", var104);
                 cmd.Parameters.AddWithValue("@lzw23", var105);
                 cmd.Parameters.AddWithValue("@lzd23", var106);
                 cmd.Parameters.AddWithValue("@lzw24", var107);
                 cmd.Parameters.AddWithValue("@lzd24", var108);
                 cmd.Parameters.AddWithValue("@lzw25", var109);
                 cmd.Parameters.AddWithValue("@lzd25", var110);
                 cmd.Parameters.AddWithValue("@lzw26", var111);
                 cmd.Parameters.AddWithValue("@lzd26", var112);
                 cmd.Parameters.AddWithValue("@lzw27", var113);
                 cmd.Parameters.AddWithValue("@lzd27", var114);
                 cmd.Parameters.AddWithValue("@lzw28", var115);
                 cmd.Parameters.AddWithValue("@lzd28", var116);
                 cmd.Parameters.AddWithValue("@lzw29", var117);
                 cmd.Parameters.AddWithValue("@lzd29", var118);
                 cmd.Parameters.AddWithValue("@lzw30", var119);
                 cmd.Parameters.AddWithValue("@lzd30", var120);

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable table = new DataTable();

            conn.Open();
            adapter.Fill(table);
            conn.Close();
            MessageBox.Show("You have successfully updated the paramters for this die");

        }
       
        private void Button12_Click(object sender, EventArgs e)
        {
            groupBox1.Show();
            groupBox2.Show();
            groupBox3.Show();
        }

        private void Button13_Click(object sender, EventArgs e)
        {
            Form3 frm3 = new Form3();
            frm3.ShowDialog();

        }

        private void Button14_Click(object sender, EventArgs e)
        {
            ExtDieInspection frm2 = new ExtDieInspection();
            frm2.ShowDialog();

        }

        private void Button15_Click(object sender, EventArgs e)
        {
            ProdDieInspection frm4 = new ProdDieInspection();
            frm4.ShowDialog();

        }

        private void Button16_Click(object sender, EventArgs e)
        {
            DieRingInspection frm5 = new DieRingInspection();
            frm5.ShowDialog();
        }

        private void Button17_Click(object sender, EventArgs e)
        {
            DieStatus frm7 = new DieStatus();
            frm7.ShowDialog();
        }

        private void Button18_Click(object sender, EventArgs e)
        {
            Dispatch frm8 = new Dispatch();
            frm8.ShowDialog();

        }
    }
}
