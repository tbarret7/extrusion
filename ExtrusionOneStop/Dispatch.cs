﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data.SqlTypes;
using Oracle.ManagedDataAccess.Client;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Security.Principal;
using System.Security.Authentication;
using System.Drawing.Printing;
using System.IO;
using System.Globalization;

namespace ExtrusionOneStop
{
    public partial class Dispatch : Form
    {


        public Dispatch()
        {
            InitializeComponent();
        }

        DataTable table = new DataTable();
        DataTable tblFilter = new DataTable();
        DataTable calcs = new DataTable();

        public static int GetIso8601WeekOfYear(DateTime time)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        private static DataTable GetData(string sqlCommand)
        {
            string varNum;
            string connectionString = @"Data Source = 160.106.20.239,1433\sqlexpress; Initial Catalog = GEI_Extrusion; Integrated Security = True";

            SqlConnection conn = new SqlConnection(connectionString);

            SqlCommand command = new SqlCommand(sqlCommand, conn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;

            DataTable table = new DataTable();
            table.Locale = System.Globalization.CultureInfo.InvariantCulture;

            adapter.Fill(table);

            return table;
        }


        private void Form8_Load(object sender, EventArgs e)
        {
            DataAccess da = new DataAccess();
            da.OracleConnection();


            string MainQry = "Select WO.CCN as CCN, WO.MAS_LOC as LOC, WO.WO_NUM as NUM, WO.WO_LINE as LINE, WO.START_DATE as STARTDATE, WO.DUE_DATE as DUEDATE, WO.PRIORITY as PRIORITY, WO.ITEM as ITEM, WO.REVISION as REV, "
                + "WO.STATUS as STATUS, WO_RTG.OPERATION as OPER, WO_RTG.OPER_TYPE as OPERTYPE, WO_RTG.START_DATE As OPERATION_START_DATE, WO_RTG.END_DATE As OPERATION_END_DATE, "
                + "WO_RTG.FUNCTION_ as FUNCTION, WO_RTG.WC as RTG, WC.DESCRIPTION as DESCRIPTION, WC.DEPARTMENT as DEPT, WO.ORD_QTY as ORDQTY, WO_RTG.COMP_QTY as COMPQTY, " +
                "(WO.ORD_QTY - WO_RTG.COMP_QTY) as OPENQTY, WO_RTG.LAB_RUN as LABRUN,  WC.USER_ALPHA1 As WC_Category, " +
                "TO_CHAR((CASE WHEN WO_RTG.LAB_RUN = 0 Then 0 Else (WO.ORD_QTY - WO_RTG.COMP_QTY) / WO_RTG.LAB_RUN END), '9990.99') as REMHRS, "
                + "ITEM_CCN.USER2 As DIE, ITEM.WEIGHT as WEIGHT, WO_BOM.ITEM As ALLOY, WO_BHDR.BOM_DESCRIPTION As CUSTOMER, WO_RTG.WC As WC, ITEM.HAZARD As TEMPER, ITEM.LENGTH as L, ITEM.USER1 As WT_FT "
                + "From "
                + "WO Inner Join WO_RTG On WO_RTG.CCN = WO.CCN And WO_RTG.MAS_LOC = WO.MAS_LOC And WO_RTG.WO_NUM = WO.WO_NUM And WO_RTG.WO_LINE = WO.WO_LINE Inner Join "
                + "WC On WC.CCN = WO_RTG.CCN And WC.WC = WO_RTG.WC "
                + "LEFT JOIN WO_BOM ON WO_RTG.CCN = WO_BOM.CCN And WO_RTG.MAS_LOC = WO_BOM.MAS_LOC And WO_RTG.WO_NUM = WO_BOM.WO_NUM And "
                + "WO_RTG.WO_LINE = WO_BOM.WO_LINE "
                + "Inner Join ITEM_CCN On ITEM_CCN.CCN = WO.CCN And ITEM_CCN.ITEM = WO.ITEM And ITEM_CCN.REVISION = WO.REVISION Inner Join "
                + "WO_BHDR On WO_BHDR.CCN = WO.CCN And WO_BHDR.MAS_LOC = WO.MAS_LOC And WO_BHDR.WO_NUM = WO.WO_NUM And WO_BHDR.WO_LINE = WO.WO_LINE Inner Join "
                + "ITEM On ITEM.ITEM = ITEM_CCN.ITEM And ITEM.REVISION = ITEM_CCN.REVISION "
                + "Where (WO.ORD_QTY - WO_RTG.COMP_QTY > 0) And (WO.CCN = '7' And (WO.STATUS = 'I' OR WO.STATUS = 'R' OR WO.STATUS = 'U') And "
                + "WO_RTG.WC = '2002' And WO.MFG_CLOSE_DATE Is Null) And WO_BOM.ITEM Like 'A7%'";

            string dgv4Qry = "Select " +
                    "WO_BOM.ITEM As ALLOY, " +
                    "To_Char(Sum(Case" +
                        " When WO_RTG.LAB_RUN = 0 " +
                        "Then 0" +
                        "Else(WO.ORD_QTY - WO_RTG.COMP_QTY) / WO_RTG.LAB_RUN " +
                      "End), '9990.99') As REMHRS " +
                  "From " +
                        "WO Inner Join " +
                        "WO_RTG On WO_RTG.CCN = WO.CCN " +
                        "And WO_RTG.MAS_LOC = WO.MAS_LOC " +
                        "And WO_RTG.WO_NUM = WO.WO_NUM " +
                        "And WO_RTG.WO_LINE = WO.WO_LINE Inner Join " +
                         "WO_BOM On WO_BOM.CCN = WO.CCN " +
                         "And WO_BOM.MAS_LOC = WO.MAS_LOC " +
                         "And WO_BOM.WO_NUM = WO.WO_NUM " +
                         "And WO_BOM.WO_LINE = WO.WO_LINE " +
                 "Where " +
                        "WO.ORD_QTY - WO_RTG.COMP_QTY > 0 And " +
                        "WO.CCN = '7' And " +
                        "(WO.STATUS = 'I' Or " +
                        "WO.STATUS = 'R' Or " +
                        "WO.STATUS = 'U') And " +
                        "WO_RTG.WC = '2002' And " +
                        "WO.MFG_CLOSE_DATE Is Null And " +
                        "WO_BOM.ITEM Like 'A7%' " +
                "Group By " +
                        "WO_BOM.ITEM " +
                "Order By " +
                        "WO_BOM.ITEM";

            try
            {
                //dgv4
                DataTable table4 = new DataTable();
                OracleDataAdapter adapter4 = da.ODataAdapter(dgv4Qry);
                table4.Columns.Add("ALLOY");
                table4.Columns.Add("REMHRS");

                adapter4.Fill(table4);

                dataGridView4.DataSource = table4;
                //End dgv4 fill

                OracleDataReader reader = da.ODataReader(MainQry);
                OracleDataAdapter adapter = da.ODataAdapter(MainQry);
                calcs.Columns.Add("REMHRS");
                calcs.Columns.Add("WEEKDUE");

                adapter.Fill(table);

                da.OracleCloseConnection();

                dataGridView1.DataSource = table;
                table.Columns.Add("WEEKNUM");

            }
            catch (Exception x)
            {
                MessageBox.Show(x.ToString());
            }
            

            try
            {
                //Loop to convert due date row into week numbers
                foreach (DataRow row in table.Rows)
                {
                    var fill = (DateTime)row["DUEDATE"];
                    var conv = (GetIso8601WeekOfYear(fill));
                    row["WEEKNUM"] = conv;

                }

                //LINQ to sum column REMHRS for dgv3
                var result = table.AsEnumerable().OrderBy(o => int.Parse(o.Field<string>("WeekNum")))
                            .GroupBy(WEEKNUM => WEEKNUM.Field<string>("WeekNum"))
                            .Select(n => new
                            {
                                WEEKNUM = n.Key,
                                REMHRS = n.Sum(s => (float.Parse(s.Field<string>("REMHRS"))))
                            });
                //LINQ to sum column REMHRS for dgv4


                //LINQ to sum column REMHRS for dgv5
                var results = table.AsEnumerable().OrderBy(o => o.Field<string>("DIE"))
                              .GroupBy(g => g.Field<string>("DIE"))
                              .Select(n => new
                              {
                                  DIE = n.Key,
                                  REMHRS = n.Sum(s => float.Parse(s.Field<string>("REMHRS")))
                              });
                //Fill tables with datasets
                dataGridView3.DataSource = result.ToList();
                dataGridView5.DataSource = results.ToList();
                tblFilter = ((DataTable)dataGridView1.DataSource).Clone();
                dataGridView2.DataSource = tblFilter;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }    

        private void Button10_Click(object sender, EventArgs e)
        {
            //exclude
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                tblFilter.ImportRow(((DataTable)dataGridView1.DataSource).Rows[row.Index]);
                table.Rows.RemoveAt(row.Index);
            }
            tblFilter.AcceptChanges();
            table.AcceptChanges();
            dataGridView1.Refresh();
            try
            {

                foreach (DataRow row in table.Rows)
                {
                    var fill = (DateTime)row["DUEDATE"];
                    var conv = (GetIso8601WeekOfYear(fill));
                    row["WEEKNUM"] = conv;

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            try
            {
                //LINQ to sum column REMHRS
                var result = table.AsEnumerable().OrderBy(o => int.Parse(o.Field<string>("WeekNum")))
                            .GroupBy(WEEKNUM => WEEKNUM.Field<string>("WeekNum"))
                            .Select(n => new
                            {
                                WEEKNUM = n.Key,
                                REMHRS = n.Sum(s => (float.Parse(s.Field<string>("REMHRS"))))
                            });

                dataGridView3.DataSource = result.ToList();


                var results = table.AsEnumerable().OrderBy(o => o.Field<string>("DIE"))
                              .GroupBy(g => g.Field<string>("DIE"))
                              .Select(n => new
                              {
                                  DIE = n.Key,
                                  REMHRS = n.Sum(s => float.Parse(s.Field<string>("REMHRS")))
                              });

                dataGridView5.DataSource = results.ToList();


            }
            catch (Exception x)
            {
                MessageBox.Show(x.ToString());
            }
        }

        private void Button11_Click(object sender, EventArgs e)
        {
            //include
            try
            {
                foreach (DataGridViewRow row in dataGridView2.SelectedRows)
                {

                    table.ImportRow(((DataTable)dataGridView2.DataSource).Rows[row.Index]);
                    tblFilter.Rows.RemoveAt(row.Index);

                }
                table.AcceptChanges();
                dataGridView1.DataSource = table;

                //LINQ to sum column REMHRS
                var result = table.AsEnumerable().OrderBy(o => int.Parse(o.Field<string>("WeekNum")))
                            .GroupBy(WEEKNUM => WEEKNUM.Field<string>("WeekNum"))
                            .Select(n => new
                            {
                                WEEKNUM = n.Key,
                                REMHRS = n.Sum(s => (float.Parse(s.Field<string>("REMHRS"))))
                            });

                dataGridView3.DataSource = result.ToList();


                var results = table.AsEnumerable().OrderBy(o => o.Field<string>("DIE"))
                              .GroupBy(g => g.Field<string>("DIE"))
                              .Select(n => new
                              {
                                  DIE = n.Key,
                                  REMHRS = n.Sum(s => float.Parse(s.Field<string>("REMHRS")))
                              });

                dataGridView5.DataSource = results.ToList();


            }
            catch (Exception x)
            {
                MessageBox.Show(x.ToString());
            }
        }

        private void Button12_Click(object sender, EventArgs e)
        {       //Release button
            BuggyTagR frm = new BuggyTagR();
            frm.ShowDialog();

        }
              

        private void Button13_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataRow row in table.Rows)
                {

                    for (int i = 0; i > dataGridView1.Rows.Count - 1; i++)
                    {
                        MessageBox.Show(row["NUM"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            //reload tables
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            //BuggyTag     
            DataTable dt = ((DataTable)dataGridView1.DataSource).Clone();
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                dt.ImportRow(((DataTable)dataGridView1.DataSource).Rows[row.Index]);
                
            }           
  
            dt.AcceptChanges();

            BuggyTag frm = new BuggyTag(dt);
            frm.ShowDialog();

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            DieShopDispatchU frm = new DieShopDispatchU();
            frm.ShowDialog();

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            DieShopDispatchR frm = new DieShopDispatchR();
            frm.ShowDialog();

        }
    }
}
