﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;
using System.Data.SqlClient;
using Microsoft.Reporting.WinForms;

namespace ExtrusionOneStop
{
    public partial class DieShopDispatchU : Form
    {
        public DieShopDispatchU()
        {
            InitializeComponent();
        }

        private void DieShopDispatchU_Load(object sender, EventArgs e)
        {

            DataAccess da = new DataAccess();
            GEI_ExtrusionDataSet ds = new GEI_ExtrusionDataSet();
            DataTable table = new DataTable();

            string qry =                
                "SELECT " +
                    "WO.CCN As CCN, " +
                    "WO.STATUS As STATUS, " +
                    "WO_RTG.WC As WC, " +
                    "WO.OPEN_DATE As ENDDATE, " +
                    "WO.START_DATE As STARTDATE, " +
                    "WO.WO_NUM As TWO, " +
                    "WO.WO_LINE As Line, " +
                    "WO.DUE_DATE As WO_DUE, " +
                    "WO_RTG.START_DATE AS OPERATION_START_DATE, " +
                    "WO_RTG.END_DATE As OPERATION_END_DATE, " +
                    "WO.ITEM As ITEM, " +
                    "WO.REVISION As REV, " +
                    "ITEM_CCN.USER2 AS DIE, " +
                    "WO.ORD_QTY As ORDQTY, " +
                    "ITEM.WEIGHT As WEIGHT, " +
                    "((WO.ORD_QTY) * (ITEM.WEIGHT)) As LBS, " +
                    "ITEM.USER1 As WT_FT, " +
                    "qryDie_Shop_Filter.PRTWO, " +
                    "qryDie_Shop_Filter.PRTLN, " +
                    "qryDie_Shop_Filter.DUE_DATE " +
                "FROM " +
                    "WO INNER JOIN WO_RTG ON WO.WO_LINE = WO_RTG.WO_LINE AND " +
                    "WO.WO_NUM = WO_RTG.WO_NUM AND WO.MAS_LOC = WO_RTG.MAS_LOC AND WO.CCN = WO_RTG.CCN INNER JOIN ITEM_CCN INNER JOIN ITEM ON " +
                    "ITEM_CCN.REVISION = ITEM.REVISION AND ITEM_CCN.ITEM = ITEM.ITEM ON WO.REVISION = ITEM_CCN.REVISION AND " +
                    "WO.ITEM = ITEM_CCN.ITEM AND WO.CCN = ITEM_CCN.CCN " +
                    "LEFT OUTER JOIN " +
                        "(Select " +
                            "WOWOB.CCN, " +
                            "WOWOB.MAS_LOC, " +
                            "WOWOB.WO_NUM, " +
                            "WOWOB.WO_LINE, " +
                            "WOWOB.PARENT_WO_NUM AS PRTWO, " +
                            "WOWOB.PARENT_WO_LINE AS PRTLN, " +
                            "WO.DUE_DATE AS DUE_DATE " +
                        "FROM " +
                                "WO Inner Join " +
                                "WOWOB On WOWOB.CCN = WO.CCN " +
                                "And WOWOB.MAS_LOC = WO.MAS_LOC " +
                                "And WOWOB.WO_NUM = WO.WO_NUM " +
                                "And WOWOB.WO_LINE = WO.WO_LINE " +
                        "WHERE " +
                                "WO.CCN = '7') qryDie_Shop_Filter ON WO.CCN = qryDie_Shop_Filter.CCN And WO.MAS_LOC = qryDie_Shop_Filter.MAS_LOC And " +
                                "WO.WO_NUM = qryDie_Shop_Filter.WO_NUM And WO.WO_LINE = qryDie_Shop_Filter.WO_LINE " +
                "WHERE " +
                    "WO.CCN = '7' AND WO.STATUS = 'U' AND WO_RTG.WC = '2001' " +
                "ORDER BY " +
                    "WO.START_DATE";
                
            
            try
            {
                da.OracleConnection();
                da.ODataAdapter(qry).Fill(table);
                da.ODataAdapter(qry).Fill(ds, ds.Tables[3].TableName);

                da.OracleCloseConnection();

                ReportDataSource src = new ReportDataSource("DieShopDispatchU", ds.Tables[3]);
                this.reportViewer1.LocalReport.DataSources.Clear();
                this.reportViewer1.LocalReport.DataSources.Add(src);

                this.reportViewer1.Refresh();
                dataGridView1.DataSource = table;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            this.reportViewer1.RefreshReport();
        }
    }
}
