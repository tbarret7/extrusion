﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ExtrusionOneStop
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private static DataTable GetData(string sqlCommand, string num)
        {
            string geiDieNum = num;

            string connectionString = @"Data Source = 160.106.20.239,1433\sqlexpress; Initial Catalog = GEI_Extrusion; Integrated Security = True";

            SqlConnection conn = new SqlConnection(connectionString);

            SqlCommand command = new SqlCommand(sqlCommand, conn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;

            DataTable table = new DataTable();
            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
            command.Parameters.Add("@geiDieNum", geiDieNum);

            adapter.Fill(table);

            return table;
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            
            
        }

        

        private void FillBy1ToolStripButton_Click(object sender, EventArgs e)
        {
            string geiDieNum = geiDieNumToolStripTextBox.Text;
            
            try
            {
                tbl3DielogDataGridView.DataSource = GetData("SELECT ID, G_Number, Die_Number, PO_Number, Req_Number, Customer, Vendor, Size, " +
                "Holes, BKR, BOL, FP, Temp, Date_ordered, Due, Alloy, Received, Die_price, " +
                "Expedite FROM tbl3Dielog WHERE(Die_Number = @geiDieNum)", geiDieNum);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void ToolStripButton1_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.tbl3DielogBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.gEI_ExtrusionDataSet1);

        }

        private void OpenOrdersrev3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            DieRecipe frm1 = new DieRecipe();
            frm1.ShowDialog();
            
        }

        private void Tbl3DielogDataGridView_MouseDown(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    {
                        contextMenuStrip1.Show(this, new Point(e.X, e.Y));
                    }
                    break;

            }
        }

        private void CopyDieToNewRecordToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void Tbl3DielogDataGridView_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                try
                {
                    tbl3DielogDataGridView.CurrentCell = tbl3DielogDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex];
                    tbl3DielogDataGridView.Rows[e.RowIndex].Selected = true;
                    tbl3DielogDataGridView.Focus();

                    string geiDieNum = Convert.ToString(tbl3DielogDataGridView.Rows[e.RowIndex].Cells[1].Value);
                    MessageBox.Show(geiDieNum);

                }
                catch (Exception)
                {

                }
            }
        }

        private void FillBy1ToolStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
