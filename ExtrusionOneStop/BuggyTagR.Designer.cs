﻿namespace ExtrusionOneStop
{
    partial class BuggyTagR
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.BuggyTagRBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.GEI_ExtrusionDataSet = new ExtrusionOneStop.GEI_ExtrusionDataSet();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            ((System.ComponentModel.ISupportInitialize)(this.BuggyTagRBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GEI_ExtrusionDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // BuggyTagRBindingSource
            // 
            this.BuggyTagRBindingSource.DataMember = "BuggyTagR";
            this.BuggyTagRBindingSource.DataSource = this.GEI_ExtrusionDataSet;
            // 
            // GEI_ExtrusionDataSet
            // 
            this.GEI_ExtrusionDataSet.DataSetName = "GEI_ExtrusionDataSet";
            this.GEI_ExtrusionDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(532, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(611, 418);
            this.dataGridView1.TabIndex = 1;
            // 
            // reportViewer1
            // 
            reportDataSource1.Name = "BuggyTagR";
            reportDataSource1.Value = this.BuggyTagRBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "ExtrusionOneStop.BuggyTagR.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 12);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(526, 418);
            this.reportViewer1.TabIndex = 4;
            // 
            // BuggyTagR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1155, 523);
            this.Controls.Add(this.reportViewer1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "BuggyTagR";
            this.Text = "BuggyTagR";
            this.Load += new System.EventHandler(this.Form6_Load);
            ((System.ComponentModel.ISupportInitialize)(this.BuggyTagRBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GEI_ExtrusionDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource BuggyTagRBindingSource;
        private GEI_ExtrusionDataSet GEI_ExtrusionDataSet;
    }
}