﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data.SqlTypes;
using Oracle.ManagedDataAccess.Client;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Security.Principal;
using System.Security.Authentication;
using System.Drawing.Printing;
using System.IO;

namespace ExtrusionOneStop
{
    public partial class DieStatus : Form
    {
        public DieStatus()
        {
            InitializeComponent();
        }
        private static DataTable GetData(string sqlCommand, string num, string suff)
        {
            string geiDieNum = num;
            string geiDieSuff = suff;
            string connectionString = @"Data Source = 160.106.20.239,1433\sqlexpress; Initial Catalog = GEI_Extrusion; Integrated Security = True";

            SqlConnection conn = new SqlConnection(connectionString);

            SqlCommand command = new SqlCommand(sqlCommand, conn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;

            DataTable table = new DataTable();
            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
            command.Parameters.Add("@geiDieNum", geiDieNum);
            command.Parameters.Add("@geiDieSuff", geiDieSuff);

            adapter.Fill(table);

            return table;
        }
        private void Form7_Load(object sender, EventArgs e)
        {           
            
        }

        private void Button9_Click(object sender, EventArgs e)
        {
            button2.Hide();
            string geiDieNum = textBox28.Text;
            string geiDieSuff = textBox24.Text;
                        
            try
            {
                tblDieStatusDataGridView.DataSource = GetData("SELECT Rec_id, Die_Number, Die_Suffix, Date_Recvd, Date_Scrapped, Tool_Size, " +
                "Backer, Feeder, Bolster, Change_Tooling1, Change_Tooling2, Change_Tooling3, Vendor FROM tblDieStatus " +
                "WHERE(Die_Number = @geiDieNum) AND(Die_Suffix = @geiDieSuff)", geiDieNum, geiDieSuff);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        private void TblDieStatusDataGridView_Click(object sender, EventArgs e)
        {
            try
            {
                textBox1.Text = tblDieStatusDataGridView.CurrentRow.Cells[1].Value.ToString();
                textBox2.Text = tblDieStatusDataGridView.CurrentRow.Cells[2].Value.ToString();
                textBox3.Text = tblDieStatusDataGridView.CurrentRow.Cells[6].Value.ToString();
                textBox4.Text = tblDieStatusDataGridView.CurrentRow.Cells[7].Value.ToString();
                textBox5.Text = tblDieStatusDataGridView.CurrentRow.Cells[8].Value.ToString();
                textBox6.Text = tblDieStatusDataGridView.CurrentRow.Cells[9].Value.ToString();
                textBox7.Text = tblDieStatusDataGridView.CurrentRow.Cells[4].Value.ToString();
                textBox8.Text = tblDieStatusDataGridView.CurrentRow.Cells[5].Value.ToString();
                comboBox1.Text = tblDieStatusDataGridView.CurrentRow.Cells[3].Value.ToString();
                textBox10.Text = tblDieStatusDataGridView.CurrentRow.Cells[10].Value.ToString();
                textBox11.Text = tblDieStatusDataGridView.CurrentRow.Cells[11].Value.ToString();
                textBox12.Text = tblDieStatusDataGridView.CurrentRow.Cells[12].Value.ToString();
                textBox9.Text = tblDieStatusDataGridView.CurrentRow.Cells[0].Value.ToString();             
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            try
            {   //submit
                string geiDieNum = textBox1.Text;
                string geiDieSuff = textBox2.Text;
                string toolSize = textBox3.Text;
                string backer = textBox4.Text;
                string feeder = textBox5.Text;
                string bolster = textBox6.Text;
                string vendor = comboBox1.Text.ToString();
                string dateRecv = textBox7.Text;
                string dateScrap = textBox8.Text;
                string tooling1 = textBox10.Text;
                string tooling2 = textBox11.Text;
                string tooling3 = textBox12.Text;                

                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString.ToString();
                SqlConnection conn = new SqlConnection(connString);
                SqlCommand cmd = new SqlCommand("Insert into tblDieStatus (Die_Number, Die_Suffix, Vendor, Date_Recvd, Date_Scrapped, Tool_Size, Backer, Feeder, " +
                    "Bolster, Change_Tooling1, Change_Tooling2, Change_Tooling3) Select '" + geiDieNum + "' as Die_Number_Ins, '" + geiDieSuff + "' as Die_Suffix_Ins, '"
                    + vendor + "' as Vendor_Ins, '" + dateRecv + "' as Date_Recvd_Ins, '" + dateScrap + "' as Date_Scrapped_Ins, '" + toolSize + "' as Tool_Size_Ins, '"
                    + backer + "' as Backer_Ins, '" + feeder + "' as Feeder_Ins, '" + bolster + "' as Bolster_Ins, '" + tooling1 + "' as Change_Tooling1_Ins, '"
                    + tooling2 + "' as Change_Tooling2_Ins, '" + tooling3 + "' as Change_Tooling3_Ins", conn);

                DataTable table = new DataTable();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);

                conn.Open();
                adapter.Fill(table);
                conn.Close();
                MessageBox.Show("You have successfully input this Die Status record");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            //update
            try
            {   
                string geiDieNum = textBox1.Text;
                string geiDieSuff = textBox2.Text;
                string toolSize = textBox3.Text;
                string backer = textBox4.Text;
                string feeder = textBox5.Text;
                string bolster = textBox6.Text;
                string vendor = comboBox1.Text.ToString();
                string dateRecv = textBox7.Text;
                string dateScrap = textBox8.Text;
                string tooling1 = textBox10.Text;
                string tooling2 = textBox11.Text;
                string tooling3 = textBox12.Text;
                string recid = textBox9.Text;

                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString.ToString();
                SqlConnection conn = new SqlConnection(connString);
                SqlCommand cmd = new SqlCommand("Update tblDieStatus SET Die_Number=@dieNum, Die_Suffix=@dieSuff, Vendor=@ven, Date_Recvd=@daterecvd, " +
                    "Date_Scrapped=@datescrap, Tool_Size=@tool, Backer=@back, Feeder=@feed, Bolster=@bol, Change_Tooling1=@tool1, Change_Tooling2=@tool2, " +
                    "Change_Tooling3=@tool3 WHERE Rec_id = '" + recid + "'", conn);

                cmd.Parameters.AddWithValue("@dieNum", geiDieNum);
                cmd.Parameters.AddWithValue("@dieSuff", geiDieSuff);
                cmd.Parameters.AddWithValue("@ven", vendor);
                cmd.Parameters.AddWithValue("@daterecvd", dateRecv);
                cmd.Parameters.AddWithValue("@datescrap", dateScrap);
                cmd.Parameters.AddWithValue("@tool", toolSize);
                cmd.Parameters.AddWithValue("@back", backer);
                cmd.Parameters.AddWithValue("@feed", feeder);
                cmd.Parameters.AddWithValue("@bol", bolster);
                cmd.Parameters.AddWithValue("@tool1", tooling1);
                cmd.Parameters.AddWithValue("@tool2", tooling2);
                cmd.Parameters.AddWithValue("@tool3", tooling3);

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable table = new DataTable();

                conn.Open();
                adapter.Fill(table);
                conn.Close();
                MessageBox.Show("You have successfully updated this Die Status record");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }
    }
}
