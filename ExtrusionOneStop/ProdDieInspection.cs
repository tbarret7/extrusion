﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data.SqlTypes;
using Oracle.ManagedDataAccess.Client;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Security.Principal;
using System.Security.Authentication;
using System.Drawing.Printing;
using System.IO;


namespace ExtrusionOneStop
{
    public partial class ProdDieInspection : Form
    {
        public ProdDieInspection()
        {
            InitializeComponent();
        }
        private static DataTable GetData(string sqlCommand, string num)
        {
            string geiDieNum = num;
            
            string connectionString = @"Data Source = 160.106.20.239,1433\sqlexpress; Initial Catalog = GEI_Extrusion; Integrated Security = True";

            SqlConnection conn = new SqlConnection(connectionString);

            SqlCommand command = new SqlCommand(sqlCommand, conn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;

            DataTable table = new DataTable();
            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
            command.Parameters.Add("@geiDieNum", geiDieNum);
            
            adapter.Fill(table);

            return table;
        }
        private void Form4_Load(object sender, EventArgs e)
        {
            textBox12.Hide();

            DateTime thisDay = DateTime.Today;
            textBox6.Text = thisDay.ToShortDateString();
           

        }

        private void Button9_Click(object sender, EventArgs e)
        {
            string geiDieNum = textBox28.Text;
            string geiDieSuff = textBox24.Text;
           
            try
            {
                tblProdDieInspDataGridView.DataSource = GetData("SELECT Rec_id, Die_Number, Die_Suffix, Vendor, " +
                "Comments, RejComments, InspBy, CorrectedBy, CorrectedDate, CurrentDate, DieTrial, Production, Weld_scrap, " +
                "Weld_scrap_comments, Coring, Coring_comments, TB, TB_comments FROM dbo.tblProdDieInsp WHERE Die_Number = @geiDieNum", geiDieNum);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
                        
            button1.Hide();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            string geiDieNum = textBox1.Text;
            string geiDieSuff = textBox2.Text;
            string vendor = comboBox1.Text.ToString();
            string comments = textBox3.Text;
            string rejComments = textBox4.Text;
            string inspBy = textBox5.Text;
            string currentDate = textBox6.Text;
            string correctedBy = textBox7.Text;
            string correctedDate = textBox8.Text;
            string weldScrap = comboBox2.Text.ToString();
            string weldScrapComments = textBox9.Text;
            string coring = comboBox3.Text.ToString();
            string coringComments = textBox10.Text;
            string tb = comboBox4.Text;
            string tbComments = textBox11.Text;
            bool dieTrial = checkBox1.Checked;
            bool production = checkBox2.Checked;
            

            try
            {


                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString.ToString();
                SqlConnection conn = new SqlConnection(connString);
                SqlCommand cmd = new SqlCommand("Insert into tblProdDieInsp (Die_Number, Die_Suffix, Vendor, Comments, RejComments, InspBy, CorrectedBy, CorrectedDate, " +
                    "CurrentDate, DieTrial, Production, Weld_scrap, Weld_scrap_comments, Coring, Coring_comments, TB, TB_comments) Select '"
                    + geiDieNum + "' as Die_Number_Ins, '" + geiDieSuff + "' as Die_Suffix_Ins, '" + vendor + "' as Vendor_Ins, '" + comments + "' as Comments_Ins, '"
                    + rejComments + "' as RejComments_Ins, '" + inspBy + "' as InspBy_Ins, '" + correctedBy + "' as CorrectedBy_Ins, '" + correctedDate + "' as CorrectedDate_Ins, '"
                    + currentDate + "' as CurrentDate_Ins, '" + dieTrial + "' as DieTrial_Ins, '" + production + "' as Production_Ins, '" + weldScrap + "' as Weld_scrap_Ins, '"
                    + weldScrapComments + "' as Weld_scrap_comments_Ins, '" + coring + "' as Coring_Ins, '" + coringComments + "' as Coring_comments_Ins, '"
                    + tb + "' as TB_Ins, '" + tbComments + "' as TB_comments_Ins", conn);

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable table = new DataTable();

                conn.Open();
                adapter.Fill(table);
                conn.Close();

                MessageBox.Show("You have successfully input a new record");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

            }

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            
            string geiDieNum = "";
            string geiDieSuff = "";
            string vendor = "";
            string comments = "";
            string rejComments = "";
            string inspby = "";
            string correctedBy = "";
            string correctDate = "";
            string currentDate = "";
            bool dieTrial;
            bool production;
            string weldScrap = "";
            string weldScrapComments = "";
            string coring = "";
            string coringComments = "";
            string tb = "";
            string tbComments = "";
            int recid = 0;

            try
            {
                 recid = Convert.ToInt32(textBox12.Text);
                 geiDieNum = textBox1.Text;
                 geiDieSuff = textBox2.Text;
                 vendor = comboBox1.Text.ToString();
                 comments = textBox3.Text;
                 rejComments = textBox4.Text;
                 inspby = textBox5.Text;
                 correctedBy = textBox7.Text;
                 correctDate = textBox8.Text;
                 currentDate = textBox6.Text;
                 dieTrial = checkBox1.Checked;
                 production = checkBox2.Checked;
                 weldScrap = comboBox2.Text.ToString();
                 weldScrapComments = textBox9.Text;
                 coring = comboBox3.Text.ToString();
                 coringComments = textBox10.Text;
                 tb = comboBox4.Text.ToString();
                 tbComments = textBox11.Text;

                //update record
                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString.ToString();
                SqlConnection conn = new SqlConnection(connString);
                SqlCommand cmd = new SqlCommand("Update tblProdDieInsp SET Die_Number=@dienum, Die_Suffix=@diesuff, Vendor=@ven, Comments=@comm, " +
                    "RejComments=@rejcomm, InspBy=@inspby, CorrectedBy=@correctby, CorrectedDate=@correctdate, CurrentDate=@currentdate, DieTrial=@dietrial, " +
                    "Production=@prod, Weld_scrap=@weldscrap, Weld_scrap_comments=@weldcomm, Coring=@coring, Coring_comments=@coringcomm, " +
                    "TB=@tb, TB_comments=@tbcomm WHERE Rec_id = " + recid + "", conn);

                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable table = new DataTable();

                cmd.Parameters.AddWithValue("@dienum", geiDieNum);
                cmd.Parameters.AddWithValue("@diesuff", geiDieSuff);
                cmd.Parameters.AddWithValue("@ven", vendor);
                cmd.Parameters.AddWithValue("@comm", comments);
                cmd.Parameters.AddWithValue("@rejcomm", rejComments);
                cmd.Parameters.AddWithValue("@inspby", inspby);
                cmd.Parameters.AddWithValue("@correctby", correctedBy);
                cmd.Parameters.AddWithValue("@correctdate", correctDate);
                cmd.Parameters.AddWithValue("@currentdate", currentDate);
                cmd.Parameters.AddWithValue("@dietrial", dieTrial);
                cmd.Parameters.AddWithValue("@prod", production);
                cmd.Parameters.AddWithValue("@weldscrap", weldScrap);
                cmd.Parameters.AddWithValue("@weldcomm", weldScrapComments);
                cmd.Parameters.AddWithValue("@coring", coring);
                cmd.Parameters.AddWithValue("@coringcomm", coringComments);
                cmd.Parameters.AddWithValue("@tb", tb);
                cmd.Parameters.AddWithValue("@tbcomm", tbComments);

                conn.Open();
                adapter.Fill(table);
                conn.Close();
                cmd.Dispose();

                MessageBox.Show("You've successfully updated this record.");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            
            
        }

        private void TblProdDieInspDataGridView_Click(object sender, EventArgs e)
        {
            try
            {
                textBox12.Text = tblProdDieInspDataGridView.CurrentRow.Cells[0].Value.ToString();
                textBox1.Text = tblProdDieInspDataGridView.CurrentRow.Cells[1].Value.ToString();
                textBox2.Text = tblProdDieInspDataGridView.CurrentRow.Cells[2].Value.ToString();
                textBox8.Text = tblProdDieInspDataGridView.CurrentRow.Cells[3].Value.ToString();
                comboBox2.Text = tblProdDieInspDataGridView.CurrentRow.Cells[4].Value.ToString();
                textBox9.Text = tblProdDieInspDataGridView.CurrentRow.Cells[5].Value.ToString();
                comboBox3.Text = tblProdDieInspDataGridView.CurrentRow.Cells[6].Value.ToString();
                textBox10.Text = tblProdDieInspDataGridView.CurrentRow.Cells[7].Value.ToString();
                comboBox4.Text = tblProdDieInspDataGridView.CurrentRow.Cells[8].Value.ToString();
                textBox11.Text = tblProdDieInspDataGridView.CurrentRow.Cells[9].Value.ToString();
                textBox3.Text = tblProdDieInspDataGridView.CurrentRow.Cells[10].Value.ToString();
                textBox4.Text = tblProdDieInspDataGridView.CurrentRow.Cells[11].Value.ToString();
                textBox5.Text = tblProdDieInspDataGridView.CurrentRow.Cells[12].Value.ToString();
                textBox6.Text = tblProdDieInspDataGridView.CurrentRow.Cells[13].Value.ToString();
                textBox7.Text = tblProdDieInspDataGridView.CurrentRow.Cells[14].Value.ToString();                
                comboBox1.Text = tblProdDieInspDataGridView.CurrentRow.Cells[15].Value.ToString();
                checkBox1.Checked = Convert.ToBoolean(tblProdDieInspDataGridView.CurrentRow.Cells[16].Value);
                checkBox2.Checked = Convert.ToBoolean(tblProdDieInspDataGridView.CurrentRow.Cells[17].Value);               
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }

        private void TblProdDieInspDataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                SelectNextControl(tblProdDieInspDataGridView, true, true, true, true);
                // or Parent.SelectNextControl() if the grid is an only child, etc.
                e.Handled = true;
            }
        }
    }
}
